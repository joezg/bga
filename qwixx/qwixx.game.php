<?php

/**
 *------
 * BGA framework: © Gregory Isabelli <gisabelli@boardgamearena.com> & Emmanuel Colin <ecolin@boardgamearena.com>
 * Qwixx implementation : © <Your name here> <Your email address here>
 * 
 * This code has been produced on the BGA studio platform for use on http://boardgamearena.com.
 * See http://en.boardgamearena.com/#!doc/Studio for more information.
 * -----
 * 
 * qwixx.game.php
 *
 * This is the main file for your game logic.
 *
 * In this PHP file, you are going to defines the rules of the game.
 *
 */


require_once(APP_GAMEMODULE_PATH . 'module/table/table.game.php');


class Qwixx extends Table
{

    function __construct()
    {


        // Your global variables labels:
        //  Here, you can assign labels to global variables you are using for this game.
        //  You can use any number of global variables with IDs between 10 and 99.
        //  If your game has options (variants), you also have to associate here a label to
        //  the corresponding ID in gameoptions.inc.php.
        // Note: afterwards, you can get/set the global variables with getGameStateValue/setGameStateInitialValue/setGameStateValue
        parent::__construct();
        self::initGameStateLabels(array(
            //    "my_first_global_variable" => 10,
            //    "my_second_global_variable" => 11,
            //      ...
            //    "my_first_game_variant" => 100,
            //    "my_second_game_variant" => 101,
            //      ...
        ));
    }

    protected function getGameName()
    {
        return "qwixx";
    }

    /*
        setupNewGame:
        
        This method is called only once, when a new game is launched.
        In this method, you must setup the game according to the game rules, so that
        the game is ready to be played.
    */
    protected function setupNewGame($players, $options = array())
    {
        $sql = "DELETE FROM player WHERE 1 ";
        self::DbQuery($sql);

        // Set the colors of the players with HTML color code
        // The default below is red/green/blue/orange/brown
        // The number of colors defined here must correspond to the maximum number of players allowed for the gams
        $default_colors = array("ff0000", "008000", "0000ff", "ffa500", "773300");


        // Create players
        // Note: if you added some extra field on "player" table in the database (dbmodel.sql), you can initialize it there.
        $sql = "INSERT INTO player (player_id, player_color, player_canal, player_name, player_avatar) VALUES ";
        $values = array();
        foreach ($players as $player_id => $player) {
            $color = array_shift($default_colors);
            $values[] = "('" . $player_id . "','$color','" . $player['player_canal'] . "','" . addslashes($player['player_name']) . "','" . addslashes($player['player_avatar']) . "')";
        }
        $sql .= implode($values, ',');
        self::DbQuery($sql);
        self::reloadPlayersBasicInfos();

        /************ Start the game initialization *****/

        // Init global values with their initial values
        //self::setGameStateInitialValue( 'my_first_global_variable', 0 );

        // Init game statistics
        // (note: statistics used in this file must be defined in your stats.inc.php file)
        //self::initStat( 'table', 'table_teststat1', 0 );    // Init a table statistics
        //self::initStat( 'player', 'player_teststat1', 0 );  // Init a player statistics (for all players)

        // TODO: setup the initial game situation here


        // Activate first player (which is in general a good idea :) )
        $this->activeNextPlayer();

        /************ End of the game initialization *****/
    }

    /*
        getAllDatas: 
        
        Gather all informations about current game situation (visible by the current player).
        
        The method is called each time the game interface is displayed to a player, ie:
        _ when the game starts
        _ when a player refreshes the game page (F5)
    */
    protected function getAllDatas()
    {
        $result = array('players' => array());

        $current_player_id = self::getCurrentPlayerId();    // !! We must only return informations visible by this player !!

        // Get information about players
        // Note: you can retrieve some extra field you added for "player" table in "dbmodel.sql" if you need it.
        $sql = "SELECT player_id id, player_score score FROM player ";
        $result['players'] = self::getCollectionFromDb($sql);

        $sql = "SELECT player, color, ordinal FROM CROSSED_FIELDS";
        $result['crossedFields'] = self::getObjectListFromDB($sql);

        $sql = "SELECT player, count FROM FEHLWURFEL";
        $result['fehlwurfel'] = self::getObjectListFromDB($sql);

        return $result;
    }

    /*
        getGameProgression:
        
        Compute and return the current game progression.
        The number returned must be an integer beween 0 (=the game just started) and
        100 (= the game is finished or almost finished).
    
        This method is called each time we are in a game state with the "updateGameProgression" property set to true 
        (see states.inc.php)
    */
    function getGameProgression()
    {
        // TODO: compute and return the game progression

        return 0;
    }


    //////////////////////////////////////////////////////////////////////////////
    //////////// Utility functions
    ////////////    

    /*
        In this space, you can put any utility methods useful for your game logic
    */

    function rollDiceAndPersist()
    {

        // first clear data in dice table
        $sql = "DELETE FROM DICE";
        self::DbQuery($sql);


        // insert new values for dice
        $sql = "INSERT INTO DICE (color, value) VALUES";
        $values = array();
        foreach ($this->dice as $die) {
            $values[] = "('" . $die . "'," . bga_rand(1, 6) . ")";
        }
        $sql .= implode($values, ',');
        self::DbQuery($sql);
    }

    function getCurrentDiceValues()
    {
        $sql = "SELECT color, value FROM DICE";
        return self::getCollectionFromDb($sql);
    }

    function getPlayerCrossesRange($playerId)
    {
        $sql = "SELECT color, max(ordinal) max, min(ordinal) min FROM `CROSSED_FIELDS` WHERE player = '" . $playerId . "' GROUP BY color";
        return self::getCollectionFromDb($sql);
    }

    //////////////////////////////////////////////////////////////////////////////
    //////////// Player actions
    //////////// 

    /*
        Each time a player is doing some game action, one of the methods below is called.
        (note: each method below must match an input method in qwixx.action.php)
    */

    function pass()
    {
        self::checkAction('pass');
        $this->gamestate->setPlayerNonMultiactive(self::getCurrentPlayerId(), 'continue');
    }

    function useWhite()
    {
        self::checkAction('use');
        // TODO validate clicked field here
        $this->gamestate->setPlayerNonMultiactive(self::getCurrentPlayerId(), 'continue');
    }
    /*
    
    Example:

    function playCard( $card_id )
    {
        // Check that this is the player's turn and that it is a "possible action" at this game state (see states.inc.php)
        self::checkAction( 'playCard' ); 
        
        $player_id = self::getActivePlayerId();
        
        // Add your game logic to play a card there 
        ...
        
        // Notify all players about the card played
        self::notifyAllPlayers( "cardPlayed", clienttranslate( '${player_name} played ${card_name}' ), array(
            'player_id' => $player_id,
            'player_name' => self::getActivePlayerName(),
            'card_name' => $card_name,
            'card_id' => $card_id
        ) );
          
    }
    
    */


    //////////////////////////////////////////////////////////////////////////////
    //////////// Game state arguments
    ////////////

    /*
        Here, you can create methods defined as "game state arguments" (see "args" property in states.inc.php).
        These methods function is to return some additional information that is specific to the current
        game state.
    */
    function checkDieValueAvailable($color, $value, $range)
    {
        if (!array_key_exists($color, $range)) {
            return true;
        }

        switch ($color) {
            case 'red':
            case 'yellow':
                return $range[$color]["max"] < $value;

            case 'green':
            case 'blue':
                return $range[$color]["min"] > $value;

            default:
                return false;
        }
    }

    function addDieIfAvailable($color, $value, $range, &$availableValue)
    {
        if ($this->checkDieValueAvailable($color, $value, $range)) {
            $availableValue[] =  ["color" => $color, "ordinal" => $value];
        }
    }

    function getAvailableWhiteDiceValues($dice, $crossesRange, &$result)
    {
        $sum = $dice["white1"]["value"] + $dice["white2"]["value"];

        $this->addDieIfAvailable("red", $sum, $crossesRange, $result);
        $this->addDieIfAvailable("yellow", $sum, $crossesRange, $result);
        $this->addDieIfAvailable("green", $sum, $crossesRange, $result);
        $this->addDieIfAvailable("blue", $sum, $crossesRange, $result);
    }

    function getAvailableColoredDiceValues($color, $dice, $crossesRange, &$result)
    {
        $first = $dice["white1"]["value"] + $dice[$color]["value"];
        $second = $dice["white2"]["value"] + $dice[$color]["value"];

        $this->addDieIfAvailable($color, $first, $crossesRange, $result);
        $this->addDieIfAvailable($color, $second, $crossesRange, $result);
    }

    function argWhiteDieTurn()
    {
        $dice = $this->getCurrentDiceValues();

        // check available fields
        $current_player_id = self::getCurrentPlayerId();
        $crossesRange = $this->getPlayerCrossesRange($current_player_id);

        $availableFields = [];
        $availableColoredFields = [];

        $this->getAvailableWhiteDiceValues($dice, $crossesRange, $availableFields);
        $this->getAvailableColoredDiceValues("red", $dice, $crossesRange, $availableColoredFields);
        $this->getAvailableColoredDiceValues("yellow", $dice, $crossesRange, $availableColoredFields);
        $this->getAvailableColoredDiceValues("green", $dice, $crossesRange, $availableColoredFields);
        $this->getAvailableColoredDiceValues("blue", $dice, $crossesRange, $availableColoredFields);

        return array(
            'dice' => array_values($dice),
            'availableFields' => $availableFields,
            'availableColoredFields' => $availableColoredFields
        );
    }

    /*
    Example for game state "MyGameState":
    
    function argMyGameState()
    {
        // Get some values from the current game situation in database...
    
        // return values:
        return array(
            'variable1' => $value1,
            'variable2' => $value2,
            ...
        );
    }    
    */

    //////////////////////////////////////////////////////////////////////////////
    //////////// Game state actions
    ////////////

    /*
        Here, you can create methods defined as "game state actions" (see "action" property in states.inc.php).
        The action method of state X is called everytime the current game state is set to X.
    */

    function stWhiteDieTurn()
    {
        //$this->gamestate->setAllPlayersMultiactive();
        // check which players will be active in this turn
        $players = self::loadPlayersBasicInfos();
        $i = 1;
        $activePlayers = array();
        foreach ($players as $player_id => $player) {
            if ($i < 3)
                $activePlayers[] = $player_id;
            else
                self::notifyAllPlayers("autopass", clienttranslate('${player_name} cannot use white dice'), array(
                    'player_name' => $player['player_name'],
                ));

            $i++;
        }
        $this->gamestate->setPlayersMultiactive($activePlayers, 'continue');

        // roll dice & persist to db, later in args method pass to UI via args
        $this->rollDiceAndPersist();
    }



    function stNextPlayer()
    {
        $this->activeNextPlayer();
        self::notifyAllPlayers("newround", clienttranslate('Starting new round'), array());

        $this->gamestate->nextState("nextPlayer");
    }

    /*
Example for game state "MyGameState":

    function stMyGameState()
    {
        // Do some stuff ...
        
        // (very often) go to another gamestate
        $this->gamestate->nextState( 'some_gamestate_transition' );
    }    
    */

    //////////////////////////////////////////////////////////////////////////////
    //////////// Zombie
    ////////////

    /*
        zombieTurn:
        
        This method is called each time it is the turn of a player who has quit the game (= "zombie" player).
        You can do whatever you want in order to make sure the turn of this player ends appropriately
        (ex: pass).
    */

    function zombieTurn($state, $active_player)
    {
        $statename = $state['name'];

        if ($state['type'] == "activeplayer") {
            switch ($statename) {
                default:
                    $this->gamestate->nextState("zombiePass");
                    break;
            }

            return;
        }

        if ($state['type'] == "multipleactiveplayer") {
            // Make sure player is in a non blocking status for role turn
            $sql = "
                UPDATE  player
                SET     player_is_multiactive = 0
                WHERE   player_id = $active_player
            ";
            self::DbQuery($sql);

            $this->gamestate->updateMultiactiveOrNextState('');
            return;
        }

        throw new feException("Zombie mode not supported at this game state: " . $statename);
    }
}
