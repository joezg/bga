-- ------
-- BGA framework: © Gregory Isabelli <gisabelli@boardgamearena.com> & Emmanuel Colin <ecolin@boardgamearena.com>
-- Qwixx implementation : © <Your name here> <Your email address here>
--
-- This code has been produced on the BGA studio platform for use on http://boardgamearena.com.
-- See http://en.boardgamearena.com/#!doc/Studio for more information.
-- -----
-- dbmodel.sql
-- This is the file where you are describing the database schema of your game
-- Basically, you just have to export from PhpMyAdmin your table structure and copy/paste
-- this export here.
-- Note that the database itself and the standard tables ("global", "stats", "gamelog" and "player") are
-- already created and must not be created here
-- Note: The database schema is created from this file when the game starts. If you modify this file,
--       you have to restart a game to see your changes in database.
-- Example 1: create a standard "card" table to be used with the "Deck" tools (see example game "hearts"):
-- CREATE TABLE IF NOT EXISTS `card` (
--   `card_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
--   `card_type` varchar(16) NOT NULL,
--   `card_type_arg` int(11) NOT NULL,
--   `card_location` varchar(16) NOT NULL,
--   `card_location_arg` int(11) NOT NULL,
--   PRIMARY KEY (`card_id`)
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
-- Example 2: add a custom field to the standard "player" table
-- ALTER TABLE `player` ADD `player_my_custom_field` INT UNSIGNED NOT NULL DEFAULT '0';
SET
  @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS,
  UNIQUE_CHECKS = 0;
SET
  @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS,
  FOREIGN_KEY_CHECKS = 0;
SET
  @OLD_SQL_MODE = @@SQL_MODE,
  SQL_MODE = 'TRADITIONAL,ALLOW_INVALID_DATES';
-- -----------------------------------------------------
  -- Table `DICE`
  -- -----------------------------------------------------
  CREATE TABLE IF NOT EXISTS `DICE` (
    `color` VARCHAR(8) NOT NULL,
    `value` INT(1) NOT NULL,
    PRIMARY KEY (`color`)
  ) ENGINE = InnoDB;
-- -----------------------------------------------------
  -- Table `CROSSED_FIELDS`
  -- -----------------------------------------------------
  CREATE TABLE IF NOT EXISTS `CROSSED_FIELDS` (
    `player` INT(10) NOT NULL,
    `color` VARCHAR(8) NOT NULL,
    `ordinal` TINYINT NOT NULL,
    PRIMARY KEY (`player`, `color`, `ordinal`)
  ) ENGINE = InnoDB;
-- -----------------------------------------------------
  -- Table `FEHLWURFEL`
  -- -----------------------------------------------------
  CREATE TABLE IF NOT EXISTS `FEHLWURFEL` (
    `player` INT(10) NOT NULL,
    `count` INT(1) NOT NULL DEFAULT 0,
    PRIMARY KEY (`player`)
  ) ENGINE = InnoDB;
SET
  SQL_MODE = @OLD_SQL_MODE;
SET
  FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS;
SET
  UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS;
-- -----------------------------------------------------
  -- Data for table `DICE`
  -- -----------------------------------------------------
  START TRANSACTION;
INSERT INTO `DICE` (`color`, `value`)
VALUES
  ('white1', 1);
INSERT INTO `DICE` (`color`, `value`)
VALUES
  ('white2', 1);
INSERT INTO `DICE` (`color`, `value`)
VALUES
  ('red', 1);
INSERT INTO `DICE` (`color`, `value`)
VALUES
  ('yellow', 1);
INSERT INTO `DICE` (`color`, `value`)
VALUES
  ('green', 1);
INSERT INTO `DICE` (`color`, `value`)
VALUES
  ('blue', 1);
COMMIT;