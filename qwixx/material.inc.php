<?php

/**
 *------
 * BGA framework: © Gregory Isabelli <gisabelli@boardgamearena.com> & Emmanuel Colin <ecolin@boardgamearena.com>
 * Qwixx implementation : © <Your name here> <Your email address here>
 * 
 * This code has been produced on the BGA studio platform for use on http://boardgamearena.com.
 * See http://en.boardgamearena.com/#!doc/Studio for more information.
 * -----
 *
 * material.inc.php
 *
 * Qwixx game material description
 *
 * Here, you can describe the material of your game with PHP variables.
 *   
 * This file is loaded in your game logic class constructor, ie these variables
 * are available everywhere in your game logic code.
 *
 */

$this->colors = array(
    "red" => array("top" => 15, "reverse" => false),
    "yellow" => array("top" => 66, "reverse" => false),
    "green" => array("top" => 117, "reverse" => true),
    "blue" => array("top" => 168, "reverse" => true),
);

$this->dice = array("white1", "white2", "red", "yellow", "green", "blue");
