/**
 *------
 * BGA framework: © Gregory Isabelli <gisabelli@boardgamearena.com> & Emmanuel Colin <ecolin@boardgamearena.com>
 * Qwixx implementation : © <Your name here> <Your email address here>
 *
 * This code has been produced on the BGA studio platform for use on http://boardgamearena.com.
 * See http://en.boardgamearena.com/#!doc/Studio for more information.
 * -----
 *
 * qwixx.js
 *
 * Qwixx user interface script
 *
 * In this file, you are describing the logic of your user interface, in Javascript language.
 *
 */

define([
  'dojo',
  'dojo/_base/declare',
  'dojo/_base/connect',
  'ebg/core/gamegui',
  'ebg/counter'
], function(dojo, declare, conn) {
  return declare('bgagame.qwixx', ebg.core.gamegui, {
    constructor: function() {
      console.log('qwixx constructor');

      // Here, you can init the global variables of your user interface
      // Example:
      // this.myGlobalValue = 0;
    },

    /*
            setup:
            
            This method must set up the game user interface according to current game situation specified
            in parameters.
            
            The method is called each time the game interface is displayed to a player, ie:
            _ when the game starts
            _ when a player refreshes the game page (F5)
            
            "gamedatas" argument contains all datas retrieved by your "getAllDatas" PHP method.
        */

    setup: function(gamedatas) {
      console.log('Starting game setup');
      dojo.place('player-' + this.player_id, 'dice', 'before');

      gamedatas.crossedFields.forEach(
        function(field) {
          this.setCross(field.player, field.color, field.ordinal);
        }.bind(this)
      );

      gamedatas.fehlwurfel.forEach(function(fehlwurfe) {
        for (let i = 1; i <= fehlwurfe.count; i++) {
          dojo
            .query('#player-' + fehlwurfe.player + ' .fehlwurfe-' + i)
            .addClass('little-red-cross');
        }
      });

      // Setup game notifications to handle (see "setupNotifications" method below)
      this.setupNotifications();

      console.log('Ending game setup');
    },

    ///////////////////////////////////////////////////
    //// Game & client states

    // onEnteringState: this method is called each time we are entering into a new game state.
    //                  You can use this method to perform some user interface changes at this moment.
    //
    onEnteringState: function(stateName, args) {
      console.log('Entering state: ' + stateName);
      var currentPlayer = this.player_id;
      console.log(args.args);
      switch (stateName) {
        case 'whiteDieTurn':
          // show dice values
          args.args.dice.forEach(
            function(die) {
              this.setDie(die.color, die.value);
            }.bind(this)
          );

          // mark fields as available (css) and connect handlers
          if (this.isCurrentPlayerActive()) {
            args.args.availableFields.forEach(
              function(field) {
                this.setAvailableField(
                  currentPlayer,
                  field.color,
                  field.ordinal
                );
              }.bind(this)
            );

            var isSecondary = true;
            args.args.availableColoredFields.forEach(
              function(field) {
                this.setAvailableField(
                  currentPlayer,
                  field.color,
                  field.ordinal,
                  isSecondary
                );
              }.bind(this)
            );
          }

          break;

        case 'dummmy':
          break;
      }
    },

    // onLeavingState: this method is called each time we are leaving a game state.
    //                 You can use this method to perform some user interface changes at this moment.
    //
    onLeavingState: function(stateName) {
      console.log('Leaving state: ' + stateName);

      switch (stateName) {
        /* Example:
            
            case 'myGameState':
            
                // Hide the HTML block we are displaying only during this game state
                dojo.style( 'my_html_block_id', 'display', 'none' );
                
                break;
           */

        case 'dummmy':
          break;
      }
    },

    // onUpdateActionButtons: in this method you can manage "action buttons" that are displayed in the
    //                        action status bar (ie: the HTML links in the status bar).
    //
    onUpdateActionButtons: function(stateName, args) {
      console.log('onUpdateActionButtons: ' + stateName);

      if (this.isCurrentPlayerActive()) {
        switch (stateName) {
          case 'whiteDieTurn':
            this.addActionButton('pass_button', _('pass'), 'onPass');
            break;
          case 'colorDieTurn':
            this.addActionButton('pass_button', _('pass'), 'onPass');
            break;
        }
      }
    },

    ///////////////////////////////////////////////////
    //// Utility methods

    /*
        
            Here, you can defines some utility methods that you can use everywhere in your javascript
            script.
        
        */

    getPlayerCrossNode: function(player, color, ordinal) {
      return dojo.query(
        '#player-' + player + ' .cross-' + color + '-' + ordinal
      );
    },

    setAvailableField: function(player, color, ordinal, isSecondary) {
      var node = this.getPlayerCrossNode(player, color, ordinal);

      node.addClass('availableField').addClass(color);

      if (!isSecondary) {
        node
          .addClass('primary')
          .addClass(color)
          .connect('onclick', this, this.onFieldClicked);
      }
    },

    setCross: function(player, color, ordinal) {
      this.setNodeCross(this.getPlayerCrossNode(player, color, ordinal));
    },

    setNodeCross: function(node) {
      var className = 'big-green-cross';
      if (node.addClass) {
        node.addClass(className);
        return;
      }

      dojo.addClass(node, className);
    },

    getDieNode: function(color) {
      var colors = {
        white1: 'die_dots_white_1',
        white2: 'die_dots_white_2',
        red: 'die_dots_red',
        yellow: 'die_dots_yellow',
        green: 'die_dots_green',
        blue: 'die_dots_blue'
      };

      return dojo.byId(colors[color]);
    },

    setDie: function(color, value) {
      dojo.addClass(this.getDieNode(color), 'dots-' + value);
    },

    ///////////////////////////////////////////////////
    //// Player's action

    /*
        
            Here, you are defining methods to handle player's action (ex: results of mouse click on 
            game objects).
            
            Most of the time, these methods:
            _ check the action is possible at this game state.
            _ make a call to the game server
        
        */

    onFieldClicked: function(e) {
      if (!this.checkAction('use')) {
        return;
      }
      var target = e.target,
        color = dojo.attr(target, 'data-color'),
        ordinal = dojo.attr(target, 'data-ord');

      this.ajaxcall(
        '/qwixx/qwixx/useWhite.html',
        {
          lock: true,
          color: color,
          ordinal: ordinal
        },
        this,
        function(result) {
          // this field is selected
          this.setNodeCross(target);
          // remove available field marker
          dojo.query('.availableField').removeClass('availableField');
        }.bind(this),
        function(is_error) {
          // What to do after the server call in anyway (success or failure)
          // (most of the time: nothing)
        }
      );
    },

    onPass: function() {
      console.log('Passed');

      if (!this.checkAction('pass')) {
        return;
      }

      this.ajaxcall(
        '/qwixx/qwixx/pass.html',
        {
          lock: true
        },
        this,
        function(result) {
          // What to do after the server call if it succeeded
          // (most of the time: nothing)
        },
        function(is_error) {
          // What to do after the server call in anyway (success or failure)
          // (most of the time: nothing)
        }
      );
    },

    /* Example:
        
        onMyMethodToCall1: function( evt )
        {
            console.log( 'onMyMethodToCall1' );
            
            // Preventing default browser reaction
            dojo.stopEvent( evt );

            // Check that this action is possible (see "possibleactions" in states.inc.php)
            if( ! this.checkAction( 'myAction' ) )
            {   return; }

            this.ajaxcall( "/qwixx/qwixx/myAction.html", { 
                                                                    lock: true, 
                                                                    myArgument1: arg1, 
                                                                    myArgument2: arg2,
                                                                    ...
                                                                 }, 
                         this, function( result ) {
                            
                            // What to do after the server call if it succeeded
                            // (most of the time: nothing)
                            
                         }, function( is_error) {

                            // What to do after the server call in anyway (success or failure)
                            // (most of the time: nothing)

                         } );        
        },        
        
        */

    ///////////////////////////////////////////////////
    //// Reaction to cometD notifications

    /*
            setupNotifications:
            
            In this method, you associate each of your game notifications with your local method to handle it.
            
            Note: game notification names correspond to "notifyAllPlayers" and "notifyPlayer" calls in
                  your qwixx.game.php file.
        
        */
    setupNotifications: function() {
      console.log('notifications subscriptions setup');

      // TODO: here, associate your game notifications with local methods

      // Example 1: standard notification handling
      // dojo.subscribe( 'cardPlayed', this, "notif_cardPlayed" );

      // Example 2: standard notification handling + tell the user interface to wait
      //            during 3 seconds after calling the method in order to let the players
      //            see what is happening in the game.
      // dojo.subscribe( 'cardPlayed', this, "notif_cardPlayed" );
      // this.notifqueue.setSynchronous( 'cardPlayed', 3000 );
      //
    }

    // TODO: from this point and below, you can write your game notifications handling methods

    /*
        Example:
        
        notif_cardPlayed: function( notif )
        {
            console.log( 'notif_cardPlayed' );
            console.log( notif );
            
            // Note: notif.args contains the arguments specified during you "notifyAllPlayers" / "notifyPlayer" PHP call
            
            // TODO: play the card in the user interface.
        },    
        
        */
  });
});
