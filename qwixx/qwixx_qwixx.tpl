{OVERALL_GAME_HEADER}

<!-- 
--------
-- BGA framework: © Gregory Isabelli <gisabelli@boardgamearena.com> & Emmanuel Colin <ecolin@boardgamearena.com>
-- Qwixx implementation : © <Your name here> <Your email address here>
-- 
-- This code has been produced on the BGA studio platform for use on http://boardgamearena.com.
-- See http://en.boardgamearena.com/#!doc/Studio for more information.
-------

    qwixx_qwixx.tpl
    
    This is the HTML template of your game.
    
    Everything you are writing in this file will be displayed in the HTML page of your game user interface,
    in the "main game zone" of the screen.
    
    You can use in this template:
    _ variables, with the format {MY_VARIABLE_ELEMENT}.
    _ HTML block, with the BEGIN/END format
    
    See your "view" PHP file to check how to set variables and control blocks
    
    Please REMOVE this comment before publishing your game on BGA
-->
<div id="play-area">
    <div id="dice">
        <div id="die_white_1" class="die">
            <div id="die_dots_white_1" class="dots"></div>
        </div>
        <div id="die_white_2" class="die">
            <div id="die_dots_white_2" class="dots"></div>
        </div>
        <div id="die_red" class="die">
            <div id="die_dots_red" class="dots"></div>
        </div>
        <div id="die_yellow" class="die">
            <div id="die_dots_yellow" class="dots"></div>
        </div>
        <div id="die_green" class="die">
            <div id="die_dots_green" class="dots"></div>
        </div>
        <div id="die_blue" class="die">
            <div id="die_dots_blue" class="dots"></div>
        </div>
    </div>
</div>

<div class="player-boards">
<!-- BEGIN player -->
<div id="player-{ID}" class="player-scoresheet">
    <h2 style="color:#{PLAYER_COLOR}">{NAME}</h2>
    <div class="scoresheet">
        <!-- BEGIN cross -->
        <div class="cross cross-{COLOR}-{ORD}" data-color="{COLOR}" data-ord="{ORD}" style="left: {LEFT}px; top: {TOP}px"></div>
        <!-- END cross -->

        <div class="fehlwurfen">
            <div class="fehlwurfe fehlwurfe-1"></div>
            <div class="fehlwurfe fehlwurfe-2"></div>
            <div class="fehlwurfe fehlwurfe-3"></div>
            <div class="fehlwurfe fehlwurfe-4"></div>
        </div>
    </div>
</div>
<!-- END player -->
</div>


<script type="text/javascript">

// Javascript HTML templates

/*
// Example:
var jstpl_some_game_item='<div class="my_game_item" id="my_game_item_${id}"></div>';

*/

</script>  

{OVERALL_GAME_FOOTER}
