{OVERALL_GAME_HEADER}

<!-- 
--------
-- BGA framework: © Gregory Isabelli <gisabelli@boardgamearena.com> & Emmanuel Colin <ecolin@boardgamearena.com>
-- Dudo implementation : © <Your name here> <Your email address here>
-- 
-- This code has been produced on the BGA studio platform for use on http://boardgamearena.com.
-- See http://en.boardgamearena.com/#!doc/Studio for more information.
-------

    dudo_dudo.tpl
    
    This is the HTML template of your game.
    
    Everything you are writing in this file will be displayed in the HTML page of your game user interface,
    in the "main game zone" of the screen.
    
    You can use in this template:
    _ variables, with the format {MY_VARIABLE_ELEMENT}.
    _ HTML block, with the BEGIN/END format
    
    See your "view" PHP file to check how to set variables and control blocks
-->

<div id="playertables">

    <!-- BEGIN player -->
    <div class="playertable whiteblock playertable_{DIR}">
        <div class="playertablename" style="color:#{PLAYER_COLOR}">
            {PLAYER_NAME}
        </div>
        <div >
            <div id="dice_number_{PLAYER_ID}" class="number_of_dice nr-dice-tooltip">0</div><div id="dice_nr_icon_{PLAYER_ID}" class="die-notif die_{COLOR} nr-dice-tooltip"></div>
        </div>
        <div class="playertablebid hidden" id="playertablebid_{PLAYER_ID}">
            <div id="bidding_bid_{PLAYER_ID}" class="bid">
            </div>
            <div class="die die_{COLOR} bidding_die" id="bidding_die_{PLAYER_ID}">
                 <div class="dots" >
                 </div>
            </div>
        </div>
    </div>
    <!-- END player -->
	
	<div id="usedDiceCup"></div>

</div>

<div id="cup_bidding">
	<div id="mycup_wrap" class="whiteblock">
		<h3>{MY_CUP}</h3>
		<div id="mycup">
			<!-- BEGIN die -->
			<div class="die die_{COLOR} {ROW} {COLUMN} hidden" id="die_{I}">
				<div class="dots" >
				</div>
			</div>
			<!-- END die -->
		</div>
	</div>

	<div id="bidding_box" class="whiteblock">
		<div id="biddingbox-bid" class="display-none">
			<h3>{PLACE_BID}</h3>
			<div id="die_bid">
				<!-- BEGIN die_bid -->
				<div class="die die_{COLOR} {ROW} {COLUMN}" id="die_bid_{I}">
					<div class="dots die_{DOTS}" >
					</div>
				</div>
				<!-- END die_bid -->
			</div>
			<div id="arrow_up" class="arrow-up"></div>
			<div id="number_bid">5</div>
			<div id="arrow_down" class="arrow-down"></div>
            <h3 id="palifico_round_info" class="display-none palifico-info">{PALIFICO}</h3>
		</div>
		<div id="biddingbox-wait" class="display-none">
			<h3>{PLEASE_WAIT}</h3>
		</div>
	</div>

    <div id="perudo_banner"></div>
</div>


<script type="text/javascript">
    // Javascript HTML templates
    var jstpl_die='<div class="die die_${color}"><div class="dots ${dots_class}"></div></div>';
</script>

{OVERALL_GAME_FOOTER}
