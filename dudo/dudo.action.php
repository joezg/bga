<?php
/**
 *------
 * BGA framework: © Gregory Isabelli <gisabelli@boardgamearena.com> & Emmanuel Colin <ecolin@boardgamearena.com>
 * Dudo implementation : © <Your name here> <Your email address here>
 *
 * This code has been produced on the BGA studio platform for use on http://boardgamearena.com.
 * See http://en.boardgamearena.com/#!doc/Studio for more information.
 * -----
 * 
 * dudo.action.php
 *
 * Dudo main action entry point
 *
 *
 * In this file, you are describing all the methods that can be called from your
 * user interface logic (javascript).
 *       
 * If you define a method "myAction" here, then you can call it from your javascript code with:
 * this.ajaxcall( "/dudo/dudo/myAction.html", ...)
 *
 */
  
  
  class action_dudo extends APP_GameAction
  { 
    // Constructor: please do not modify
   	public function __default()
  	{
  	    if( self::isArg( 'notifwindow') )
  	    {
            $this->view = "common_notifwindow";
  	        $this->viewArgs['table'] = self::getArg( "table", AT_posint, true );
  	    }
  	    else
  	    {
            $this->view = "dudo_dudo";
            self::trace( "Complete reinitialization of board game" );
      }
  	} 
  	
  	public function bid()
    {
        self::setAjaxMode(); 
        
        /*$bid = array(
			'die' => self::getArg( "bidDie", AT_posint, true ),
			'value' => self::getArg( "bidValue", AT_posint, true )
			);*/
			
		$bid = array();
		$bid['die'] = self::getArg( "bidDie", AT_posint, true );
		$bid['value'] = self::getArg( "bidValue", AT_posint, true );
		$this->game->bid($bid);

        self::ajaxResponse( );
    }
	
	public function doubt()
    {
        self::setAjaxMode(); 
        
        $this->game->doubt();

        self::ajaxResponse( );
    }

    public function calza()
    {
        self::setAjaxMode();

        $this->game->calza();

        self::ajaxResponse( );
      }
	
    /*
    
    Example:
  	
    public function myAction()
    {
        self::setAjaxMode();     

        // Retrieve arguments
        // Note: these arguments correspond to what has been sent through the javascript "ajaxcall" method
        $arg1 = self::getArg( "myArgument1", AT_posint, true );
        $arg2 = self::getArg( "myArgument2", AT_posint, true );

        // Then, call the appropriate method in your game logic, like "playCard" or "myAction"
        $this->game->myAction( $arg1, $arg2 );

        self::ajaxResponse( );
    }
    
    */

  }
  

