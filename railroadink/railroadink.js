/**
 *------
 * BGA framework: © Gregory Isabelli <gisabelli@boardgamearena.com> & Emmanuel Colin <ecolin@boardgamearena.com>
 * railroadink implementation : © <Your name here> <Your email address here>
 *
 * This code has been produced on the BGA studio platform for use on http://boardgamearena.com.
 * See http://en.boardgamearena.com/#!doc/Studio for more information.
 * -----
 *
 * railroadink.js
 *
 * railroadink user interface script
 *
 * In this file, you are describing the logic of your user interface, in Javascript language.
 *
 */

 var ROUTE_TYPE_REGULAR = 0;
 var ROUTE_TYPE_SPECIAL = 1;

define([
  'dojo',
  'dojo/_base/declare',
  'dojo/NodeList-traverse',
  'ebg/core/gamegui',
  'ebg/counter'
], function(dojo, declare) {
  //TODO add sounds

  var eventHandles = {
    handles: {},
    attach: function(node, group, event, handle) {
      if (!this.handles[group]) {
        this.handles[group] = [];
      }

      this.handles[group].push(dojo.connect(node, event, handle));
    },
    attachQuery: function(query, group, event, handle) {
      query.forEach(
        function(node) {
          this.attach(node, group, event, handle);
        }.bind(this)
      );
    },
    detachAll: function(group) {
      if (!this.handles[group]) {
        return;
      }

      this.handles[group].forEach(function(handle) {
        handle.remove();
      });
      this.handles[group] = [];
    }
  };

  var dice = {
    value: [],
    selectedDie: null,
    element: dojo.query('#dice'),
    set: function(dice, usedDice) {
      this.value = dice;
      dice.forEach(this.setDie.bind(this));
      usedDice.forEach(this.useDie.bind(this));
    },
    reset: function() {
      this.unselectAll();
      this.unuseAll();
      this.removeRoutes();
    },
    setDie: function(die) {
      dojo.addClass('die-' + die.id, 'route-' + die.route);
    },
    useDie: function(die) {
      dojo.addClass('die-' + die.id, 'used');
      dojo.removeClass('die-' + die.id, 'selected');
    },
    unuseAll: function(die) {
      this.element.query('.die').removeClass('used');
      this.element.query('.die').removeClass('selected');
    },
    unselectAll: function() {
      this.element.query('.die').removeClass('selected');
    },
    removeRoutes: function() {
      var diceElements = this.element.query('.die');
      diceElements.removeClass();
      diceElements.addClass("die");
    },
    activate: function(onSelected) {
      this.element.addClass('active');
      this.selectedDie = null;

      eventHandles.attachQuery(
        dojo.query('#dice .die'),
        'diceActivation',
        'click',
        function(e) {
          var dieElement = dojo.query(e.target);

          this.unselectAll();
          dieElement.addClass('selected');

          var selectedDieId = +dojo.attr(e.target, 'data-id');
          this.selectedDie = this.value.find(function(die) {
            return +die.id === +selectedDieId;
          });
          onSelected(this.selectedDie);
        }.bind(this)
      );
    },
    deactivate: function() {
      this.element.removeClass('active');
      this.unselectAll();
      eventHandles.detachAll('diceActivation');
    }
  };

  var boards = {
    set: function(boards) {
      boards.forEach(this.setBoard.bind(this));
    },
    setBoard: function(board) {
        var playerBoard = this.getPlayerBoard(board.playerId);
        playerBoard.setFields(board.fields);
        playerBoard.setScore(board.score);
    },
    getPlayerBoard: function(player) {
      var playerBoardElement = dojo.query('#player-sheet-' + player);
      return {
        setFields: function(fields) {
          fields.forEach(this.setField.bind(this));
        },
        setField: function(field) {
          this.getField(field.x, field.y).set(field);
        },
        resetFields: function(fields){
          this.deactivateAllFields();
          this.getAllFields().reset();
          if (fields){
            this.setFields(fields);
          }
        },
        getField: function(x, y) {
          var fieldElement = playerBoardElement.query('.field-' + x + '-' + y);
          var routeElement = fieldElement.query(' .route');
          var roundElement = fieldElement.query(' .round-number');
          return {
            set: function(field) {
              routeElement.addClass('set');

              routeElement.attr("data-route", field.route);

              routeElement.addClass('route-' + field.route);
              routeElement.addClass('route-type-' + field.routeType);
              routeElement.attr("data-route-type", field.routeType);

              var rotation = field.rotation || 0;
              routeElement.attr('data-rotation', rotation);

              routeElement.attr('data-flipped', +field.flipped);

              this.addTransform();

              if (field.round){
                roundElement.innerHTML(field.round); 
              }
            },
            getData: function() {
              return {
                x: x,
                y: y,
                rotation: routeElement.attr('data-rotation')[0],
                flipped: routeElement.attr('data-flipped')[0],
                route: routeElement.attr('data-route')[0],
                routeType: routeElement.attr('data-route-type')[0],
              }
            },
            addTransform() {
              var rotation = +routeElement.attr('data-rotation'),
                flipped = +routeElement.attr('data-flipped'),
                degrees = rotation * 90,
                scale = !!flipped ? -1 : 1,
                transformValue =
                  'rotate(' + degrees + 'deg) scaleX(' + scale + ')';

              routeElement.style({
                transform: transformValue
              });
            },
            flip: function(possibilities) {
              var next = +!+routeElement.attr('data-flipped');
              routeElement.attr('data-flipped', next);
              if (possibilities) {
                this.rotate(1, possibilities);
              }

              this.addTransform();
            },
            getNormalizedRotation(rotation) {
              var normalized = rotation % 4;

              return normalized < 0 ? normalized + 4 : normalized;
            },
            rotate: function(direction, possibilities) {
              var current = +routeElement.attr('data-rotation'),
                next = current + direction;

              if (possibilities) {
                var isFlipped = !!+routeElement.attr('data-flipped');
                var rotations = isFlipped
                  ? possibilities.flippedRotations
                  : possibilities.normalRotations;

                var counter = 0;
                while (
                  counter < 4 &&
                  rotations.indexOf(this.getNormalizedRotation(next)) === -1
                ) {
                  next = next + direction;
                  counter++;
                }
              }

              routeElement.attr('data-rotation', next);
              this.addTransform();
            },
            makeInteractive: function(selectedRoute, possibilities, onDone) {
              fieldElement.addClass('interactive');
              eventHandles.attachQuery(
                fieldElement.query('.interactions .left'),
                'fieldInteractions',
                'click',
                function(e) {
                  dojo.stopEvent(e);
                  this.rotate(-1, possibilities);
                }.bind(this)
              );
              eventHandles.attachQuery(
                fieldElement.query('.interactions .right'),
                'fieldInteractions',
                'click',
                function(e) {
                  dojo.stopEvent(e);
                  this.rotate(1, possibilities);
                }.bind(this)
              );
              if (
                possibilities.flippedRotations.length === 0 ||
                possibilities.normalRotations.length === 0
              ) {
                fieldElement
                  .query('.interactions .flip')
                  .style('display', 'none');
              } else {
                fieldElement
                  .query('.interactions .flip')
                  .style('display', 'inherit');
                eventHandles.attachQuery(
                  fieldElement.query('.interactions .flip'),
                  'fieldInteractions',
                  'click',
                  function(e) {
                    dojo.stopEvent(e);
                    this.flip(possibilities);
                  }.bind(this)
                );
              }
              eventHandles.attachQuery(
                fieldElement.query('.interactions .done'),
                'fieldInteractions',
                'click',
                function(e) {
                  dojo.stopEvent(e);
                  onDone(this, selectedRoute);
                }.bind(this)
              );
            },
            turnOffInteractive: function() {
              fieldElement.removeClass('interactive');
            }
          };
        },
        getAllFields: function() {
          return this.getFields();
        },
        getSpecificFields: function(coords) {
          if (coords.length === 0) {
            return this.getFields(false);
          }
          var selectors = coords.map(function(c) {
            return '.field-' + c.x + '-' + c.y;
          });

          return this.getFields(selectors.join(','));
        },
        getFields: function(query) {
          var fieldElements =
            query === false
              ? new dojo.NodeList()
              : playerBoardElement.query(query || '.field');
          var playerBoard = this;
          return {
            activate: function(selectedDie, onFieldSelected) {
              fieldElements.addClass('available');
              eventHandles.attachQuery(
                fieldElements,
                'placeDieOnField',
                'click',
                function(e) {
                  if (selectedDie !== null) {
                    var selectedField = dojo.query(e.target).closest('.field'),
                      row = selectedField.attr('data-row')[0],
                      col = selectedField.attr('data-col')[0];

                    onFieldSelected(
                      playerBoard.getField(row, col),
                      selectedDie
                    );
                  }
                }
              );
            },
            deactivate: function() {
              fieldElements.removeClass('available');
              eventHandles.detachAll('placeDieOnField');
            },
            reset: function() {
              fieldElements.removeClass('interactive');
              var routeElements = fieldElements.query('.route');
              [
                'set',
                'route-0',
                'route-1',
                'route-2',
                'route-3',
                'route-4',
                'route-5',
                'route-6',
                'route-7',
                'route-8',
                'route-type-0',
                'route-type-1',
              ].forEach(routeElements.removeClass.bind(routeElements));
              routeElements.style({ transform: 'none' });
              routeElements.removeAttr("data-route")
              routeElements.removeAttr("data-route-type")
              routeElements.removeAttr("data-rotation")
              routeElements.removeAttr("data-flipped")
              
              var roundElements = fieldElements.query('.round-number');
              roundElements.innerHTML("");
              
              eventHandles.detachAll('fieldInteractions');
            }
          };
        },
        activateAvailableFields: function(
          availableFields,
          selectedDie,
          onFieldSelected
        ) {
          this.getAllFields().deactivate();

          var fields = this.getSpecificFields(availableFields);
          fields.activate(selectedDie, onFieldSelected);
        },
        deactivateAllFields: function() {
          this.getAllFields().deactivate();
        },
        activateSpecialRoutes: function(onSelectedSpecialRoute) {
          var specialRoutes = playerBoardElement.query('.special-routes');
          specialRoutes.addClass('active');
          eventHandles.attachQuery(
            playerBoardElement.query('.special-route'),
            'specialRoutesUse',
            'click',
            function(e) {
              var route = dojo.attr(e.target, 'data-route');

              specialRoutes.query('.special-route').removeClass('selected');
              var selectedSpecialRoute = dojo.query(e.target);
              selectedSpecialRoute.addClass('selected');

              onSelectedSpecialRoute({ route: route, routeType: ROUTE_TYPE_SPECIAL });
            }
          );
        },
        deactivateSpecialRoutes: function() {
          var specialRoutes = playerBoardElement.query('.special-routes');
          specialRoutes.removeClass('active');
          this.unselectAllSpecialRoutes();
          eventHandles.detachAll('specialRoutesUse');
        },
        useSpecialRoute: function(specialRoute) {
          var specialRoute = playerBoardElement.query(
            '.special-routes .special-route-' + specialRoute.route
          );
          specialRoute.addClass('used');
          specialRoute.removeClass('selected');
        },
        unselectAllSpecialRoutes: function() {
          playerBoardElement.query('.special-route').removeClass('selected');
        },
        unuseAllSpecialRoutes: function() {
          playerBoardElement.query('.special-route').removeClass('used');
        },
        clearInteractiveField: function() {
          this.getFields('.field.interactive').reset();
          },
          setScore: function(score) {
            var scoreElement = playerBoardElement.query('.score');
  
            scoreElement.query(".score-part.total").innerHTML(score.total);
            scoreElement.query(".score-part.exits").innerHTML(score.exits.total);
            scoreElement.query(".score-part.highway").innerHTML(score.longestHighway.total);
            scoreElement.query(".score-part.railroad").innerHTML(score.longestRailroad.total);
            scoreElement.query(".score-part.central").innerHTML(score.centralFields.total);
            scoreElement.query(".score-part.errors").innerHTML(score.errors.total);
        }
      };
    }
  };

  return declare('bgagame.railroadink', ebg.core.gamegui, {
    constructor: function() {
      console.log('railroadink constructor');

      // Here, you can init the global variables of your user interface
      // Example:
      // this.myGlobalValue = 0;
    },

    /*
            setup:
            
            This method must set up the game user interface according to current game situation specified
            in parameters.
            
            The method is called each time the game interface is displayed to a player, ie:
            _ when the game starts
            _ when a player refreshes the game page (F5)
            
            "gamedata" argument contains all datas retrieved by your "getAllDatas" PHP method.
        */

    setup: function(gamedata) {
      console.log('Starting game setup');
      console.log(gamedata);
      dojo.place(
        'player-area-' + this.player_id,
        dojo.query('.player-sheets')[0],
        'first'
      );

      this.setRound(gamedata.round);

      // Setting up player boards
      for (var player_id in gamedata.players) {
        var player = gamedata.players[player_id];
      }
      var usedDice = gamedata.usedDice[this.player_id] || {};
      
      this.dice.set(gamedata.dice, Object.values(usedDice));
      this.boards.set(gamedata.boards);
      
      var usedSpecial = gamedata.usedSpecial[this.player_id];
      var currentPlayerBoard = this.boards.getPlayerBoard(this.player_id);
      this.setSpecialRoutesUseForPlayerBoard(usedSpecial, currentPlayerBoard);

      // Setup game notifications to handle (see "setupNotifications" method below)
      this.setupNotifications();

      console.log('Ending game setup');
    },

    ///////////////////////////////////////////////////
    //// Game & client states

    // onEnteringState: this method is called each time we are entering into a new game state.
    //                  You can use this method to perform some user interface changes at this moment.
    //
    onEnteringState: function(stateName, msg) {
      console.log('Entering state: ' + stateName);

      switch (stateName) {
        case 'useDice':
          console.log(msg.args._private.availableFields);

          this.availableFields = Object.values(
            msg.args._private.availableFields
          );
          this.setupDiceUse();
          break;
        case 'dummmy':
          break;
      }
    },

    // onLeavingState: this method is called each time we are leaving a game state.
    //                 You can use this method to perform some user interface changes at this moment.
    //
    onLeavingState: function(stateName) {
      console.log('Leaving state: ' + stateName);

      switch (stateName) {
        case 'useDice':
          this.availableFields = null;
          break;
        /* Example:
            
            case 'myGameState':
            
                // Hide the HTML block we are displaying only during this game state
                dojo.style( 'my_html_block_id', 'display', 'none' );
                
                break;
           */

        case 'dummmy':
          break;
      }
    },

    // onUpdateActionButtons: in this method you can manage "action buttons" that are displayed in the
    //                        action status bar (ie: the HTML links in the status bar).
    //
    onUpdateActionButtons: function(stateName, args) {
      console.log('onUpdateActionButtons: ' + stateName);

      if (this.isCurrentPlayerActive()) {
        switch (
          stateName
          ) {
              case "useDice":
                this.addActionButton( 'custom_undo_button', _('undo'), 'onUndo' );
                this.addActionButton( 'done_button', _('done'), 'onDone' );
                break;
        }
      }
    },

    ///////////////////////////////////////////////////
    //// Utility methods

    /*
        
            Here, you can defines some utility methods that you can use everywhere in your javascript
            script.
        
        */
    dice: dice,
    boards: boards,
    setRound: function(round){
      dojo.query("#round").query("span").innerHTML(round);
    },
    canUseSpecialRoutes: function(){
      return !this.specialRouteUsedThisTurn && this.usedSpecialRoutesNumber < 3;
    },
    setupDiceUse: function() {
      this.dice.activate(this.routeSelected.bind(this));

      if (this.canUseSpecialRoutes()){
        this.boards
          .getPlayerBoard(this.player_id)
          .activateSpecialRoutes(this.routeSelected.bind(this));
      }
    },
    filterAvailableFieldsByRoute(selectedRoute) {      
      return this.availableFields.filter(function(field) {
        $routePossibilities = Object.values(field.possibleRoutes);
        $currentRoutePossibilities = $routePossibilities.filter(function(
          possibility
        ) {
          return (
            +possibility.route === +selectedRoute.route &&
            +possibility.routeType === +selectedRoute.routeType &&
            (possibility.normalRotations.length > 0 ||
              possibility.flippedRotations.length > 0)
          );
        });

        return $currentRoutePossibilities.length > 0;
      });
    },
    routeSelected: function(selectedRoute) {
      var playerBoard = this.boards.getPlayerBoard(this.player_id);

      if (selectedRoute.routeType === ROUTE_TYPE_SPECIAL) {
        this.dice.unselectAll();
      } else {
        playerBoard.unselectAllSpecialRoutes();
      }

      playerBoard.clearInteractiveField();

      var filteredFields = this.filterAvailableFieldsByRoute(selectedRoute);

      playerBoard.activateAvailableFields(
        filteredFields,
        selectedRoute,
        this.placedOnField.bind(this)
      );
    },
    placedOnField: function(field, selectedRoute) {
      var playerBoard = this.boards.getPlayerBoard(this.player_id);
      playerBoard.clearInteractiveField();

      var fieldCoordinates = field.getData();
      var possibleRoutes = this.availableFields.filter(function(f) {
        return +f.x === +fieldCoordinates.x && +f.y === +fieldCoordinates.y;
      })[0].possibleRoutes;

      var currentRoutePossibilities = possibleRoutes.filter(function(p) {
        return (
          +p.route === +selectedRoute.route &&
          +p.routeType === +selectedRoute.routeType
        );
      })[0];

      var isFlipped = false,
        rotation = currentRoutePossibilities.normalRotations[0];
      if (currentRoutePossibilities.normalRotations.length === 0) {
        isFlipped = true;
        rotation = currentRoutePossibilities.flippedRotations[0];
      }

      field.set({
        route: selectedRoute.route,
        routeType: selectedRoute.routeType,
        rotation: rotation,
        flipped: isFlipped
      });
      field.makeInteractive(
        selectedRoute,
        currentRoutePossibilities,
        this.onConfirm.bind(this)
      );
    },
    onConfirm: function(field, selectedRoute) {
      field.turnOffInteractive();

      var playerBoard = this.boards.getPlayerBoard(this.player_id);
      playerBoard.deactivateAllFields();

      var data = field.getData();

      if (+selectedRoute.routeType === ROUTE_TYPE_REGULAR) {
        this.dice.useDie(selectedRoute);
        data.dieId = selectedRoute.id
      } else {
        var playerBoard = this.boards
          .getPlayerBoard(this.player_id);

        playerBoard.useSpecialRoute(selectedRoute);
        playerBoard.deactivateSpecialRoutes();
        this.specialRouteUsedThisTurn = true;
        this.usedSpecialRoutesNumber++;

        data.specialId = selectedRoute.route;
      }

      if (this.checkAction('drawRoute')){
        data.lock = true;
        this.ajaxcall(
          'railroadink/railroadink/drawRoute.html',
          data,
          this,
          function() {}
        );
      }

      this.setupDiceUse();
    },
    setSpecialRoutesUseForPlayerBoard: function(usedSpecial, board){
      board.unselectAllSpecialRoutes();
      board.unuseAllSpecialRoutes();

      if (!usedSpecial){
        usedSpecial = { routes: {} };
      }

      Object.values(usedSpecial.routes).forEach(function(special){
        board.useSpecialRoute({ route: special.id });
      });
      this.usedSpecialRoutesNumber = Object.values(usedSpecial.routes).length;
      this.specialRouteUsedThisTurn = usedSpecial.currentRoundUsed;

      if (this.canUseSpecialRoutes()){
        board.activateSpecialRoutes(this.routeSelected.bind(this));
      }
    },
    ///////////////////////////////////////////////////
    //// Player's action

    /*
        
            Here, you are defining methods to handle player's action (ex: results of mouse click on 
            game objects).
            
            Most of the time, these methods:
            _ check the action is possible at this game state.
            _ make a call to the game server
        
            */
    
    onUndo: function(){
      this.checkAction('undoUseDice');
      this.ajaxcall(
        'railroadink/railroadink/undoUseDice.html',
        { lock: true },
        this,
        function() {}
      );
    },

       
    onDone: function(){
      this.checkAction('doneUseDice');
      this.ajaxcall(
        'railroadink/railroadink/doneUseDice.html',
        { lock: true },
        this,
        function() {}
      );
    },

    /* Example:
        
        onMyMethodToCall1: function( evt )
        {
            console.log( 'onMyMethodToCall1' );
            
            // Preventing default browser reaction
            dojo.stopEvent( evt );

            // Check that this action is possible (see "possibleactions" in states.inc.php)
            if( ! this.checkAction( 'myAction' ) )
            {   return; }

            this.ajaxcall( "/railroadink/railroadink/myAction.html", { 
                                                                    lock: true, 
                                                                    myArgument1: arg1, 
                                                                    myArgument2: arg2,
                                                                    ...
                                                                 }, 
                         this, function( result ) {
                            
                            // What to do after the server call if it succeeded
                            // (most of the time: nothing)
                            
                         }, function( is_error) {

                            // What to do after the server call in anyway (success or failure)
                            // (most of the time: nothing)

                         } );        
        },        
        
        */

    ///////////////////////////////////////////////////
    //// Reaction to cometD notifications

    /*
            setupNotifications:
            
            In this method, you associate each of your game notifications with your local method to handle it.
            
            Note: game notification names correspond to "notifyAllPlayers" and "notifyPlayer" calls in
                  your railroadink.game.php file.
        
        */
    setupNotifications: function() {
      console.log('notifications subscriptions setup');

      dojo.subscribe( 'newAvailableFields', this, "notif_newAvailableFields");
      dojo.subscribe( 'newRound', this, "notif_newRound");
      dojo.subscribe( 'undoUseDice', this, "notif_undoUseDice");
      dojo.subscribe( 'updatePlayerBoard', this, "notif_updatePlayerBoard");

      // TODO: here, associate your game notifications with local methods

      // Example 1: standard notification handling
      // dojo.subscribe( 'cardPlayed', this, "notif_cardPlayed" );

      // Example 2: standard notification handling + tell the user interface to wait
      //            during 3 seconds after calling the method in order to let the players
      //            see what is happening in the game.
      // dojo.subscribe( 'cardPlayed', this, "notif_cardPlayed" );
      // this.notifqueue.setSynchronous( 'cardPlayed', 3000 );
      //
    },

    notif_updatePlayerBoard: function(notif){
      var board = this.boards.getPlayerBoard(notif.args.board.playerId)
      board.resetFields(notif.args.board.fields);
      board.setScore(notif.args.board.score);

      this.scoreCtrl[ notif.args.board.playerId ].setValue( notif.args.board.score.total );
    },

    notif_undoUseDice: function(notif){
      this.dice.unuseAll();
      var board = this.boards.getPlayerBoard(this.player_id)
      board.resetFields(notif.args.board.fields);
      
      this.availableFields = Object.values(
        notif.args.availableFields
      );
        
      this.setSpecialRoutesUseForPlayerBoard(notif.args.usedSpecial[this.player_id], board);
    },

    notif_newAvailableFields: function(notif){
      console.log( 'notif_newAvailableFields' );
      console.log( notif );

      this.availableFields = Object.values(
        notif.args.availableFields
      );
    },

    notif_newRound: function(notif){
      console.log( 'notif_newRound' );
      this.setRound(notif.args.round);
      
      var usedDice = notif.args.usedDice[this.player_id] || {};
      this.dice.reset();
      this.dice.set(notif.args.dice, Object.values(usedDice));
    },

    // TODO: from this point and below, you can write your game notifications handling methods

    /*
        Example:
        
        notif_cardPlayed: function( notif )
        {
            console.log( 'notif_cardPlayed' );
            console.log( notif );
            
            // Note: notif.args contains the arguments specified during you "notifyAllPlayers" / "notifyPlayer" PHP call
            
            // TODO: play the card in the user interface.
        },    
        
        */
  });
});
