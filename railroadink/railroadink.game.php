<?php

/**
 *------
 * BGA framework: © Gregory Isabelli <gisabelli@boardgamearena.com> & Emmanuel Colin <ecolin@boardgamearena.com>
 * railroadink implementation : © <Your name here> <Your email address here>
 * 
 * This code has been produced on the BGA studio platform for use on http://boardgamearena.com.
 * See http://en.boardgamearena.com/#!doc/Studio for more information.
 * -----
 * 
 * railroadink.game.php
 *
 * This is the main file for your game logic.
 *
 * In this PHP file, you are going to defines the rules of the game.
 *
 */


require_once(APP_GAMEMODULE_PATH . 'module/table/table.game.php');
require_once('modules/RRIField.php');
require_once('modules/RRINode.php');
require_once('modules/RRIGraph.php');
require_once('modules/RRIBoard.php');


class railroadink extends Table
{

    const GAME_ROUND_GLOBAL = 'game_round';

    function __construct()
    {
        // Your global variables labels:
        //  Here, you can assign labels to global variables you are using for this game.
        //  You can use any number of global variables with IDs between 10 and 99.
        //  If your game has options (variants), you also have to associate here a label to
        //  the corresponding ID in gameoptions.inc.php.
        // Note: afterwards, you can get/set the global variables with getGameStateValue/setGameStateInitialValue/setGameStateValue
        parent::__construct();

        $this->initGameStateLabels(array(
            self::GAME_ROUND_GLOBAL => 10
        ));
    }

    protected function getGameName()
    {
        // Used for translations and stuff. Please do not modify.
        return "railroadink";
    }

    /*
        setupNewGame:
        
        This method is called only once, when a new game is launched.
        In this method, you must setup the game according to the game rules, so that
        the game is ready to be played.
    */
    protected function setupNewGame($players, $options = array())
    {
        // Set the colors of the players with HTML color code
        // The default below is red/green/blue/orange/brown
        // The number of colors defined here must correspond to the maximum number of players allowed for the gams
        $gameinfos = $this->getGameinfos();
        $default_colors = $gameinfos['player_colors'];

        // Create players
        // Note: if you added some extra field on "player" table in the database (dbmodel.sql), you can initialize it there.
        $sql = "INSERT INTO player (player_id, player_color, player_canal, player_name, player_avatar) VALUES ";
        $values = array();
        foreach ($players as $player_id => $player) {
            $color = array_shift($default_colors);
            $values[] = "('" . $player_id . "','$color','" . $player['player_canal'] . "','" . addslashes($player['player_name']) . "','" . addslashes($player['player_avatar']) . "')";
        }
        $sql .= implode(',', $values);
        $this->DbQuery($sql);
        $this->reattributeColorsBasedOnPreferences($players, $gameinfos['player_colors']);
        $this->reloadPlayersBasicInfos();

        /************ Start the game initialization *****/

        // Init global values with their initial values
        $this->setGameStateInitialValue( self::GAME_ROUND_GLOBAL, 0 );

        // Init game statistics
        // (note: statistics used in this file must be defined in your stats.inc.php file)
        //$this->initStat( 'table', 'table_teststat1', 0 );    // Init a table statistics
        //$this->initStat( 'player', 'player_teststat1', 0 );  // Init a player statistics (for all players)

        // TODO: setup the initial game situation here

        /************ End of the game initialization *****/
    }

    /*
        getAllDatas: 
        
        Gather all informations about current game situation (visible by the current player).
        
        The method is called each time the game interface is displayed to a player, ie:
        _ when the game starts
        _ when a player refreshes the game page (F5)
    */
    protected function getAllDatas()
    {
        $result = array();

        $current_player_id = $this->getCurrentPlayerId();    // !! We must only return informations visible by this player !!

        // Get information about players
        // Note: you can retrieve some extra field you added for "player" table in "dbmodel.sql" if you need it.
        $sql = "SELECT player_id id, player_score score, player_is_multiactive is_multiactive FROM player ";
        $players = $this->getCollectionFromDb($sql);
        
        $result['dice'] = $this->getCurrentDiceValues();
        
        $result["boards"] = [];
        $boards = $this->getPlayerBoards();
        
        $isCurrentPlayerActive = $players[$current_player_id]["is_multiactive"] == 1;

        foreach ($boards as $playerId => $board) {
            if ($playerId != $current_player_id){
                $board->removeUnconfirmedFields();

                $isPlayerActive = $players[$playerId]["is_multiactive"] == 1;
                
                if ($isCurrentPlayerActive || $isPlayerActive){
                    $board->removeFieldsForRound($this->getGameStateValue(self::GAME_ROUND_GLOBAL));
                }
            }
            
            $serializedBoard = $board->serialize();
            $result["boards"][] = $serializedBoard;
            $players[$playerId]["score"] = $serializedBoard["score"]["total"];
        }
        $result['players'] = $players;
        
        $usedDice = $this->getDiceUsage();
        $result["usedDice"] =  $usedDice;
        $result["usedSpecial"] = $this->getSpecialUsage();
        $result["round"] = $this->getGameStateValue(self::GAME_ROUND_GLOBAL);
   
        return $result;
    }

    /*
        getGameProgression:
        
        Compute and return the current game progression.
        The number returned must be an integer beween 0 (=the game just started) and
        100 (= the game is finished or almost finished).
    
        This method is called each time we are in a game state with the "updateGameProgression" property set to true 
        (see states.inc.php)
    */
    function getGameProgression()
    {
        $round = $this->getGameStateValue(self::GAME_ROUND_GLOBAL);
        return $round / 7;
    }


    //////////////////////////////////////////////////////////////////////////////
    //////////// Utility functions
    ////////////    

    /*
        In this space, you can put any utility methods useful for your game logic
    */

    function resetDiceUse() {
        $sql = "DELETE FROM player_die_use";
        $this->DbQuery($sql);
    }

    function rollDiceAndPersist()
    {

        // first clear data in dice table
        $sql = "DELETE FROM dice";
        $this->DbQuery($sql);


        // insert new values for dice
        $sql = "INSERT INTO dice (id, route, route_type) VALUES";
        $values = array();
        for ($id = 0; $id < 4; $id++) {
            if ($id < 3) {
                $values[] = "('" . $id . "'," . bga_rand(0, 5) . ", 0)";
            } else {
                $values[] = "('" . $id . "'," . bga_rand(6, 8) . ", 0)";
            }
        }
        $sql .= implode(',', $values);
        $this->DbQuery($sql);
    }

    function getCurrentDiceValues()
    {
        $sql = "SELECT id, route, route_type routeType FROM dice";
        return $this->getObjectListFromDB($sql);
    }

    function getDiceUsage()
    {
        $sql = "SELECT die_id, player_id FROM player_die_use";
        $dieUsage = $this->getObjectListFromDB($sql);

        $usedDice = [];
        foreach ($dieUsage as $usage) {
            $dieId = intval($usage["die_id"]);
            $usedDice[$usage["player_id"]][$dieId] = [ "id" => intval($usage["die_id"]) ];
        }

        return $usedDice;
    }
    
    function getSpecialUsage()
    {
        $sql = "SELECT special_id, player_id, round FROM player_special_use";
        $specialUsage = $this->getObjectListFromDB($sql);
        $round = $this->getGameStateValue(self::GAME_ROUND_GLOBAL);

        $usedSpecial = [];
        foreach ($specialUsage as $usage) {
            $specialId = intval($usage["special_id"]);
            if ($usage["round"] == $round){
                $usedSpecial[$usage["player_id"]]["currentRoundUsed"] = true;
            }
            $usedSpecial[$usage["player_id"]]["routes"][$specialId] = [ 
                "id" => intval($usage["special_id"]),
                "round" => intval($usage["round"])
            ];
        }

        return $usedSpecial;
    }

    function useDie($dieId, $playerId){
        $sql = "INSERT INTO player_die_use (die_id, player_id) VALUES (".$dieId.", ".$playerId.")";
        $this->DbQuery($sql);
    }
    
    function useSpecial($specialId, $playerId){
        $sql = "INSERT INTO player_special_use (special_id, player_id) VALUES (".$specialId.", ".$playerId.")";
        $this->DbQuery($sql);
    }

    private function getPlayerBoards($playerId = null)
    {
        $sql = "SELECT player_id, x, y, route, route_type, is_flipped, rotate, `round` FROM player LEFT OUTER JOIN board USING (player_id)";
        if ($playerId !== null) {
            $sql .= " WHERE player_id = " . $playerId;
        }
        $fields = $this->getObjectListFromDB($sql);

        $boardsByPlayer = [];

        foreach ($fields as $field) {
            if (!array_key_exists($field["player_id"], $boardsByPlayer)) {
                $board = new RRIBoard($field["player_id"]);
                $boardsByPlayer[$field["player_id"]] = $board;
            } else {
                $board = $boardsByPlayer[$field["player_id"]];
            }
            if ($field["route"] !== null) {
                $board->addField($field);
            }
        }

        if ($playerId !== null) {
            return $boardsByPlayer[$playerId];
        }

        return $boardsByPlayer;
    }

    private function addFieldToPlayerBoard($playerId, $route, $routeType, $rotation, $flipped, $x, $y)
    {
        $sql = "INSERT INTO board (player_id, x, y, route, route_type, is_flipped, rotate)";

        $values = [
            $playerId, $x, $y, $route, $routeType, $flipped, $rotation
        ];

        $sql .= "VALUES (" . implode(",", $values) . ")";

        $this->DbQuery($sql);
    }

    private function canPlayerFinish($usedDice){
        //TODO check if no available moves
        return sizeof($usedDice) === 4;
    }

    private function isPlayerActive($playerId){
        return array_search($playerId, $this->gamestate->getActivePlayerList()) !== false;
    }

    private function getAllInactivePlayers(){
        $sql = "SELECT player_id, player_name FROM player WHERE player_is_multiactive = 0";
        
        return $this->getObjectListFromDB( $sql );
    }

    private function updateCurrentRoundFields($playerId){
        $round = $this->getGameStateValue(self::GAME_ROUND_GLOBAL);

        $sql = "UPDATE board SET round = ".$round." WHERE player_id = ".$playerId." AND round IS NULL";
        $this->DbQuery($sql);
        
        $sql = "UPDATE player_special_use SET round = ".$round." WHERE player_id = ".$playerId." AND round IS NULL";
        $this->DbQuery($sql);
    }

    private function resetPlayerRound($playerId){
        $sql = "DELETE FROM player_die_use WHERE player_id = ".$playerId;
        $this->DbQuery($sql);

        $sql = "DELETE FROM board WHERE player_id = ".$playerId." AND round IS NULL";
        $this->DbQuery($sql);
        
        $sql = "DELETE FROM player_special_use WHERE player_id = ".$playerId." AND round IS NULL";
        $this->DbQuery($sql);
    } 

    function dbGetScore($player_id) {
        return $this->getUniqueValueFromDB("SELECT player_score FROM player WHERE player_id='$player_id'");
    }

    function dbSetScore($player_id, $count) {
        $this->DbQuery("UPDATE player SET player_score='$count' WHERE player_id='$player_id'");
    }

    //////////////////////////////////////////////////////////////////////////////
    //////////// Player actions
    //////////// 

    /*
        Each time a player is doing some game action, one of the methods below is called.
        (note: each method below must match an input method in railroadink.action.php)
    */

    function doneUseDice(){
        $this->checkAction('doneUseDice');
        
        $playerId = $this->getCurrentPlayerId();
        $playerName = $this->getCurrentPlayerName();
        $diceUsage = $this->getDiceUsage();
        $usedDice = isset($diceUsage[$playerId]) ? $diceUsage[$playerId] : [];
        
        if (!$this->canPlayerFinish($usedDice)) {
            throw new BgaUserException( self::_('You must use all dice if possible') );
        }

        $this->updateCurrentRoundFields($playerId);

        $this->notifyAllPlayers("Player finished", clienttranslate( '${player_name} finished drawing routes'), [
            "player_name" => $playerName
        ] );
        
        $boards = $this->getPlayerBoards();
        
        $this->notifyPlayer( $playerId, "updatePlayerBoard", 'Round number is written on newly drawn pieces. You cannot undo this round any more.', [
            "board" => $boards[$playerId]->serialize(),        
        ] );

        $inactivePlayers = $this->getAllInactivePlayers();
        foreach ($inactivePlayers as $inactivePlayer) {
            if ($inactivePlayer["player_id"] != $playerId){
                $this->notifyPlayer( $playerId, "updatePlayerBoard", 'You can now see what ${player_name} have drawn this round', [
                    "board" => $boards[$inactivePlayer["player_id"]]->serialize(),
                    "player_name" => $inactivePlayer["player_name"],
                ] );

                $this->notifyPlayer( $inactivePlayer["player_id"], "updatePlayerBoard", 'You can now see what ${player_name} have drawn this round', [
                    "board" => $boards[$playerId]->serialize(),
                    "player_name" => $playerName,
                ] );
            }
        }

        $this->gamestate->setPlayerNonMultiactive( $playerId, "nextRound" );
    }

    function undoUseDice() {
        $this->checkAction('undoUseDice');

        $playerId = $this->getCurrentPlayerId();
        if (!$this->isPlayerActive($playerId)){
            throw new BgaUserException( self::_('You cannot undo drawing routes if you finalize them.') );
        }

        $this->resetPlayerRound($playerId);

        $board = $this->getPlayerBoards($playerId);

        $fields = $board->getAvailableFields();
        $availableFields = array_map(function ($field) {
            $field["possibleRoutes"] = RRIField::getPossibleRoutes($field["restrictions"]);
            return $field;
        }, $fields);

        $this->notifyPlayer( $playerId, "undoUseDice", "", [
            "board" => $board->serialize(),
            "availableFields" => $availableFields,
            "usedSpecial" => $this->getSpecialUsage()
        ] );
    }
    
    function drawRoute($route, $routeType, $rotation, $flipped, $x, $y, $dieId, $specialId)
    {
        // Check that this is the player's turn and that it is a "possible action" at this game state (see states.inc.php)
        $this->checkAction('drawRoute');
        
        $player_id = $this->getCurrentPlayerId();
        
        $board = $this->getPlayerBoards($player_id);
        $fieldRaw = [
            "route" => $route,
            "route_type" => $routeType,
            "rotate" => $rotation,
            "is_flipped" => $flipped,
            "x" => $x,
            "y" => $y,
            "round" => null
        ];
        $field = new RRIField($fieldRaw);
        
        if (!$board->checkIfLegal($field)){
            throw new BgaUserException( self::_('This placement is not legal') );
        }
        
        $diceUsage = $this->getDiceUsage();
        $usedDice = isset($diceUsage[$player_id]) ? $diceUsage[$player_id] : [];
        if ($dieId !== null){
            if (isset($usedDice[$dieId])){
                throw new BgaUserException( self::_('This die is already used') );
            }
        }
        
        $usedSpecial = isset($this->getSpecialUsage()[$player_id]) ? $this->getSpecialUsage()[$player_id] : [];
        if ($specialId !== null) {
            if (isset($usedSpecial["routes"][$specialId])){
                throw new BgaUserException( self::_('This special route is already used') );
            }
            if (isset($usedSpecial["currentRoundUsed"]) && $usedSpecial["currentRoundUsed"] ){
                throw new BgaUserException( self::_('Special route is already used in this round') );
            }
            if (isset($usedSpecial["routes"]) && sizeof($usedSpecial["routes"]) === 3){
                throw new BgaUserException( self::_('Three special routes already used in a game') );
            }
        }
        
        $this->addFieldToPlayerBoard($player_id, $route, $routeType, $rotation, $flipped, $x, $y);
        $board->addField($fieldRaw);

        $message = clienttranslate('${player_name} draws route at ${x}-${y}');
        if ($dieId !== null){
            $message = clienttranslate('${player_name} draws route at ${x}-${y} from die ${dieId}');
            $this->useDie($dieId, $player_id);
        }
        
        if ($specialId !== null){
            $message = clienttranslate('${player_name} draws special route at ${x}-${y}');
            $this->useSpecial($specialId, $player_id);
        }

        $fields = $board->getAvailableFields();
        $availableFields = array_map(function ($field) {
            $field["possibleRoutes"] = RRIField::getPossibleRoutes($field["restrictions"]);
            return $field;
        }, $fields);

        //TODO check if some dice are left but no legal placement
        $usedDice[] =$dieId;
        $canFinish = $this->canPlayerFinish($usedDice);
       
        $this->notifyPlayer( $player_id, "newAvailableFields", "", [
            "availableFields" => $availableFields,
            "canFinish"=> $canFinish
        ] );
    }


    //////////////////////////////////////////////////////////////////////////////
    //////////// Game state arguments
    ////////////

    function argUseDice()
    {
        $boards = $this->getPlayerBoards();
        $usedDice = $this->getDiceUsage();
        
        $privateData = [];

        foreach ($boards as $playerId => $board) {
            $fields = $board->getAvailableFields();
            $availableFields = array_map(function ($field) {
                $field["possibleRoutes"] = RRIField::getPossibleRoutes($field["restrictions"]);
                return $field;
            }, $fields);

            $privateData[$playerId] = [
                "availableFields" => $availableFields,
                "canFinish" => isset($usedDice[$playerId]) && $this->canPlayerFinish($usedDice[$playerId])
            ];
        }
        
        $args =[ 
            "_private" => $privateData
        ];

        return $args;
    }

    //////////////////////////////////////////////////////////////////////////////
    //////////// Game state actions
    ////////////

    function stUseDice()
    {
        $this->incGameStateValue(self::GAME_ROUND_GLOBAL, 1);
        $this->gamestate->setAllPlayersMultiactive();
        $this->rollDiceAndPersist();
        $this->resetDiceUse();
        $this->notifyAllPlayers( "newRound", clienttranslate("New round starts."), [
            "round" => $this->getGameStateValue(self::GAME_ROUND_GLOBAL),
            "dice" => $this->getCurrentDiceValues(),
            "usedDice" => []
        ]);
    }

    function stCheckGameEnd() {
        $round = $this->getGameStateValue(self::GAME_ROUND_GLOBAL);
        
        if ($round == 7) {
            $boards = $this->getPlayerBoards();

            foreach ($boards as $playerId => $board) {
                $score = $board->getScore();
                $this->dbSetScore($playerId, $score["total"]);
            }

            $this->gamestate->nextState( "end" );
            return;
        }

        $this->gamestate->nextState( "nextRound" );
    }

    //////////////////////////////////////////////////////////////////////////////
    //////////// Zombie
    ////////////

    /*
        zombieTurn:
        
        This method is called each time it is the turn of a player who has quit the game (= "zombie" player).
        You can do whatever you want in order to make sure the turn of this player ends appropriately
        (ex: pass).
    */

    function zombieTurn($state, $active_player)
    {
        $statename = $state['name'];

        if ($state['type'] === "activeplayer") {
            switch ($statename) {
                default:
                    $this->gamestate->nextState("zombiePass");
                    break;
            }

            return;
        }

        if ($state['type'] === "multipleactiveplayer") {
            // Make sure player is in a non blocking status for role turn
            $this->gamestate->setPlayerNonMultiactive($active_player, '');

            return;
        }

        throw new feException("Zombie mode not supported at this game state: " . $statename);
    }

    ///////////////////////////////////////////////////////////////////////////////////:
    ////////// DB upgrade
    //////////

    /*
        upgradeTableDb:
        
        You don't have to care about this until your game has been published on BGA.
        Once your game is on BGA, this method is called everytime the system detects a game running with your old
        Database scheme.
        In this case, if you change your Database scheme, you just have to apply the needed changes in order to
        update the game database and allow the game to continue to run with your new version.
    
    */

    function upgradeTableDb($from_version)
    {
        // $from_version is the current version of this game database, in numerical form.
        // For example, if the game was running with a release of your game named "140430-1345",
        // $from_version is equal to 1404301345

        // Example:
        //        if( $from_version <= 1404301345 )
        //        {
        //            // ! important ! Use DBPREFIX_<table_name> for all tables
        //
        //            $sql = "ALTER TABLE DBPREFIX_xxxxxxx ....";
        //            $this->applyDbUpgradeToAllDB( $sql );
        //        }
        //        if( $from_version <= 1405061421 )
        //        {
        //            // ! important ! Use DBPREFIX_<table_name> for all tables
        //
        //            $sql = "CREATE TABLE DBPREFIX_xxxxxxx ....";
        //            $this->applyDbUpgradeToAllDB( $sql );
        //        }
        //        // Please add your future database scheme changes here
        //
        //


    }
}
