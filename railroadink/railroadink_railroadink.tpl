{OVERALL_GAME_HEADER}

<!-- 
--------
-- BGA framework: © Gregory Isabelli <gisabelli@boardgamearena.com> & Emmanuel Colin <ecolin@boardgamearena.com>
-- railroadink implementation : © <Your name here> <Your email address here>
-- 
-- This code has been produced on the BGA studio platform for use on http://boardgamearena.com.
-- See http://en.boardgamearena.com/#!doc/Studio for more information.
-------

    railroadink_railroadink.tpl
    
    This is the HTML template of your game.
    
    Everything you are writing in this file will be displayed in the HTML page of your game user interface,
    in the "main game zone" of the screen.
    
    You can use in this template:
    _ variables, with the format {MY_VARIABLE_ELEMENT}.
    _ HTML block, with the BEGIN/END format
    
    See your "view" PHP file to check how to set variables and control blocks
    
    Please REMOVE this comment before publishing your game on BGA
-->

<div id="dice">
    <!-- BEGIN die -->
    <div id="die-{ID}" class="die" data-id="{ID}"></div>
    <!-- END die -->
</div>

<div id="round">
    Round: <span>0</span>
</div>

<div class="player-sheets">
    <!-- BEGIN player -->
    <div id="player-area-{PLAYER_ID}" class="player-area">
        <h2 style="color:#{PLAYER_COLOR}">{NAME}</h2>
        <div id="player-sheet-{PLAYER_ID}" class="player-sheet">
            <div class="special-routes">
                <!-- BEGIN special_route -->
                <div class="special-route special-route-{ROUTE_ID}" data-route="{ROUTE_ID}"></div>
                <!-- END special_route -->
            </div>
            <div class="score">
                <div class="score-part exits"></div>
                <div class="score-part highway"></div>
                <div class="score-part railroad"></div>
                <div class="score-part central"></div>
                <div class="score-part errors"></div>
                <div class="score-part special"></div>
                <div class="score-part total"></div>
            </div>
            <!-- BEGIN field -->
            <div class="field field-{ROW}-{COL}" data-row="{ROW}" data-col="{COL}" style="top: {TOP}px; left: {LEFT}px">
                <div class="route"></div>
                <div class="interactions">
                    <a href="#" class="bgabutton bgabutton_blue left">&#x293f;</a>
                    <a href="#" class="bgabutton bgabutton_blue right">&#x293e;</a>
                    <a href="#" class="bgabutton bgabutton_blue flip">&#x293a;</a>
                    <a href="#" class="bgabutton bgabutton_blue done">&#x2713;</a>
                </div>
                <div class="round-number"></div>
            </div>
            <!-- END field -->
        </div>
    </div>
    <!-- END player -->
</div>

<script type="text/javascript">

// Javascript HTML templates

/*
// Example:
var jstpl_some_game_item='<div class="my_game_item" id="my_game_item_${id}"></div>';

*/

</script>  

{OVERALL_GAME_FOOTER}
