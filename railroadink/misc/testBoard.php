<?php

include "/home/jurica/projects/bga/railroadink/modules/RRIBoard.php";
include "/home/jurica/projects/bga/railroadink/modules/RRIField.php";
include "/home/jurica/projects/bga/railroadink/modules/RRIGraph.php";
include "/home/jurica/projects/bga/railroadink/modules/RRINode.php";
include "/home/jurica/projects/bga/railroadink/material.inc.php";

$board1Fields = [
    [
        "x" => 0, 
        "y" => 0, 
        "route" => 2, 
        "route_type" => 0, 
        "is_flipped" => 0, 
        "rotate" => 1, 
        "round" => 6
    ],
    [
        "x" => 0, 
        "y" => 1, 
        "route" => 8, 
        "route_type" => 0, 
        "is_flipped" => 0, 
        "rotate" => 3, 
        "round" => 5
    ],
    [
        "x" => 0, 
        "y" => 3, 
        "route" => 4, 
        "route_type" => 0, 
        "is_flipped" => 0, 
        "rotate" => 0, 
        "round" => 4
    ],
    [
        "x" => 0, 
        "y" => 4, 
        "route" => 7, 
        "route_type" => 0, 
        "is_flipped" => 0, 
        "rotate" => 1, 
        "round" => null
    ],
    [
        "x" => 1, 
        "y" => 0, 
        "route" => 6, 
        "route_type" => 0, 
        "is_flipped" => 0, 
        "rotate" => 0, 
        "round" => 4
    ],
    [
        "x" => 1, 
        "y" => 1, 
        "route" => 5, 
        "route_type" => 0, 
        "is_flipped" => 0, 
        "rotate" => 0, 
        "round" => 5
    ],
    [
        "x" => 1, 
        "y" => 4, 
        "route" => 2, 
        "route_type" => 0, 
        "is_flipped" => 0, 
        "rotate" => 1, 
        "round" => null
    ],
    [
        "x" => 2, 
        "y" => 0, 
        "route" => 1, 
        "route_type" => 1, 
        "is_flipped" => 0, 
        "rotate" => 0, 
        "round" => 5
    ],
    [
        "x" => 2, 
        "y" => 1, 
        "route" => 1, 
        "route_type" => 0, 
        "is_flipped" => 0, 
        "rotate" => 1, 
        "round" => 4
    ],
    [
        "x" => 2, 
        "y" => 2, 
        "route" => 2, 
        "route_type" => 0, 
        "is_flipped" => 0, 
        "rotate" => 0, 
        "round" => 5
    ],
    [
        "x" => 2, 
        "y" => 3, 
        "route" => 0, 
        "route_type" => 0, 
        "is_flipped" => 0, 
        "rotate" => 1, 
        "round" => 3
    ],
    [
        "x" => 2, 
        "y" => 4, 
        "route" => 0, 
        "route_type" => 0, 
        "is_flipped" => 0, 
        "rotate" => 3, 
        "round" => null
    ],
    [
        "x" => 2, 
        "y" => 5, 
        "route" => 0, 
        "route_type" => 0, 
        "is_flipped" => 0, 
        "rotate" => 1, 
        "round" => 6
    ],
    [
        "x" => 3, 
        "y" => 0, 
        "route" => 1, 
        "route_type" => 0, 
        "is_flipped" => 0, 
        "rotate" => -1,
        "round" =>  null,
    ],
    [
        "x" => 3, 
        "y" => 1, 
        "route" => 3, 
        "route_type" => 1, 
        "is_flipped" => 0, 
        "rotate" => 0, 
        "round" => 3
    ],
    [
        "x" => 3, 
        "y" => 2, 
        "route" => 2, 
        "route_type" => 0, 
        "is_flipped" => 0, 
        "rotate" => 0, 
        "round" => 3
    ],
    [
        "x" => 3, 
        "y" => 3, 
        "route" => 1, 
        "route_type" => 0, 
        "is_flipped" => 0, 
        "rotate" => 3, 
        "round" => 3
    ],
    [
        "x" => 3, 
        "y" => 4, 
        "route" => 2, 
        "route_type" => 0, 
        "is_flipped" => 0, 
        "rotate" => 0, 
        "round" => 5
    ],
    [
        "x" => 3, 
        "y" => 5, 
        "route" => 0, 
        "route_type" => 0, 
        "is_flipped" => 0, 
        "rotate" => 0, 
        "round" => 6
    ],
    [
        "x" => 4, 
        "y" => 0, 
        "route" => 3, 
        "route_type" => 0, 
        "is_flipped" => 0, 
        "rotate" => 2, 
        "round" => 1
    ],
    [
        "x" => 4, 
        "y" => 1, 
        "route" => 5, 
        "route_type" => 1, 
        "is_flipped" => 0, 
        "rotate" => 0, 
        "round" => 2
    ],
    [
        "x" => 4, 
        "y" => 2, 
        "route" => 5, 
        "route_type" => 0, 
        "is_flipped" => 0, 
        "rotate" => 0, 
        "round" => 2
    ],
    [
        "x" => 4, 
        "y" => 3, 
        "route" => 5, 
        "route_type" => 0, 
        "is_flipped" => 0, 
        "rotate" => 0, 
        "round" => null
    ],
    [
        "x" => 5, 
        "y" => 0, 
        "route" => 3, 
        "route_type" => 0, 
        "is_flipped" => 0, 
        "rotate" => 0, 
        "round" => 1
    ],
    [
        "x" => 5, 
        "y" => 1, 
        "route" => 1, 
        "route_type" => 0, 
        "is_flipped" => 0, 
        "rotate" => 2, 
        "round" => 2
    ],
    [
        "x" => 5, 
        "y" => 2, 
        "route" => 7, 
        "route_type" => 0, 
        "is_flipped" => 0, 
        "rotate" => 0, 
        "round" => 2
    ],
    [
        "x" => 5, 
        "y" => 3, 
        "route" => 7, 
        "route_type" => 0, 
        "is_flipped" => 0, 
        "rotate" => 2, 
        "round" => 3
    ],
    [
        "x" => 5, 
        "y" => 4, 
        "route" => 8, 
        "route_type" => 0, 
        "is_flipped" => 0, 
        "rotate" => 0, 
        "round" => 6
    ],
    [
        "x" => 6, 
        "y" => 1, 
        "route" => 1, 
        "route_type" => 0, 
        "is_flipped" => 0, 
        "rotate" => 2, 
        "round" => 2
    ],
    [
        "x" => 6, 
        "y" => 2, 
        "route" => 7, 
        "route_type" => 0, 
        "is_flipped" => 0, 
        "rotate" => 4, 
        "round" => 1
    ],
    [
        "x" => 6, 
        "y" => 3, 
        "route" => 3, 
        "route_type" => 0, 
        "is_flipped" => 0, 
        "rotate" => 1, 
        "round" => 1
    ],
];

$board2Fields = [
    [
        "x" => 0, 
        "y" => 0, 
        "route" => 7, 
        "route_type" => 0, 
        "is_flipped" => 0, 
        "rotate" => 1, 
        "round" => null
    ],
    [
        "x" => 0, 
        "y" => 1, 
        "route" => 2, 
        "route_type" => 0, 
        "is_flipped" => 0, 
        "rotate" => 1, 
        "round" => null
    ],
    [
        "x" => 0, 
        "y" => 3, 
        "route" => 5, 
        "route_type" => 0, 
        "is_flipped" => 0, 
        "rotate" => 1, 
        "round" => 2
    ],
    [
        "x" => 0, 
        "y" => 5, 
        "route" => 2, 
        "route_type" => 0, 
        "is_flipped" => 0, 
        "rotate" => 1, 
        "round" => 6
    ],
    [
        "x" => 1, 
        "y" => 0, 
        "route" => 1, 
        "route_type" => 1, 
        "is_flipped" => 0, 
        "rotate" => 0, 
        "round" => 4
    ],
    [
        "x" => 1, 
        "y" => 1, 
        "route" => 0, 
        "route_type" => 0, 
        "is_flipped" => 0, 
        "rotate" => 0, 
        "round" => null
    ],
    [
        "x" => 1, 
        "y" => 3, 
        "route" => 7, 
        "route_type" => 0, 
        "is_flipped" => 0, 
        "rotate" => 1, 
        "round" => 3
    ],
    [
        "x" => 1, 
        "y" => 5, 
        "route" => 8, 
        "route_type" => 0, 
        "is_flipped" => 0, 
        "rotate" => 3, 
        "round" => 6
    ],
    [
        "x" => 1, 
        "y" => 6, 
        "route" => 5, 
        "route_type" => 0, 
        "is_flipped" => 0, 
        "rotate" => 0, 
        "round" => 5
    ],
    [
        "x" => 2, 
        "y" => 0, 
        "route" => 6, 
        "route_type" => 0, 
        "is_flipped" => 0, 
        "rotate" => 0, 
        "round" => 4
    ],
    [
        "x" => 2, 
        "y" => 1, 
        "route" => 8, 
        "route_type" => 0, 
        "is_flipped" => 0, 
        "rotate" => 1, 
        "round" => 5
    ],
    [
        "x" => 2, 
        "y" => 3, 
        "route" => 1, 
        "route_type" => 0, 
        "is_flipped" => 0, 
        "rotate" => 2, 
        "round" => 3
    ],
    [
        "x" => 2, 
        "y" => 4, 
        "route" => 0, 
        "route_type" => 0, 
        "is_flipped" => 0, 
        "rotate" => 1, 
        "round" => 3
    ],
    [
        "x" => 3, 
        "y" => 0, 
        "route" => 1, 
        "route_type" => 0, 
        "is_flipped" => 0, 
        "rotate" => -1,
        "round" =>  null
    ],
    [
        "x" => 3, 
        "y" => 1, 
        "route" => 1, 
        "route_type" => 0, 
        "is_flipped" => 0, 
        "rotate" => -1,
        "round" =>  null
    ],
    [
        "x" => 3, 
        "y" => 2, 
        "route" => 2, 
        "route_type" => 0, 
        "is_flipped" => 0, 
        "rotate" => 0, 
        "round" => 3
    ],
    [
        "x" => 3, 
        "y" => 3, 
        "route" => 1, 
        "route_type" => 0, 
        "is_flipped" => 0, 
        "rotate" => -1,
        "round" =>  null
    ],
    [
        "x" => 3, 
        "y" => 4, 
        "route" => 1, 
        "route_type" => 0, 
        "is_flipped" => 0, 
        "rotate" => 0, 
        "round" => 2
    ],
    [
        "x" => 4, 
        "y" => 2, 
        "route" => 3, 
        "route_type" => 0, 
        "is_flipped" => 0, 
        "rotate" => 2, 
        "round" => 1
    ],
    [
        "x" => 4, 
        "y" => 3, 
        "route" => 7, 
        "route_type" => 0, 
        "is_flipped" => 0, 
        "rotate" => 2, 
        "round" => 1
    ],
    [
        "x" => 4, 
        "y" => 4, 
        "route" => 3, 
        "route_type" => 1, 
        "is_flipped" => 0, 
        "rotate" => 0, 
        "round" => 2
    ],
    [
        "x" => 4, 
        "y" => 5, 
        "route" => 7, 
        "route_type" => 0, 
        "is_flipped" => 0, 
        "rotate" => 0, 
        "round" => 2
    ],
    [
        "x" => 4, 
        "y" => 6, 
        "route" => 4, 
        "route_type" => 0, 
        "is_flipped" => 0, 
        "rotate" => 1, 
        "round" => 4
    ],
    [
        "x" => 5, 
        "y" => 2, 
        "route" => 3, 
        "route_type" => 0, 
        "is_flipped" => 0, 
        "rotate" => 3, 
        "round" => 1
    ],
    [
        "x" => 5, 
        "y" => 3, 
        "route" => 3, 
        "route_type" => 0, 
        "is_flipped" => 0, 
        "rotate" => 1, 
        "round" => 1
    ],
    [
        "x" => 5, 
        "y" => 6, 
        "route" => 5, 
        "route_type" => 0, 
        "is_flipped" => 0, 
        "rotate" => 1, 
        "round" => null
    ],
    [
        "x" => 6, 
        "y" => 1, 
        "route" => 0, 
        "route_type" => 0, 
        "is_flipped" => 0, 
        "rotate" => 2, 
        "round" => 6
    ],
    [
        "x" => 6, 
        "y" => 2, 
        "route" => 2, 
        "route_type" => 0, 
        "is_flipped" => 0, 
        "rotate" => 0, 
        "round" => 5
    ],
    [
        "x" => 6, 
        "y" => 3, 
        "route" => 5, 
        "route_type" => 1, 
        "is_flipped" => 0, 
        "rotate" => 5, 
        "round" => 1
    ],
    [
        "x" => 6, 
        "y" => 4, 
        "route" => 2, 
        "route_type" => 0, 
        "is_flipped" => 0, 
        "rotate" => 0, 
        "round" => 5
    ],
    [
        "x" => 6, 
        "y" => 5, 
        "route" => 0, 
        "route_type" => 0, 
        "is_flipped" => 0, 
        "rotate" => 1, 
        "round" => 6
    ]
];

$board3Fields = [
    ["x" => 3, "y" => 0, "route" => 6, "route_type" => 0, "is_flipped" => 0, "rotate" => 1, "round" => 1],
    ["x" => 3, "y" => 1, "route" => 2, "route_type" => 0, "is_flipped" => 0, "rotate" => 0, "round" => 2],
    ["x" => 3, "y" => 2, "route" => 3, "route_type" => 1, "is_flipped" => 0, "rotate" => 0, "round" => 2],
    ["x" => 3, "y" => 3, "route" => 7, "route_type" => 0, "is_flipped" => 0, "rotate" => 0, "round" => 2],
    ["x" => 3, "y" => 4, "route" => 3, "route_type" => 0, "is_flipped" => 0, "rotate" => 1, "round" => 2],
    ["x" => 4, "y" => 0, "route" => 4, "route_type" => 0, "is_flipped" => 0, "rotate" => 2, "round" => 1],
    ["x" => 4, "y" => 1, "route" => 3, "route_type" => 0, "is_flipped" => 0, "rotate" => 1, "round" => 2],
    ["x" => 5, "y" => 0, "route" => 4, "route_type" => 0, "is_flipped" => 0, "rotate" => -1,"round" =>  1],
    ["x" => 5, "y" => 1, "route" => 4, "route_type" => 1, "is_flipped" => 0, "rotate" => 0, "round" => 1],
    ["x" => 5, "y" => 2, "route" => 1, "route_type" => 0, "is_flipped" => 0, "rotate" => 0, "round" => 1],
];

$board1 = new RRIBoard(12);
$board2 = new RRIBoard(12);
$board3 = new RRIBoard(12);

// $board1->addFields($board1Fields);
// $board2->addFields($board2Fields);
$board3->addFields($board3Fields);

// $score1 = $board1->getScore();
// $score2 = $board2->getScore();
$score3 = $board3->getScore();

var_dump($score3);
