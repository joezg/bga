<?php

class RRINode {
    private $field;
    private $edges = [];
    private $unconnectedDirections = [];

    public function __construct($field) {
        $this->field = $field;
    }

    public function isFreeEnd(){
        return sizeof($this->unconnectedDirections) > 0;
    }

    public function setField($field){
        $this->field = $field;
    }

    public function getField(){
        return $this->field;
    }

    public function addEdge($direction, $node){
        $this->edges[$direction] = $node;
    }

    public function getEdges(){
        return $this->edges;
    }

    public function getEdge($direction){
        if (!$this->isEdgeExists($direction)){
            return null;
        }

        return $this->edges[$direction];
    }

    public function isEdgeExists($direction){
        return array_key_exists($direction, $this->getEdges());
    }
    
    public function addUnconnectedDirection($direction){
        $this->unconnectedDirections[] = $direction;
    }

    public function getUnconnectedDirections(){
        return $this->unconnectedDirections;
    }

    public function getOutOfBoardConnections(){
        if (sizeof($this->unconnectedDirections) === 0){
            return [];
        }

        $connections = [];

        if ($this->getField()->getY() <= 0 && array_search("N", $this->unconnectedDirections) !== false){
            $connections[] = "N";
        }
        
        if ($this->getField()->getY() >= 6 && array_search("S", $this->unconnectedDirections) !== false){
            $connections[] = "S";
        }
        
        if ($this->getField()->getX() <= 0 && array_search("W", $this->unconnectedDirections) !== false){
            $connections[] = "W";
        }
        
        if ($this->getField()->getX() >= 6 && array_search("E", $this->unconnectedDirections) !== false){
            $connections[] = "E";
        }


        return $connections;

    }

}