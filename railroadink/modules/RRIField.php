<?php

class RRIField
{
    private $route;
    private $rotation;
    private $routeType;
    private $flipped;
    private $x;
    private $y;
    private $round;

    const TYPE_REGULAR = 0;
    const TYPE_SPECIAL = 1;

    private static $routes = [
        0 => [
            "sides" => ["R", "B", "B", "R"],
            "rotations" => 4,
            "station" => false,
            "flips" => false
        ],
        1 => [
            "sides" => ["R", "R", "B", "R"],
            "rotations" => 4,
            "station" => false,
            "flips" => false
        ],
        2 => [
            "sides" => ["R", "B", "R", "B"],
            "rotations" => 2,
            "station" => false,
            "flips" => false
        ],
        3 => [
            "sides" => ["H", "B", "B", "H"],
            "rotations" => 4,
            "station" => false,
            "flips" => false
        ],
        4 => [
            "sides" => ["H", "H", "B", "H"],
            "rotations" => 4,
            "station" => false,
            "flips" => false
        ],
        5 => [
            "sides" => ["H", "B", "H", "B"],
            "rotations" => 2,
            "station" => false,
            "flips" => false
        ],
        6 => [
            "sides" => ["H", "R", "H", "R"],
            "rotations" => 2,
            "station" => false,
            "flips" => false
        ],
        7 => [
            "sides" => ["R", "B", "H", "B"],
            "rotations" => 4,
            "station" => true,
            "flips" => false
        ],
        8 => [
            "sides" => ["R", "B", "B", "H"],
            "rotations" => 2,
            "station" => true,
            "flips" => true
        ],
    ];
    private static $specialRoutes = [
        0 => [
            "sides" => ["H", "H", "R", "H"],
            "rotations" => 4,
            "station" => true,
            "flips" => false
        ],
        1 => [
            "sides" => ["H", "R", "R", "R"],
            "rotations" => 4,
            "station" => true,
            "flips" => false
        ],
        2 => [
            "sides" => ["H", "H", "H", "H"],
            "rotations" => 0,
            "station" => false,
            "flips" => false
        ],
        3 => [
            "sides" => ["R", "R", "R", "R"],
            "rotations" => 0,
            "station" => false,
            "flips" => false
        ],
        4 => [
            "sides" => ["H", "R", "R", "H"],
            "rotations" => 4,
            "station" => true,
            "flips" => false
        ],
        5 => [
            "sides" => ["H", "R", "H", "R"],
            "rotations" => 2,
            "station" => true,
            "flips" => false
        ],
    ];

    public function __construct($raw)
    {
        $raw = array_merge([
            "route" => null,
            "route_type" => null,
            "rotate" => null,
            "is_flipped" => null,
            "x" => null,
            "y" => null,
            "round" => null,

        ], $raw);

        $this->route = intval($raw["route"]);
        $this->routeType = intval($raw["route_type"]);
        $this->rotation = self::getNormalizedRotation(intval($raw["rotate"]));
        $this->flipped = intval($raw["is_flipped"]) === 1;
        $this->x = intval($raw["x"]);
        $this->y = intval($raw["y"]);
        $this->round = intval($raw["round"]);
    }

    public function serialize()
    {
        return
            [
                'route'   => $this->route,
                'routeType'   => $this->routeType,
                'rotation'   => $this->rotation,
                'flipped'   => $this->flipped,
                'x'   => $this->x,
                'y'   => $this->y,
                'round' => $this->round
            ];
    }

    public function isSplitable(){
        return $this->routeType === 0 && $this->route === 6;
    }

    public function split(){
        if ($this->isSplitable()){
            $first = new RRIField([
                "route" => 5,
                "route_type" => $this->routeType,
                "rotate" => $this->rotation,
                "is_flipped" => $this->flipped,
                "x" => $this->x,
                "y" => $this->y,
                "round" => $this->round,
            ]);

            $second = new RRIField([
                "route" => 2,
                "route_type" => $this->routeType,
                "rotate" => $this->rotation + 1,
                "is_flipped" => $this->flipped,
                "x" => $this->x,
                "y" => $this->y,
                "round" => $this->round,
            ]);

            return [$first, $second];
        }

        throw new Error("Cannot split field with routeType ".$this->routeType." and route ".$this->route);
    }

    public static function getSpecialRoutes()
    {
        return self::$specialRoutes;
    }

    private static function rotateSides($sides, $rotations)
    {
        if ($rotations === 0) {
            return $sides;
        }
        array_unshift($sides, array_pop($sides));
        return self::rotateSides($sides, $rotations - 1);
    }

    private static function flipSides($sides)
    {
        $temp = $sides[1];
        $sides[1] = $sides[3];
        $sides[3] = $temp;

        return $sides;
    }

    private static function getNormalizedRotation($rotation)
    {
        $normalized = $rotation % 4;

        return $normalized < 0 ? $normalized + 4 : $normalized;
    }

    public function hasStation(){
        $route = $this->getRoutesByType($this->routeType)[$this->route];

        return $route["station"];
    }

    private function getRoutesByType($type){
        return self::getAllRoutesGroupedByType()[$type];
    }

    private function getFieldRoutes(){
        return $this->getRoutesByType($this->routeType);
    }

    public function checkIfConnected($direction){
        [$north, $east, $south, $west] = $this->getRotatedAndFlippedSides();

        switch ($direction) {
            case 'N':
                return $south !== "B";           
            case 'S':
                return $north !== "B";           
            case 'E':
                return $west !== "B";           
            case 'W':
                return $east !== "B";           
            default:
                return false;
        }
    }

    private function getRotatedAndFlippedSides() {
        $route = $this->getFieldRoutes()[$this->route];
        $sides = $route["sides"];
        if ($this->flipped) {
            $sides = self::flipSides($sides);
        }

        $sides = self::rotateSides($sides, $this->rotation);

        return $sides;
    }

    public function getFieldDirections($includeOffBoard = false)
    {
        $directions = [];

        [$north, $east, $south, $west] = $this->getRotatedAndFlippedSides();

        if ($north !== "B" && ($this->y > 0 || $includeOffBoard)) {
            $directions[] = [
                "to" => [
                    "x" => $this->x,
                    "y" => $this->y - 1
                ],
                "direction" => "N",
                "type" => $north
            ];
        }
        if ($east !== "B" && ($this->x < 6 || $includeOffBoard)) {
            $directions[] = [
                "to" => [
                    "x" => $this->x + 1,
                    "y" => $this->y
                ],
                "direction" => "E",
                "type" => $east
            ];
        }
        if ($south !== "B" && ($this->y < 6 || $includeOffBoard)) {
            $directions[] = [
                "to" => [
                    "x" => $this->x,
                    "y" => $this->y + 1
                ],
                "direction" => "S",
                "type" => $south
            ];
        }
        if ($west !== "B" && ($this->x > 0 || $includeOffBoard)) {
            $directions[] = [
                "to" => [
                    "x" => $this->x - 1,
                    "y" => $this->y
                ],
                "direction" => "W",
                "type" => $west
            ];
        }

        return $directions;
    }

    public function checkAgainstRestrictions($restrictions) {
        $route = $this->getFieldRoutes()[$this->route];
        return self::checkRouteRotationAgainsRestrictions($route, $this->rotation, $this->flipped, $restrictions);
    }

    private static function getSideForDirection($sides, $positions)
    {
        switch ($positions) {
            case 'N':
                return $sides[0];
            case 'E':
                return $sides[1];
            case 'S':
                return $sides[2];
            case 'W':
                return $sides[3];

            default:
                return "B";
        }
    }

    private static function checkRouteRotationAgainsRestrictions($route, $rotation, $flipped, $restrictions)
    {
        $sides = $route["sides"];
        if ($flipped) {
            $sides = self::flipSides($sides);
        }
        if ($rotation > 0) {
            $sides = self::rotateSides($sides, $rotation);
        }

        $isConnected = false;
        foreach ($restrictions as $direction => $restriction) {
            $sideForDirection = self::getSideForDirection($sides, $direction);

            if ($sideForDirection === $restriction) {
                $isConnected = true;
            }

            if ($restriction !== $sideForDirection && $sideForDirection !== "B") {
                return false;
            }
        }

        return $isConnected;
    }

    private static function checkRouteAgainstRestriction($route, $flipped, $restrictions)
    {
        $rotations = [];
        for ($i = 0; $i < 4; $i++) {
            if (self::checkRouteRotationAgainsRestrictions($route, $i, $flipped, $restrictions)) {
                $rotations[] = $i;
            }
        }

        return $rotations;
    }

    private static function isRouteConnectionPossible($sides, $restrictionValues)
    {
        return sizeof(array_intersect($sides, $restrictionValues)) > 0;
    }

    private static function getAllRoutesGroupedByType() {
        return [
            self::TYPE_REGULAR => self::$routes,
            self::TYPE_SPECIAL => self::$specialRoutes,
        ];
    }

    /**
     * Gets the possible routes type that could be placed on a board 
     * field with certain restrictions.
     */
    public static function getPossibleRoutes($restrictions)
    {
        $possibleRoutes = [];

        foreach (self::getAllRoutesGroupedByType() as $routeType => $currentRoutes) {
            foreach ($currentRoutes as $id => $route) {
                $routePositions = [
                    "route" => $id,
                    "routeType" => $routeType,
                    "normalRotations" => [],
                    "flippedRotations" => []
                ];

                if (self::isRouteConnectionPossible($route["sides"], array_values($restrictions))) {
                    $routePositions["normalRotations"] = self::checkRouteAgainstRestriction($route, false, $restrictions);
                    if ($route["flips"]) {
                        $routePositions["flippedRotations"] = self::checkRouteAgainstRestriction($route, true, $restrictions);
                    }
                }
                $possibleRoutes[] = $routePositions;
            }
        }

        return $possibleRoutes;
    }

    public function getDirectionsFrom($direction)
    {
        [$north, $east, $south, $west] = $this->getRotatedAndFlippedSides();

        $type = $this->getConnectionType($direction);

        $nextDirections = [];

        if ($north === $type && $north !== "B" && $direction !== "N"){
            $nextDirections[] = "N";
        }
        if ($south === $type && $south !== "B" && $direction !== "S"){
            $nextDirections[] = "S";
        }
        if ($west === $type && $west !== "B" && $direction !== "W"){
            $nextDirections[] = "W";
        }
        if ($east === $type && $east !== "B" && $direction !== "E"){
            $nextDirections[] = "E";
        }

        return $nextDirections;
    }

    public function getNeighbour($direction){
        $x = $this->x;
        $y = $this->y;
        
        if ($direction === "N"){
            $y--;
        }
        if ($direction === "S"){
            $y++;
        }
        if ($direction === "E"){
            $x++;
        }
        if ($direction === "W"){
            $x--;
        }

        $key = $x.$y;

        return compact("x", "y", "key");
    }

    public function getConnectionType($direction)
    {
        [$north, $east, $south, $west] = $this->getRotatedAndFlippedSides();

        switch ($direction) {
            case 'N':
                return $north;
            case 'S':
                return $south;
            case 'E':
                return $east;
            case 'W':
                return $west;
            default:
                # code...
                break;
        }
    }

    /**
     * Get the value of x
     */ 
    public function getX()
    {
        return $this->x;
    }

    /**
     * Set the value of x
     *
     * @return  self
     */ 
    public function setX($x)
    {
        $this->x = $x;

        return $this;
    }

    /**
     * Get the value of y
     */ 
    public function getY()
    {
        return $this->y;
    }

    /**
     * Set the value of y
     *
     * @return  self
     */ 
    public function setY($y)
    {
        $this->y = $y;

        return $this;
    }

    /**
     * Get the value of y
     */ 
    public function getRound()
    {
        return $this->round;
    }

    /**
     * Set the value of y
     *
     * @return  self
     */ 
    public function serRound($round)
    {
        $this->round = $round;

        return $this;
    }
}
