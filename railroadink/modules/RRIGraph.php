<?php

class RRIGraph {

    private $nodes;

    public function __construct($options) {
        $options = array_merge([
            "fields" => [],
            "addEntryNodes" => false
        ], $options);

        [ "fields" => $fields, "addEntryNodes" => $addEntryNodes ] = $options;
        $this->makeGraph($fields, $addEntryNodes);;
    }

    private function fieldKey(int $x, int $y)
    {
        return $x . $y;
    }

    private function getNode(int $x, int $y, string $direction = null){
        $key = $this->fieldKey($x, $y);
        if (array_key_exists($key, $this->nodes)){
            return $this->nodes[$key];
        }

        if ($direction === "S" || $direction === "N"){
            if (array_key_exists($key."NS", $this->nodes)){
                return $this->nodes[$key."NS"];
            }
        }
        
        if ($direction === "E" || $direction === "W"){
            if (array_key_exists($key."EW", $this->nodes)){
                return $this->nodes[$key."EW"];
            }
        }

        return null;
    }

    public function getFeeEndNodes(){
        return array_filter($this->nodes, function($node){
            return $node->isFreeEnd();
        });
    }
    
    public function getNodesWithStation(){
        return array_filter($this->nodes, function($node){
            return $node->getField()->hasStation();
        });
    }
    
    private function makeGraph($fields, $addEntryNodes) {
        $this->nodes = [];

        //Create nodes
        for ($x=0; $x<7; $x++) { 
            for ($y=0; $y<7; $y++) { 
                $key = $this->fieldKey($x, $y);

                if (array_key_exists($key, $fields)) {
                    $field = $fields[$key];

                    if ($field->isSplitable()){
                        $parts = $field->split();

                        foreach ($parts as $part) {
                            if ($part->getConnectionType("N") !== "B"){
                                $this->nodes[$this->fieldKey($x, $y)."NS"] = new RRINode($part);
                            }

                            if ($part->getConnectionType("E") !== "B"){
                                $this->nodes[$this->fieldKey($x, $y)."EW"] = new RRINode($part);
                            }
                        }
                    } else {
                        $node = new RRINode($fields[$key]);
                        $this->nodes[$this->fieldKey($x, $y)] = $node;
                    }
                    
                }

                if ($addEntryNodes){

                    if (array_search($x, [0, 6]) !== false && array_search($y, [1, 3, 5]) !== false){
                        $offBoardX = $x === 0 ? -1 : 7;
                        $field = new RRIField([
                            "x" => $offBoardX, 
                            "y" => $y, 
                            "route_type" => 0, 
                            "route" => $y === 3 ? 5 : 2,
                            "rotate" => 1
                        ]);
                        $node = new RRINode($field);
                        $this->nodes[$this->fieldKey($offBoardX, $y)] = $node;
                    }
                    
                    if (array_search($y, [0, 6]) !== false && array_search($x, [1, 3, 5]) !== false){
                        $offBoardY = $y === 0 ? -1 : 7;
                        $field = new RRIField([
                            "x" => $x, 
                            "y" => $offBoardY, 
                            "route_type" => 0, 
                            "route" => $x === 3 ? 2 : 5
                        ]);
                        $node = new RRINode($field);
                        $this->nodes[$this->fieldKey($x, $offBoardY)] = $node;
                    }
                }

            }
        }

        //Add edges
        foreach ($this->nodes as $node) {
            $includeOffBoard = true;
            $directions = $node->getField()->getFieldDirections($includeOffBoard);

            foreach ($directions as $next) {
                $x = $next["to"]["x"];
                $y = $next["to"]["y"];
                $direction = $next["direction"];

                $nextNode = $this->getNode($x, $y, $direction);
                
                if ($nextNode !== null){
                    if ($nextNode->getField()->checkIfConnected($direction)){
                        $node->addEdge($direction, $nextNode);
                        continue;
                    }
                }

                $node->addUnconnectedDirection($direction);
            }

        }
    }
}