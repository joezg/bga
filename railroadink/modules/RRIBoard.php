<?php

class RRIBoard
{
    private $playerId;
    private $fields = [];

    const EXIT_SCORE_MAP = [
        0 => 0,
        1 => 0,
        2 => 4,
        3 => 8,
        4 => 12,
        5 => 16,
        6 => 20,
        7 => 24,
        8 => 28,
        9 => 32,
        10 => 36,
        11 => 40,
        12 => 45,
    ];

    public function __construct(int $playerId)
    {
        $this->playerId = $playerId;
    }

    public function addFields($fields)
    {
        foreach ($fields as $raw) {
            $this->addField($raw);
        }
    }

    public function addField($raw)
    {
        $field = new RRIField($raw);
        $this->fields[$this->fieldKey($raw["x"], $raw["y"])] = $field;
    }

    private function fieldKey(int $x, int $y)
    {
        return $x . $y;
    }

    public function getPlayerId(){
        return $this->playerId;
    }

    public function removeFieldsForRound($round){
        $this->fields = array_filter($this->fields, function($field) use ($round) {
            return $field->getRound() != $round;
        });
    }

    public function removeUnconfirmedFields(){
        $this->removeFieldsForRound(null);
    }

    public function serialize()
    {
        return
            [
                "playerId" => $this->playerId,
                'fields'   => array_values(array_map(function ($field) {
                    return $field->serialize();
                }, $this->fields)),
                "score" => $this->getScore()
            ];
    }

    public function isFieldExists($x, $y)
    {
        return array_key_exists($this->fieldKey($x, $y), $this->fields);
    }

    public function getAvailableFields()
    {
        $available = [];

        $graph = new RRIGraph([ "fields" => $this->fields, "addEntryNodes" => true]);
        $freeEndNodes = $graph->getFeeEndNodes();

        foreach ($freeEndNodes as $node) {
            $insideUnconnected = array_diff($node->getUnconnectedDirections(), $node->getOutOfBoardConnections());
            foreach ($insideUnconnected as $unconnected) {
                ["x" => $x, "y" => $y, "key" => $key ] = $node->getField()->getNeighbour($unconnected);
                
                if (!$this->isFieldExists($x, $y)){
                    $currentAvailable = [
                        "x" => $x,
                        "y" => $y,
                        "restrictions" => []
                    ];
                    if (isset($available[$key])) {
                        $currentAvailable = $available[$key];
                    }
        
                    $currentAvailable["restrictions"][$this->getOppositeDirection($unconnected)] = $node->getField()->getConnectionType($unconnected);
                    $available[$key] = $currentAvailable;
                }
            }
        }

        return $available;
    }

    public function checkIfLegal($field){
        $key = $this->fieldKey($field->getX(), $field->getY());
        $availableFields = $this->getAvailableFields();
        
        if (!isset($availableFields[$key])){
            return false;
        }

        $fieldRestrictions = $availableFields[$key]["restrictions"];

        return $field->checkAgainstRestrictions($fieldRestrictions);
    }

    private function getOppositeDirection($dir)
    {
        switch ($dir) {
            case 'N':
                return "S";
            case 'S':
                return "N";
            case 'E':
                return "W";
            case 'W':
                return "E";
            default:
                return null;
        }
    }

    public function getScore() {
        $score = array_merge(
            $this->calculateCentralFieldsScore(),
            $this->calculateErrorsScore(),
            $this->calculateExitsScore(),
            $this->calculateLongestPathsScore()
        );

        $score["total"] = 0;

        foreach ($score as $scorePart) {
            $score["total"] += $scorePart["total"];
        }

        return $score;
    }

    private function calculateCentralFieldsScore(){
        $occupiedFields = [];
        for ($x=2; $x < 5; $x++) { 
            for ($y=2; $y < 5; $y++) { 
                $key = $this->fieldKey($x, $y);
                
                if (isset($this->fields[$key])){
                    $occupiedFields[] = $this->fields[$key];
                }
            }
        }

        return [
            "centralFields" => [
                "total" => sizeof($occupiedFields),
                "occupiedFields" => $occupiedFields
            ]
        ];
    }

    private function calculateErrorsScore(){

        $graph = new RRIGraph([ "fields" => $this->fields, "addEntryNodes" => false]);
        $freeEndNodes = $graph->getFeeEndNodes();

        $allErrorDirections = [];
        foreach ($freeEndNodes as $node) {
            $nodeErrors = array_diff($node->getUnconnectedDirections(), $node->getOutOfBoardConnections());

            foreach ($nodeErrors as $error) {
                $allErrorDirections[] = [
                    "field" => $node->getField(),
                    "direction" => $error
                ];
            }
        }

        return [
            "errors" => [
                "total" => -1 * sizeof($allErrorDirections),
                "allErrorDirections" => $allErrorDirections
            ]
        ];
    }

    private function calculateExitsScore() {
        $exitGroups = [];

        $graph = new RRIGraph([ "fields" => $this->fields, "addEntryNodes" => false ]);
        $freeEndNodes = $graph->getFeeEndNodes();

        $currentExitGroup = null;
        $visited = [];
        $traverse = function($node) use (&$currentExitGroup, &$traverse, &$visited) {
            if (array_search($node, $visited) !== false){
                return;
            }

            $visited[] = $node;
            
            $currentExitGroup["fields"][] = $node->getField();

            if ($this->isConnectedToStartingField($node->getField())){
                $currentExitGroup["exits"][] = $node->getField();
            }

            foreach ($node->getEdges() as $edge) {
                $traverse($edge);
            }
        };

        while (sizeof($freeEndNodes) > 0) {
            $node = array_pop($freeEndNodes);

            if ($this->isConnectedToStartingField($node->getField())){
                $currentExitGroup = [
                    "fields" => [],
                    "exits" => []
                ];

                $traverse($node);

                $exitGroups[] = $currentExitGroup;
            }
            
        }
        
        $total = 0;
        foreach ($exitGroups as &$group) {
            $score = self::EXIT_SCORE_MAP[sizeof($group["exits"])];
            $total += $score;
            $group["score"] = $score;
        }

        return [
            "exits" => [
                "total" => $total,
                "exitGroups" => $exitGroups
            ]
        ];
    }

    private function findLongestPath($currentNode, $direction, $visited = []){
        $currentField = $currentNode->getField();
        $nextDirections = $currentField->getDirectionsFrom($direction);
        
        if (array_search($currentNode, $visited) !== false){
            //todo check if any unvisited edges with same type
            return [];
        }

        $visited[] = $currentNode;

        $continuingPath = [];
        
        foreach ($nextDirections as $next) {
            if ($currentNode->isEdgeExists($next)){
                $possiblePath = $this->findLongestPath($currentNode->getEdge($next), $this->getOppositeDirection($next), $visited);

                if (sizeof($possiblePath) > sizeof($continuingPath)){
                    $continuingPath = $possiblePath;    
                }
            }
        }

        $continuingPath[] = $currentField;

        return $continuingPath;
    }

    private function calculateLongestPathsScore(){
        $graph = new RRIGraph([ "fields" => $this->fields, "addEntryNodes" => false]);

        $freeEndNodes = $graph->getFeeEndNodes();

        $longestRoad = [];
        $longestRailroad = [];

        $resolver = function($type, $path) use (&$longestRailroad, &$longestRoad) {
            if ($type === "H" && sizeof($path) > sizeof($longestRoad)){
                $longestRoad = $path;
            }
            
            if ($type === "R" && sizeof($path) > sizeof($longestRailroad)){
                $longestRailroad = $path;
            }
        };

        foreach ($freeEndNodes as $current) {
            foreach ($current->getUnconnectedDirections() as $direction) {
                $type = $current->getField()->getConnectionType($direction);
                $resolver($type, $this->findLongestPath($current, $direction));
            }
        }

        foreach ($graph->getNodesWithStation() as $current) {
            $includeOffBoard = true;
            foreach ($current->getField()->getFieldDirections($includeOffBoard) as $direction) {
                $type = $current->getField()->getConnectionType($direction["direction"]);

                $path = [$current->getField()];
                if (array_key_exists($direction["direction"], $current->getEdges())){
                    $path = array_merge($path, $this->findLongestPath($current->getEdges()[$direction["direction"]], $this->getOppositeDirection($direction["direction"])));
                }

                $resolver($type, $path);
            }
        }
        
        return [
            "longestRailroad" => [
                "fields" => $longestRailroad,
                "total" => sizeof($longestRailroad)
            ],
            "longestHighway" => [
                "fields" => $longestRoad,
                "total" => sizeof($longestRoad)
            ]
        ];
    }

    private function isConnectedToStartingField($field) {
        $isStartingField = array_search($field->getX(), [0, 6]) !== false && array_search($field->getY(), [1, 3, 5]) !== false  
            || array_search($field->getY(), [0, 6]) !== false && array_search($field->getX(), [1, 3, 5]) !== false;

        if ($isStartingField){
            if ($field->getX() == 0){
                return $field->checkIfConnected("E");
            }
            if ($field->getX() == 6){
                return $field->checkIfConnected("W");
            }
            if ($field->getY() == 0){
                return $field->checkIfConnected("S");
            }
            if ($field->getY() == 6){
                return $field->checkIfConnected("N");
            }
        }

        return false;
    }
}
