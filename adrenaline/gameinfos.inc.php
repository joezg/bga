<?php $gameinfos = array (
  'designer' => 'Filip Neduk',
  'artist' => 'Jakub Politzer',
  'year' => 2016,
  'publisher' => 'Czech Game Edition',
  'publisher_website' => 'http://czechgames.com/',
  'publisher_bgg_id' => 7345,
  'bgg_id' => 202408,
  'players' => 
  array (
    0 => 3,
    1 => 4,
    2 => 5,
  ),
  'suggest_player_number' => 5,
  'not_recommend_player_number' => NULL,
  'estimated_duration' => 60,
  'fast_additional_time' => 30,
  'medium_additional_time' => 40,
  'slow_additional_time' => 50,
  'tie_breaker_description' => '',
  'is_beta' => 1,
  'is_coop' => 0,
  'complexity' => 3,
  'luck' => 2,
  'strategy' => 3,
  'diplomacy' => 3,
  'player_colors' => 
  array (
    0 => 'ff0000',
    1 => '008000',
    2 => '0000ff',
    3 => 'ffa500',
    4 => '773300',
  ),
  'favorite_colors_support' => false,
  'game_interface_width' => 
  array (
    'min' => 740,
    'max' => NULL,
  ),
  'tags' => 
  array (
    0 => 2,
  ),
  'is_sandbox' => false,
  'turnControl' => 'simple',
); ?>