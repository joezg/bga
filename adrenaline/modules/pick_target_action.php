<?php

abstract class PickTargetAction {
    public static function getInstance($type, $game){
        if ($type == TargetActions::NEWTON){
            $instance = new NewtonPickTargetAction();
            $instance->game = $game;
            return $instance;
        } elseif ($type == TargetActions::SHOOTING){
            $instance = new ShootingPickTargetAction();
            $instance->game = $game;
            return $instance;
        }
    }

    protected abstract function getDescription();
    protected abstract function doActionHook();
    protected abstract function sendSpecificNotifications();
    protected abstract function changeState();
    protected abstract function getAllowedPlayers();
    protected abstract function getNotificationDescription();

    /** @var Adrenaline */
    protected $game = null;

    public function doAction(){
        $this->doActionHook();
    }
}

class NewtonPickTargetAction extends PickTargetAction{
    function getDescription()
    {
        return clienttranslate("for Newton");
    }

    function getNotificationDescription()
    {
        return clienttranslate("with Newton");
    }


    function getAllowedPlayers()
    {
        $playerId = $this->game->getActivePlayerId();
        $players = $this->game->loadPlayersBasicInfos();

        $playerColors = array();

        foreach ($players as $id => $player){
            if ($id != $playerId){
                $playerColors[] = PlayerColor::getColorName($player['player_color']);
            }

        }

        return $playerColors;
    }

    function doActionHook()
    {
    }

    function sendSpecificNotifications()
    {
    }

    function changeState()
    {
        $this->game->gamestate->nextState( "moveTarget" );
    }
}

class ShootingPickTargetAction extends PickTargetAction{
    private function getAction(){
        return WeaponAction::getInstance($this->game->getGlobal(Globals::SHOOTING_WEAPON_CARD_ID), $this->game);
    }

    function getDescription()
    {
        return clienttranslate("for ".$this->getAction()->getName());
    }

    function getNotificationDescription()
    {
        return clienttranslate("with ".$this->getAction()->getName());
    }


    function getAllowedPlayers()
    {
        return $this->getAction()->getValidTargets();
    }

    function doActionHook()
    {
        $this->getAction()->doAction();
    }

    function sendSpecificNotifications()
    {
        $this->getAction()->sendNotification();
    }

    function changeState()
    {
        $this->getAction()->changeState();
    }
}