<?php

abstract class MoveTargetAction {
    public static function getInstance($type, $game){
        if ($type == TargetActions::NEWTON){
            $instance = new NewtonMoveTargetAction();
            $instance->game = $game;
            return $instance;
        }
    }

    protected abstract function getDescription();
    protected abstract function doActionHook();
    protected abstract function sendSpecificNotifications();
    protected abstract function changeState();
    protected abstract function getAllowedRoomIds();
    protected abstract function getNotificationDescription();

    /** @var Adrenaline  */
    protected $game = null;
    protected $newRoomId = null;

    public function doAction($newRoomId){
        $this->newRoomId = $newRoomId;
        $this->doActionHook();
    }
}

class NewtonMoveTargetAction extends MoveTargetAction{
    function getDescription()
    {
        return "with Newton";
    }

    function getNotificationDescription()
    {
        return "with Newton";
    }


    function getAllowedRoomIds()
    {
        $board = $this->game->getCurrentBoard();
        $rooms = $board['rooms'];
        $targetId = $this->game->getGlobal(Globals::CURRENT_TARGET);
        $targetRoomId = $this->game->getPlayerRoomId($targetId);
        $targetRoom = $rooms[$targetRoomId];

        $allowedRooms = array();

        for ($i=1; $i<3; $i++){
            //to the left
            $newRoomId = $targetRoom['y'].(intval($targetRoom['x'])-$i);
            $currentRoomId = $targetRoom['y'].(intval($targetRoom['x'])-$i+1);
            if (array_key_exists($newRoomId, $rooms)){
                if ($rooms[$newRoomId]['color']==$rooms[$currentRoomId]['color']
                    || $this->game->isDoorBetweenRooms($newRoomId, $currentRoomId)){
                    $allowedRooms[] = $newRoomId;
                }
            }

            //to the right
            $newRoomId = $targetRoom['y'].(intval($targetRoom['x'])+$i);
            $currentRoomId = $targetRoom['y'].(intval($targetRoom['x'])+$i-1);
            if (array_key_exists($newRoomId, $rooms)){
                if ($rooms[$newRoomId]['color']==$rooms[$currentRoomId]['color']
                    || $this->game->isDoorBetweenRooms($newRoomId, $currentRoomId)){
                    $allowedRooms[] = $newRoomId;
                }
            }

            //up
            $newRoomId = (intval($targetRoom['y'])-$i).$targetRoom['x'];
            $currentRoomId = (intval($targetRoom['y'])-$i+1).$targetRoom['x'];
            if (array_key_exists($newRoomId, $rooms)){
                if ($rooms[$newRoomId]['color']==$rooms[$currentRoomId]['color']
                    || $this->game->isDoorBetweenRooms($newRoomId, $currentRoomId)){
                    $allowedRooms[] = $newRoomId;
                }
            }

            //down
            $newRoomId = (intval($targetRoom['y'])+$i).$targetRoom['x'];
            $currentRoomId = (intval($targetRoom['y'])+$i-1).$targetRoom['x'];
            if (array_key_exists($newRoomId, $rooms)){
                if ($rooms[$newRoomId]['color']==$rooms[$currentRoomId]['color']
                    || $this->game->isDoorBetweenRooms($newRoomId, $currentRoomId)){
                    $allowedRooms[] = $newRoomId;
                }
            }

        }

        return $allowedRooms;
    }

    function doActionHook()
    {
    }

    function sendSpecificNotifications()
    {
    }

    function changeState()
    {
        $this->game->setGlobal(Globals::TARGET_ACTION, TargetActions::NO_ACTION);
        if ($this->game->getGlobal(Globals::IS_END_OF_TURN)==1){
            $this->game->setGlobal(Globals::IS_END_OF_TURN, 0);
            $this->game->gamestate->nextState( "finishAction" );
        } else {
            $this->game->gamestate->nextState( "continue" );
        }

    }
}