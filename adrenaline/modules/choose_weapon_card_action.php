<?php

abstract class ChooseWeaponCardAction {
    public static function getInstance($type, $game){
        if ($type == ChooseWeaponCardActions::RETURN_CARD){
            $instance = new ReturnCardChooseWeaponCardAction();
            $instance->game = $game;
            return $instance;
        } elseif ($type == ChooseWeaponCardActions::SHOOT){
            $instance = new ShootChooseWeaponCardAction();
            $instance->game = $game;
            return $instance;
        }
    }

    protected abstract function doActionHook();
    protected abstract function sendSpecificNotifications();
    protected abstract function changeState();

    /** @var Adrenaline */
    protected $game = null;
    protected $cardId = null;
    protected $cardDb = null;
    protected $card = null;
    protected $playerId = null;

    public function doAction($cardId, $cardDb, $playerId){
        $this->cardId = $cardId;
        $this->cardDb = $cardDb;
        $this->playerId = $playerId;
        $this->card = $this->game->weaponCards[$cardDb['type']];

        $this->doActionHook();
    }
}

class ReturnCardChooseWeaponCardAction extends ChooseWeaponCardAction{

    private $color = null;

    function doActionHook()
    {
        $roomId = $this->game->getPlayerRoomId($this->playerId);
        $board = $this->game->getCurrentBoard();
        $room = $board['rooms'][$roomId];

        //return to display
        $this->color = $room['color'];
        $this->game->weaponDeck->moveCard( $this->cardId, $this->color, 0);
    }

    function sendSpecificNotifications()
    {
        //notify players
        $this->game->notifyAllPlayers( "returnWeaponCard", clienttranslate( '${player_name} returns ${cardName} to ${color} display'), array(
            'playerId' => $this->playerId,
            'player_name' => $this->game->getActivePlayerName(),
            'color' => $this->color,
            'cardId' => $this->cardId,
            'card' => $this->card,
            'cardName' => $this->card['name']
        ) );
    }

    function changeState()
    {
        $this->game->gamestate->nextState( "finishAction" );
    }
}

class ShootChooseWeaponCardAction extends ChooseWeaponCardAction{

    private $color = null;

    function doActionHook()
    {
        //play to table
        $this->game->weaponDeck->moveCard( $this->cardId, 'table', $this->playerId);
        $this->game->setGlobal(Globals::SHOOTING_WEAPON_CARD_ID, $this->cardId);
    }

    function sendSpecificNotifications()
    {
        //notify players
        $this->game->notifyAllPlayers( "readyToShoot", clienttranslate( '${player_name} is ready to shoot with ${cardName}'), array(
            'playerId' => $this->playerId,
            'player_name' => $this->game->getActivePlayerName(),
            'cardName' => $this->card['name']
        ) );
    }

    function changeState()
    {
        $this->game->gamestate->nextState( "resolveShooting" );
    }
}