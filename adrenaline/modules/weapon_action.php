<?php

abstract class WeaponAction {
    public static function getInstance($cardId, $game){
        $cardDb = $game->weaponDeck->getCard($cardId);
        $card = $game->weaponCards[$cardDb['type']];

        if ($card['itemType'] == "whisper"){
            $instance = new WhisperWeaponAction();
            $instance->game = $game;
            $instance->cardDb = $cardDb;
            $instance->card = $card;
            $instance->currentShootingState = $instance->game->getGlobal(Globals::SHOOTING_STATE);
            return $instance;
        }
    }

    protected abstract function changeState();
    protected abstract function getName();
    protected abstract function getValidTargets();
    protected abstract function doAction();
    protected abstract function sendNotification();

    /** @var Adrenaline */
    protected $game = null;
    protected $cardId = null;
    protected $cardDb = null;
    protected $card = null;
    protected $currentShootingState = null;
}

class WhisperWeaponAction extends WeaponAction{
    const START = 0;
    const PICKED_TARGET = 1;

    function changeState()
    {
        if ($this->currentShootingState == WhisperWeaponAction::START){
            $this->game->setGlobal(Globals::TARGET_ACTION, TargetActions::SHOOTING);
            $this->game->setGlobal(Globals::SHOOTING_STATE, WhisperWeaponAction::PICKED_TARGET);
            $this->game->gamestate->nextState( "pickTarget" );
        } else if ($this->currentShootingState == WhisperWeaponAction::PICKED_TARGET){
            $playerId = self::getActivePlayerId();
            $playerCards = $this->game->powerupDeck->getCardsInLocation( 'hand', $playerId );

            $playerHasTargetingScope = false;
            foreach($playerCards as $cardId => $cardDb){
                if ($this->powerupCards[$cardDb['type']]['itemType'] == 'targetingScope'){
                    $playerHasTargetingScope = true;
                    break;
                }
            }

            if ($playerHasTargetingScope){

            } else {
                $this->game->gamestate->nextState( "finishAction" );
            }

        }

    }

    function getValidTargets()
    {
        $playerId = $this->game->getActivePlayerId();
        return $this->game->getVisibleTargetColorsByDistance($playerId, 2);
    }

    function doAction()
    {
        $targetId = $this->game->getGlobal(Globals::CURRENT_TARGET);

        //else deal damage
        $damage = array(
            $targetId => 3
        );

        $tags = array(
            $targetId => 1
        );

        $this->game->dealDamageAndSendNotifications($damage, $tags);
    }

    function getName()
    {
        return clienttranslate('whisper');
    }

    function sendNotification()
    {
    }


}