<?php

abstract class MoveYourselfAction {
    public static function getInstance($type, $game){
        if ($type == MoveYourselfActions::TELEPORT){
            $instance = new TeleportMoveYourselfAction();
            $instance->game = $game;
            return $instance;
        } elseif ($type == MoveYourselfActions::STANDARD_MOVE){
            $instance = new StandardMoveYourselfAction($game);
            $instance->game = $game;
            return $instance;
        }
    }

    protected abstract function getDescription();
    protected abstract function doActionHook();
    protected abstract function sendSpecificNotifications();
    protected abstract function changeState();
    protected abstract function getAllowedRoomIds();
    protected abstract function getAdditionalNotificationParams();
    protected abstract function getNotificationDescription();

    protected $game = null;
    protected $newRoomId = null;

    public function doAction($newRoomId){
        $this->newRoomId = $newRoomId;
        $this->doActionHook();
    }
}

class StandardMoveYourselfAction extends MoveYourselfAction{

    private $playerId;
    private $possibleMovements;

    function __construct( $game ){
        $this->playerId = $game->getActivePlayerId();
        $originalRoomId = $game->getPlayerRoomId($this->playerId);
        $this->possibleMovements = $game->determinePossibleMovements($originalRoomId, 3);
    }

    function getDescription()
    {
        return "using teleport";
    }

    function getAllowedRoomIds()
    {
        return array_keys($this->possibleMovements);
    }

    function doActionHook()
    {
        //register number of steps
        $numberOfSteps = $this->possibleMovements[$this->newRoomId];
        $this->game->setGlobal(Globals::CURRENT_STEPS_MADE, $numberOfSteps);
    }

    function getAdditionalNotificationParams()
    {
        return array(
            'numberOfSteps' => $this->possibleMovements[$this->newRoomId]
        );
    }

    function getNotificationDescription()
    {
        return clienttranslate('${numberOfSteps} steps');
    }

    function sendSpecificNotifications()
    {
    }

    function changeState()
    {
        //if movement of only one step go to pickup state else continue
        if ($this->possibleMovements[$this->newRoomId] < 2){
            $this->game->gamestate->nextState( "continue" );
        } else {
            $this->game->gamestate->nextState( "finishAction" );
        }
    }
}

class TeleportMoveYourselfAction extends MoveYourselfAction{
    function getDescription()
    {
        return "using Teleport";
    }

    function getAdditionalNotificationParams()
    {
        return array();
    }

    function getAllowedRoomIds()
    {
        $board = $this->game->getCurrentBoard();
        return array_keys($board['rooms']);
    }

    function doActionHook()
    {
    }

    function sendSpecificNotifications()
    {
    }

    function getNotificationDescription()
    {
        return clienttranslate('using Teleport');
    }

    function changeState()
    {
        $this->game->setGlobal(Globals::MOVE_YOURSELF_ACTION, MoveYourselfActions::STANDARD_MOVE);
        if ($this->game->getGlobal(Globals::IS_END_OF_TURN)==1){
            $this->game->setGlobal(Globals::IS_END_OF_TURN, 0);
            $this->game->gamestate->nextState( "finishAction" );
        } else {
            $this->game->gamestate->nextState( "continue" );
        }
    }
}