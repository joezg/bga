<?php

abstract class PaymentAction {
    public static function getInstance($type, $game){
        if ($type == PayActions::PICK_WEAPON){
            $instance = new PickWeaponPaymentAction($game);
            $instance->game = $game;
            return $instance;
        }
    }

    protected abstract function getCosts();
    protected abstract function changeState();

    /** @var Adrenaline */
    protected $game = null;
}

class PickWeaponPaymentAction extends PaymentAction{
    private $cardId = null;
    private $cardDb = null;
    private $card = null;
    private $costs = null;
    private $playerId = null;
    function __construct($game){
        $this->cardId = $game->getGlobal(Globals::CURRENT_CARD_TO_PAY);

        $this->cardDb = $game->weaponDeck->getCard($this->cardId);
        $this->card = $game->weaponCards[$this->cardDb['type']];

        //check if player can pay
        $this->costs = $this->card['cost'];
        $this->costs[$this->card['color']]--;

        $this->playerId = $game->getActivePlayerId();
    }

    function getCosts()
    {
        return $this->costs;
    }

    function changeState(){
        $this->game->gamestate->nextState( "finishAction" );
    }
}