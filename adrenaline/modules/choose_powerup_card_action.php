<?php

abstract class ChoosePowerupCardAction {
    public static function getInstance($type, $game){
        if ($type == ChoosePowerupCardActions::SPAWN){
            $instance = new SpawnChoosePowerupCardAction();
            $instance->game = $game;
            return $instance;
        } elseif ($type == ChoosePowerupCardActions::USE_BEFORE_ACTION){
            $instance = new UseBeforeActionChoosePowerupCardAction();
            $instance->game = $game;
            return $instance;
        } elseif ($type == ChoosePowerupCardActions::USE_AFTER_ACTION){
            $instance = new UseAfterActionChoosePowerupCardAction();
            $instance->game = $game;
            return $instance;
        } elseif ($type == ChoosePowerupCardActions::USE_TAGBACK_GRENADE){
            $instance = new UseTagbackGrenadeActionChoosePowerupCardAction();
            $instance->game = $game;
            return $instance;
        } elseif ($type == ChoosePowerupCardActions::USE_TARGETING_SCOPE){
            $instance = new UseTargetingScopeActionChoosePowerupCardAction();
            $instance->game = $game;
            return $instance;
        }
    }

    abstract function getDescription();
    abstract function getAllowedTypes();
    abstract function doActionHook();
    abstract function sendPostActionNotifications();
    abstract function sendPreActionNotifications();
    abstract function changeState();
    abstract function canSkip();

    /** @var Adrenaline */
    protected $game = null;
    protected $cardDb = null;
    protected $card = null;
    protected $playerId = null;

    public function doAction($cardDb, $playerId){
        $this->cardDb = $cardDb;
        $this->playerId = $playerId;
        $this->card = $this->game->powerupCards[$cardDb['type']];

        $this->doActionHook();
    }

    protected function getCardColor(){
        return $this->card['color'];
    }
}

class SpawnChoosePowerupCardAction extends ChoosePowerupCardAction{
    private $roomId;
    function getDescription(){
        return clienttranslate(" to spawn in corresponding room");
    }

    function canSkip()
    {
        return false;
    }

    function getAllowedTypes(){
        return array( 'newton', 'teleporter', 'tagbackGrenade', 'targetingScope');
    }

    function sendPreActionNotifications()
    {
    }

    function doActionHook(){
        //set player location
        $this->roomId = $this->game->findSpawnPoint($this->getCardColor());
        $this->game->movePlayerToRoom($this->roomId, $this->playerId);
    }
    function sendPostActionNotifications(){
        $this->game->notifyAllPlayers( "spawn", clienttranslate( '${player_name} spawns at ${color} spawn point.'), array(
            'playerId' => $this->playerId,
            'player_name' => $this->game->getActivePlayerName(),
            'color' => $this->getCardColor(),
            'roomId' => $this->roomId,
            'playerColor' => $this->game->getPlayerColorName($this->playerId)
        ) );
    }
    function changeState(){
        $this->game->gamestate->nextState( "action" );
    }

}

class UseBeforeActionChoosePowerupCardAction extends ChoosePowerupCardAction{
    private $roomId;
    function getDescription(){
        return clienttranslate(" to use before this action");
    }

    function canSkip()
    {
        return true;
    }

    function getAllowedTypes(){
        return array( 'newton', 'teleporter');
    }
    function doActionHook(){
    }

    function sendPostActionNotifications()
    {
    }

    function sendPreActionNotifications()
    {
    }

    function changeState(){
        if ($this->card['itemType']=='teleporter'){
            $this->game->setGlobal(Globals::MOVE_YOURSELF_ACTION, MoveYourselfActions::TELEPORT);
            $this->game->gamestate->nextState( "teleport" );
        } elseif ($this->card['itemType']=='newton'){
            $this->game->setGlobal(Globals::TARGET_ACTION, TargetActions::NEWTON);
            $this->game->gamestate->nextState( "newton" );
        }

    }

}

class UseAfterActionChoosePowerupCardAction extends ChoosePowerupCardAction{
    private $roomId;
    function getDescription(){
        return clienttranslate(" to use at the end of a turn or ");
    }

    function canSkip()
    {
        return true;
    }

    function getAllowedTypes(){
        return array( 'newton', 'teleporter');
    }
    function doActionHook(){
    }

    function sendPostActionNotifications()
    {
    }

    function sendPreActionNotifications()
    {
    }

    function changeState(){
        if ($this->card['itemType']=='teleporter'){
            $this->game->setGlobal(Globals::MOVE_YOURSELF_ACTION, MoveYourselfActions::TELEPORT);
            $this->game->gamestate->nextState( "teleport" );
        } elseif ($this->card['itemType']=='newton'){
            $this->game->setGlobal(Globals::TARGET_ACTION, TargetActions::NEWTON);
            $this->game->gamestate->nextState( "newton" );
        }

    }

}

class UseTagbackGrenadeActionChoosePowerupCardAction extends ChoosePowerupCardAction{
    function getDescription(){
        return "";
    }

    function canSkip()
    {
        return true;
    }

    function getAllowedTypes(){
        return array( 'tagbackGrenade');
    }
    function doActionHook(){
        $targetId = $this->game->getGlobal(Globals::CURRENT_TARGET);

        $damage = array();

        $tags = array(
            $targetId => 1
        );
        $this->game->dealDamageAndSendNotifications($damage, $tags);
    }

    function sendPostActionNotifications()
    {
    }

    function sendPreActionNotifications()
    {
    }
    function changeState(){
        $this->game->gamestate->nextState( "finishAction" );
    }

}

class UseTargetingScopeActionChoosePowerupCardAction extends ChoosePowerupCardAction{
    function getDescription(){
        $state = $this->game->gamestate->state();
        if ($state['name'] == 'payTargetingScope'){
            return " to pay for targeting scope";
        } else {
            return "";
        }
    }

    function canSkip()
    {
        return true;
    }

    function getAllowedTypes(){
        $state = $this->game->gamestate->state();
        if ($state['name'] == 'payTargetingScope'){
            return array( 'newton', 'teleporter', 'tagbackGrenade', 'targetingScope');
        } else {
            return array( 'targetingScope');
        }
    }
    function doActionHook(){
        $state = $this->game->gamestate->state();
        if ($state['name'] == 'payTargetingScope'){
            $targetId = $this->game->getGlobal(Globals::CURRENT_TARGET);

            $damage = array(
                $targetId => 1
            );

            $tags = array(
            );
            $this->game->dealDamageAndSendNotifications($damage, $tags);
        }
    }

    function sendPostActionNotifications()
    {
    }

    function sendPreActionNotifications()
    {
    }
    function changeState(){
        $state = $this->game->gamestate->state();
        if ($state['name'] == 'payTargetingScope'){
            $this->game->gamestate->nextState( "finishAction" );
        } else {
            $this->game->gamestate->nextState( "payTargetingScope" );
        }
    }

}