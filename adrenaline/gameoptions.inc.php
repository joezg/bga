<?php $game_options = array (
  100 => 
  array (
    'name' => totranslate('Game length'),
    'values' => 
    array (
      1 => 
      array (
        'name' => totranslate('Normal game'),
        'tmdisplay' => totranslate('Whut dis?'),
        'nobeginner' => true,
      ),
      2 => 
      array (
        'name' => totranslate('Short game without final frenzy'),
      ),
    ),
  ),
); ?>