<?php

/**
 *------
 * BGA framework: © Gregory Isabelli <gisabelli@boardgamearena.com> & Emmanuel Colin <ecolin@boardgamearena.com>
 * MrJack implementation : © <Your name here> <Your email address here>
 *
 * This code has been produced on the BGA studio platform for use on http://boardgamearena.com.
 * See http://en.boardgamearena.com/#!doc/Studio for more information.
 * -----
 *
 * stats.inc.php
 *
 * MrJack game statistics description
 *
 */

/*
    In this file, you are describing game statistics, that will be displayed at the end of the
    game.
    
    !! After modifying this file, you must use "Reload  statistics configuration" in BGA Studio backoffice
    ("Control Panel" / "Manage Game" / "Your Game")
    
    There are 2 types of statistics:
    _ table statistics, that are not associated to a specific player (ie: 1 value for each game).
    _ player statistics, that are associated to each players (ie: 1 value for each player in the game).

    Statistics types can be "int" for integer, "float" for floating point values, and "bool" for boolean
    
    Once you defined your statistics there, you can start using "initStat", "setStat" and "incStat" method
    in your game logic, using statistics names defined below.
    
    !! It is not a good idea to modify this file when a game is running !!

    If your game is already public on BGA, please read the following before any change:
    http://en.doc.boardgamearena.com/Post-release_phase#Changes_that_breaks_the_games_in_progress
    
    Notes:
    * Statistic index is the reference used in setStat/incStat/initStat PHP method
    * Statistic index must contains alphanumerical characters and no space. Example: 'turn_played'
    * Statistics IDs must be >=10
    * Two table statistics can't share the same ID, two player statistics can't share the same ID
    * A table statistic can have the same ID than a player statistics
    * Statistics ID is the reference used by BGA website. If you change the ID, you lost all historical statistic data. Do NOT re-use an ID of a deleted statistic
    * Statistic name is the English description of the statistic as shown to players
    
*/

$stats_type = [

    // Statistics global to table
    "table" => [

        "round_number" => [
            "id"=> 10,
            "name" => totranslate("Number of rounds"),
            "type" => "int" 
        ],
        "mrjack_won" => [
            "id"=> 11,
            "name" => totranslate("Mr. Jack won"),
            "type" => "bool" 
        ],
        "detective_won" => [
            "id"=> 12,
            "name" => totranslate("Detective won"),
            "type" => "bool" 
        ],
        "end_by_capture" => [
            "id"=> 13,
            "name" => totranslate("End by capture attempt"),
            "type" => "bool" 
        ],
        "capture_attempt_successful" => [
            "id"=> 14,
            "name" => totranslate("Capture attempt successful"),
            "type" => "bool" 
        ],
        "end_by_escape" => [
            "id"=> 15,
            "name" => totranslate("End by escape"),
            "type" => "bool" 
        ],
        "end_by_time" => [
            "id"=> 16,
            "name" => totranslate("End by time"),
            "type" => "bool" 
        ],
    ],
    
    // Statistics existing for each player
    "player" => [

        "round_number" => [
            "id"=> 10,
            "name" => totranslate("Number of rounds"),
            "type" => "int" 
        ],
        "won" => [
            "id"=> 11,
            "name" => totranslate("Is winner"),
            "type" => "bool" 
        ],
        "played_as_mrjack" => [
            "id"=> 13,
            "name" => totranslate("Played as Mr. Jack"),
            "type" => "bool" 
        ],
        "played_as_detective" => [
            "id"=> 14,
            "name" => totranslate("Played as detective"),
            "type" => "bool" 
        ],
        "won_as_mrjack" => [
            "id"=> 15,
            "name" => totranslate("Won as Mr. Jack"),
            "type" => "bool" 
        ],
        "won_as_detective" => [
            "id"=> 16,
            "name" => totranslate("Won as detective"),
            "type" => "bool" 
        ],
        "lost_as_mrjack" => [
            "id"=> 17,
            "name" => totranslate("Lost as Mr. Jack"),
            "type" => "bool" 
        ],
        "lost_as_detective" => [
            "id"=> 18,
            "name" => totranslate("Lost as detective"),
            "type" => "bool" 
        ],
        "won_by_capture" => [
            "id"=> 19,
            "name" => totranslate("Won by capture"),
            "type" => "bool" 
        ],
        "lost_by_capture" => [
            "id"=> 20,
            "name" => totranslate("Lost by capture"),
            "type" => "bool" 
        ],
        "won_by_incorrect_capture" => [
            "id"=> 21,
            "name" => totranslate("Won by incorrect capture"),
            "type" => "bool" 
        ],
        "lost_by_incorrect_capture" => [
            "id"=> 22,
            "name" => totranslate("Lost by incorrect capture"),
            "type" => "bool" 
        ],
        "won_by_escape" => [
            "id"=> 23,
            "name" => totranslate("Won by escape"),
            "type" => "bool" 
        ],
        "lost_by_escape" => [
            "id"=> 24,
            "name" => totranslate("Lost by escape"),
            "type" => "bool" 
        ],
        "won_by_time" => [
            "id"=> 25,
            "name" => totranslate("Won by time"),
            "type" => "bool" 
        ],
        "lost_by_time" => [
            "id"=> 26,
            "name" => totranslate("Lost by time"),
            "type" => "bool" 
        ],
        "picked_ms" => [
            "id"=> 27,
            "name" => totranslate("Picked Ms. Stealthy"),
            "type" => "int" 
        ],
        "picked_sh" => [
            "id"=> 28,
            "name" => totranslate("Picked Sherlock Holmes"),
            "type" => "int" 
        ],
        "picked_jhw" => [
            "id"=> 29,
            "name" => totranslate("Picked John H. Watson"),
            "type" => "int" 
        ],
        "picked_swg" => [
            "id"=> 30,
            "name" => totranslate("Picked Sir William Gull"),
            "type" => "int" 
        ],
        "swg_moved" => [
            "id"=> 31,
            "name" => totranslate("Moved Sir William Gull"),
            "type" => "int" 
        ],
        "swg_switch" => [
            "id"=> 32,
            "name" => totranslate("Switched Sir William Gull"),
            "type" => "int" 
        ],
        "picked_sg" => [
            "id"=> 33,
            "name" => totranslate("Picked Sergeant Goodley"),
            "type" => "int" 
        ],
        "picked_js" => [
            "id"=> 34,
            "name" => totranslate("Picked John Smith"),
            "type" => "int" 
        ],
        "picked_jb" => [
            "id"=> 35,
            "name" => totranslate("Picked Jeremy Bert"),
            "type" => "int" 
        ],
        "picked_il" => [
            "id"=> 36,
            "name" => totranslate("Picked Inspector Lestrade"),
            "type" => "int" 
        ],
        "mrjack_visible_percentage" => [
            "id"=> 37,
            "name" => totranslate("Percentage of round when Mr. Jack was visible"),
            "type" => "float" 
        ],
        "innocent_after_1" => [
            "id"=> 38,
            "name" => totranslate("Innocent characters after round 1"),
            "type" => "int" 
        ],
        "innocent_after_2" => [
            "id"=> 39,
            "name" => totranslate("Innocent characters after round 2"),
            "type" => "int" 
        ],
        "innocent_after_3" => [
            "id"=> 40,
            "name" => totranslate("Innocent characters after round 3"),
            "type" => "int" 
        ],
        "innocent_after_4" => [
            "id"=> 41,
            "name" => totranslate("Innocent characters after round 4"),
            "type" => "int" 
        ],
        "innocent_after_5" => [
            "id"=> 42,
            "name" => totranslate("Innocent characters after round 5"),
            "type" => "int" 
        ],
        "innocent_after_6" => [
            "id"=> 43,
            "name" => totranslate("Innocent characters after round 6"),
            "type" => "int" 
        ],
        "innocent_after_7" => [
            "id"=> 44,
            "name" => totranslate("Innocent characters after round 7"),
            "type" => "int" 
        ],
    ]

];
