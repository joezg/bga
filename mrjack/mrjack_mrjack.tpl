{OVERALL_GAME_HEADER}

<!-- 
--------
-- BGA framework: © Gregory Isabelli <gisabelli@boardgamearena.com> & Emmanuel Colin <ecolin@boardgamearena.com>
-- MrJack implementation : © Jurica Hladek (jurica.hladek@gmail.com)
-- 
-- This code has been produced on the BGA studio platform for use on http://boardgamearena.com.
-- See http://en.boardgamearena.com/#!doc/Studio for more information.
-------
-->

<div id="play-area">

    <div id="card-display">
        <div id="character-card-0" class="character-card"></div>
        <div id="character-card-1" class="character-card"></div>
        <div id="character-card-2" class="character-card"></div>
        <div id="character-card-3" class="character-card"></div>
        <div id="character-ability"></div>
    </div>
    
    <div class="info-wrapper">
        <div id="round-number"></div>
        <div class="witness-card-wrapper">
            <div id="witness-card" class="witness-card">
                <div class="witness-card-face back"></div>
                <div class="witness-card-face front"></div>
            </div>
        </div>
        <div class="deck-wrapper">
            <div id="character-card-deck" class="facedown-deck"></div>
            <div><span id="character-card-number"></span>x</div>
        </div>
        <div class="deck-wrapper">
            <div id="alibi-card-deck" class="facedown-deck"></div>
            <div><span id="alibi-card-number"></span>x</div>
        </div>
    </div>

    <div id="board-wrapper">
        <a class="bgabutton bgabutton_blue" id="toggle-lights">Toggle lights</a>
        <div id="responsive-board">
            <div id="board">
                <!-- BEGIN field -->
                <div id="field-{X}-{Y}" class="field {BLOCKED_CLASS} z-order-{Z_ORDER}" style="top: {TOP}%; left: {LEFT}%;">
                    <div id="field-intreaction-{X}-{Y}" class="field-interaction" data-x={X} data-y={Y}></div>
                    <div class="field-visuals"></div>
                    <div class="token" id="token-{X}-{Y}"></div>
                    <div class="pawn" id="pawn-{X}-{Y}">
                        <div class="pawn-visual">
                            <div class="pawn-visual-face back"></div>
                            <div class="pawn-visual-face front"></div>
                        </div>
                        <div class="pawn-effect"></div>
                    </div>
                    <div class="lights-wrapper" id="lights-{X}-{Y}">
                    </div>
                </div>
                <!-- END field -->

                <div id="police-cordon-nw" class="police-cordon" data-position="nw">                </div>
                <div id="police-cordon-sw" class="police-cordon" data-position="sw">                </div>
                <div id="police-cordon-ne" class="police-cordon" data-position="ne">                </div>
                <div id="police-cordon-se" class="police-cordon" data-position="se">                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
// Javascript HTML templates

var jstpl_player_board = '\
<div>\
    <span id="player-role-${id}"></span>\
    <div class="character-and-alibi-cards">\
        <div id="mrjack-character-${id}" class="character-appearance">\
        </div>\
        <div id="private-cards-${id}" class="private-cards">\
        </div>\
    </div>\
</div>';

var jstpl_character_ability = '\
<div class="character-ability-box">\
    <div class="character-info">\
        <div class="character-appearance ${id}"></div>\
        <div class="character-text">\
            <div class="character-name"><h2>${name}</h2></div>\
            <div class="character-ability">\
                <h3>${specialAbilityHeader} (${isMandatory}):</h3>\
                ${ability}\
            </div>\
            <div class="character-status">\
                <h3>${characterStatus}</h3>\
            </div>\
        </div>\
    </div>\
    <div class="character-ability-interactions">\
        <a class="character-ability-use-button bgabutton bgabutton_blue">{USE_ABILITY_LABEL}</a>\
        <a class="character-ability-cancel-button bgabutton bgabutton_blue">{CANCEL_LABEL}</a>\
        <p><small>{CANCEL_HELP}</small></p>\
    </div>\
</div>';

var jstpl_light = '\
    <div class="lights lights-${type}">\
        <div class="lights-transform">\
        </div>\
    </div>';

var jstpl_roundTooltip = '\
    <div class="round-tooltip">\
        <div class="round-number">\
            <h2>Round ${round}/8</h2>\
        </div>\
        <div class="turn-order">\
            <h2>Turn order</h2>\
            ${turnOrder}\
        </div>\
        <div class="round-end">\
            <h2>Round end</h2>\
            ${roundEnd}\
        </div>\
    </div>';

var jstpl_endDialogContent = '\
    <div class="end-game-dialog">\
        <div class="message">\
            ${message}\
        </div>\
        <a class="bgabutton bgabutton_blue confirm-button">OK</a>\
    </div>';

</script>  

{OVERALL_GAME_FOOTER}
