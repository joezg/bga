<?php
/**
 *------
 * BGA framework: © Gregory Isabelli <gisabelli@boardgamearena.com> & Emmanuel Colin <ecolin@boardgamearena.com>
 * MrJack implementation : © <Your name here> <Your email address here>
 *
 * This code has been produced on the BGA studio platform for use on http://boardgamearena.com.
 * See http://en.boardgamearena.com/#!doc/Studio for more information.
 * -----
 * 
 * states.inc.php
 *
 * MrJack game states description
 *
 */

/*
   Game state machine is a tool used to facilitate game developpement by doing common stuff that can be set up
   in a very easy way from this configuration file.

   Please check the BGA Studio presentation about game state to understand this, and associated documentation.

   Summary:

   States types:
   _ activeplayer: in this type of state, we expect some action from the active player.
   _ multipleactiveplayer: in this type of state, we expect some action from multiple players (the active players)
   _ game: this is an intermediary state where we don't expect any actions from players. Your game logic must decide what is the next game state.
   _ manager: special type for initial and final state

   Arguments of game states:
   _ name: the name of the GameState, in order you can recognize it on your own code.
   _ description: the description of the current game state is always displayed in the action status bar on
                  the top of the game. Most of the time this is useless for game state with "game" type.
   _ descriptionmyturn: the description of the current game state when it's your turn.
   _ type: defines the type of game states (activeplayer / multipleactiveplayer / game / manager)
   _ action: name of the method to call when this game state become the current game state. Usually, the
             action method is prefixed by "st" (ex: "stMyGameStateName").
   _ possibleactions: array that specify possible player actions on this step. It allows you to use "checkAction"
                      method on both client side (Javacript: this.checkAction) and server side (PHP: self::checkAction).
   _ transitions: the transitions are the possible paths to go from a game state to another. You must name
                  transitions in order to use transition names in "nextState" PHP method, and use IDs to
                  specify the next game state for each transition.
   _ args: name of the method to call to retrieve arguments for this gamestate. Arguments are sent to the
           client side to be used on "onEnteringState" or to set arguments in the gamestate description.
   _ updateGameProgression: when specified, the game progression is updated (=> call to your getGameProgression
                            method).
*/

//    !! It is not a good idea to modify this file when a game is running !!

 
$machinestates = [

    // The initial state. Please do not modify.
    1 => [
        "name" => "gameSetup",
        "description" => "",
        "type" => "manager",
        "action" => "stGameSetup",
        "transitions" => ["" => 2]
    ],
    
    2 => [
        "name" => "roundSetup",
        "description" => "",
        "type" => "game",
        "action" => "stRoundSetup",
        "transitions" => ["pickCharacter" => 10]
    ],

    10 => [
        "name" => "pickCharacterCard",
        "description" => clienttranslate('${actplayer} must pick a character'),
        "descriptionmyturn" => clienttranslate('${you} must pick ${characterText} (click on a card or token)'),
        "type" => "activeplayer",
        "possibleactions" => ["selectCharacterCard"],
        "transitions" => ["moveCharacter" => 20, "moveCharacterWithAbility" => 15],
        "args" => "argPickCharacterCard",
        "updateGameProgression" => true
    ],

    15 => [
        "name" => "moveCharacterWithAbility",
        "description" => clienttranslate('${actplayer} must move the character or use ability'),
        "descriptionmyturn" => clienttranslate('${you} must move the character or use ability'),
        "type" => "activeplayer",
        "possibleactions" => ["moveCharacter", "characterExit", "characterCapture", "decideAbility", "cancelCharacter"],
        "transitions" => [
            "moveGaslight" => 30,
            "moveCordon" => 32,
            "moveManhole" => 34,
            "switchPlace" => 37,
            "moveCloser" => 38,
            "pickCharacter" => 10,
            "endTurn" => 60,
            "endGame" => 99, 
        ],
        "args" => "argMoveCharacter"
    ],

    16 => [
        "name" => "afterSpecialAction",
        "type" => "game",
        "action" => "stAfterSpecialAction",
        "transitions" => ["endTurn" => 60, "moveCharacter" => 20]
    ],

    20 => [
        "name" => "moveCharacter",
        "description" => clienttranslate('${actplayer} must move the character'),
        "descriptionmyturn" => clienttranslate('${you} must move the character'),
        "type" => "activeplayer",
        "possibleactions" => ["moveCharacter", "characterExit", "characterCapture", "cancelCharacter"],
        "transitions" => [
            "endTurn" => 60, 
            "endGame" => 99, 
            "pickCharacter" => 10,
            "dealAlibiCard" => 36,
            "rotateWatson" => 41,
        ],
        "args" => "argMoveCharacter"
    ],
    
    30 => [
        "name" => "selectSourceGaslight",
        "description" => clienttranslate('${actplayer} must move one gaslight'),
        "descriptionmyturn" => clienttranslate('${you} must move one gaslight'),
        "type" => "activeplayer",
        "possibleactions" => ["selectSourceGaslight", "cancelAbility"],
        "transitions" => ["selected" => 31, "cancel" => 15],
        "args" => "argSelectSourceGaslight"
    ],

    31 => [
        "name" => "selectGaslightDestination",
        "description" => clienttranslate('${actplayer} must decide where to move selected gaslight'),
        "descriptionmyturn" => clienttranslate('${you} must decide where to move selected gaslight'),
        "type" => "activeplayer",
        "possibleactions" => ["selectGaslightDestination", "cancelSelection"],
        "transitions" => ["afterSpecialAction" => 16, "cancelSelection" => 30],
        "args" => "argSelectGaslightDestination"
    ],
    
    32 => [
        "name" => "selectSourceCordon",
        "description" => clienttranslate('${actplayer} must select one cordon to move'),
        "descriptionmyturn" => clienttranslate('${you} must select one cordon to move'),
        "type" => "activeplayer",
        "possibleactions" => ["selectSourceCordon", "cancelAbility"],
        "transitions" => ["selected" => 33, "cancel" => 15],
        "args" => "argMoveCordon"
    ],
    
    33 => [
        "name" => "selectCordonDestination",
        "description" => clienttranslate('${actplayer} must decide where to move selected cordon'),
        "descriptionmyturn" => clienttranslate('${you} must decide where to move selected cordon'),
        "type" => "activeplayer",
        "possibleactions" => ["selectCordonDestination", "cancelSelection"],
        "transitions" => ["afterSpecialAction" => 16, "cancelSelection" => 32],
        "args" => "argMoveCordon"
    ],
    34 => [
        "name" => "selectSourceManhole",
        "description" => clienttranslate('${actplayer} must select one manhole to move'),
        "descriptionmyturn" => clienttranslate('${you} must select one manhole to move'),
        "type" => "activeplayer",
        "possibleactions" => ["selectSourceManhole", "cancelAbility"],
        "transitions" => ["selected" => 35, "cancel" => 15],
        "args" => "argSelectSourceManhole"
    ],
    
    35 => [
        "name" => "selectManholeDestination",
        "description" => clienttranslate('${actplayer} must decide where to move selected manhole'),
        "descriptionmyturn" => clienttranslate('${you} must decide where to move selected manhole'),
        "type" => "activeplayer",
        "possibleactions" => ["selectManholeDestination", "cancelSelection"],
        "transitions" => ["afterSpecialAction" => 16, "cancelSelection" => 34],
        "args" => "argSelectManholeDestination"
    ],

    36 => [
        "name" => "dealAlibiCard",
        "description" => "",
        "type" => "game",
        "action" => "stDealAlibiCard",
        "transitions" => ["afterSpecialAction" => 16 ]
    ],

    37 => [
        "name" => "switchPlace",
        "description" => clienttranslate('${actplayer} must select character to switch place with'),
        "descriptionmyturn" => clienttranslate('${you} must select character to switch place with'),
        "type" => "activeplayer",
        "possibleactions" => ["switchPlace", "cancelAbility"],
        "transitions" => ["endTurn" => 60, "cancel" => 15],
        "args" => "argOtherCharacters"
    ],

    38 => [
        "name" => "startMoveCloser",
        "description" => "",
        "type" => "game",
        "action" => "stStartMovecloser",
        "transitions" => ["pickCharacterTomoveCloser" => 39 ]
    ],

    39 => [
        "name" => "pickCharacterToMoveCloser",
        "description" => clienttranslate('${actplayer} must select character to move closer to Sergeant Goodley (${count} movements left)'),
        "descriptionmyturn" => clienttranslate('${you} must select character to move closer to Sergeant Goodley (${count} movements left)'),
        "type" => "activeplayer",
        "possibleactions" => ["pickCharacterToMoveCloser", "cancelAbility"],
        "transitions" => ["moveCloser" => 40, "cancel" => 15],
        "args" => "argPickCharacterToMoveCloser"
    ],
    
    40 => [
        "name" => "moveCloser",
        "description" => clienttranslate('${actplayer} must move ${characterName} closer to Sergeant Goodley (${count} movements left)'),
        "descriptionmyturn" => clienttranslate('${you} must move ${characterName} closer to Sergeant Goodley (${count} movements left)'),
        "type" => "activeplayer",
        "possibleactions" => ["moveCloser", "cancelCharacter"],
        "transitions" => [
            "movementPointsLeft" => 39,
            "afterSpecialAction" => 16,
            "rotateWatson" => 41 
        ],
        "args" => "argMoveCloser"
    ],
    
    41 => [
        "name" => "rotateWatson",
        "description" => clienttranslate('${actplayer} must chose final Watson orientation'),
        "descriptionmyturn" => clienttranslate('${you} must chose final Watson orientation'),
        "type" => "activeplayer",
        "possibleactions" => ["rotateWatson"],
        "transitions" => [
            "endTurn" => 60,
            "movementPointsLeft" => 39,
            "afterSpecialAction" => 16,
        ],
        "args" => "argRotateWatson"
    ],

    60 => [
        "name" => "turnEnd",
        "description" => "",
        "type" => "game",
        "action" => "stTurnEnd",
        "transitions" => ["pickCharacterCard" => 10, "newRound" => 70]
    ],
    
    70 => [
        "name" => "roundWrapUp",
        "description" => "",
        "type" => "game",
        "action" => "stRoundWrapUp",
        "transitions" => ["nextRound" => 2, "endGame" => 99]
    ],
    
    // Final state.
    // Please do not modify (and do not overload action/args methods).
    99 => [
        "name" => "gameEnd",
        "description" => clienttranslate("End of game"),
        "type" => "manager",
        "action" => "stGameEnd",
        "args" => "argGameEnd"
    ]

];



