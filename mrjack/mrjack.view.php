<?php
/**
 *------
 * BGA framework: © Gregory Isabelli <gisabelli@boardgamearena.com> & Emmanuel Colin <ecolin@boardgamearena.com>
 * MrJack implementation : © <Your name here> <Your email address here>
 *
 * This code has been produced on the BGA studio platform for use on http://boardgamearena.com.
 * See http://en.boardgamearena.com/#!doc/Studio for more information.
 * -----
 *
 * mrjack.view.php
 *
 * This is your "view" file.
 *
 * The method "build_page" below is called each time the game interface is displayed to a player, ie:
 * _ when the game starts
 * _ when a player refreshes the game page (F5)
 *
 * "build_page" method allows you to dynamically modify the HTML generated for the game interface. In
 * particular, you can set here the values of variables elements defined in mrjack_mrjack.tpl (elements
 * like {MY_VARIABLE_ELEMENT}), and insert HTML block elements (also defined in your HTML template file)
 *
 * Note: if the HTML of your game interface is always the same, you don't have to place anything here.
 *
 */
  
require_once( APP_BASE_PATH."view/common/game.view.php" );

class view_mrjack_mrjack extends game_view
{
  function getGameName() {
    return "mrjack";
  }    
  
  function build_page( $viewArgs )
  {		
    $this->page->begin_block("mrjack_mrjack", "field");

    foreach ($this->game->getBoardFields() as $field) {
      $this->page->insert_block("field", [
        "TOP" => $this->getTopOffset($field->getX(), $field->getY()),
        "LEFT" => self::LEFT_OFFSETS[$field->getX()],
        "X" => $field->getX(),
        "Y" => $field->getY(),
        "Z_ORDER" => $field->getX() + $field->getY(),
        "BLOCKED_CLASS" => !$field->isPassable() ? "blocked" : ""
      ]);
    }

    $this->tpl["USE_ABILITY_LABEL"] = self::_("Use ability");
    $this->tpl["CANCEL_LABEL"] = self::_("Cancel");
    $this->tpl["CANCEL_HELP"] = self::_("(You can also cancel by clicking on selected character pawn)");

  }

  function getTopOffset($x, $y){
    $remainder = abs($x % 2);
    $offset = self::TOP_OFFSETS[$remainder];

    if ($remainder == 1){
      $x--;
    }

    return $offset[2 + $y - $x/2];
  }

  const LEFT_OFFSETS = [
    -1 => 94.95,
    0 => 87.8,
    1 => 80.7,
    2 => 73.55,
    3 => 66.45,
    4 => 59.3,
    5 => 52.15,
    6 => 45.05,
    7 => 37.95,
    8 => 30.85,
    9 => 23.65,
    10 => 16.5,
    11 => 9.3,
    12 => 2.25,
    13 => -4.8
  ];

  const TOP_OFFSETS = [
    0 => [
      0 => 92.05,
      1 => 81.75,
      2 => 71.4,
      3 => 61.05,
      4 => 50.8,
      5 => 40.5,
      6 => 30.2,
      7 => 19.85,
      8 => 9.45,
      9 => -0.8
    ],
    1 => [
      0 => 97.25,
      1 => 86.85,
      2 => 76.50,
      3 => 66.2,
      4 => 55.9,
      5 => 45.7,
      6 => 35.35,
      7 => 25.05,
      8 => 14.75,
      9 => 4.45,
      10 => -6.1,
    ]
  ];
}
  

