/**
 *------
 * BGA framework: © Gregory Isabelli <gisabelli@boardgamearena.com> & Emmanuel Colin <ecolin@boardgamearena.com>
 * MrJack implementation : © <Your name here> <Your email address here>
 *
 * This code has been produced on the BGA studio platform for use on http://boardgamearena.com.
 * See http://en.boardgamearena.com/#!doc/Studio for more information.
 * -----
 *
 * mrjack.js
 *
 * MrJack user interface script
 * 
 * In this file, you are describing the logic of your user interface, in Javascript language.
 *
 */

define([
    "dojo","dojo/_base/declare",
    "dojo/fx",
    "dojo/_base/fx",
    "dojo/dom-style",
    "dojo/NodeList-traverse",
    "ebg/core/gamegui",
    "ebg/counter",
    "ebg/stock"
],
function (dojo, declare, fx, baseFx, domStyle) {
    var gamegui = declare("bgagame.mrjack", ebg.core.gamegui, {
        constructor: function(){
            this.playerInfoBoxes = {};
            this.gameBoard = GameBoard(this);

            eventHandles.attach(dojo.byId("toggle-lights"), "toggleLights", "click", function(){
                dojo.addClass(dojo.byId("board"), "lights-flicker");
                dojo.toggleClass(dojo.byId("board"), "lights-off");
                setTimeout(function(){
                    dojo.removeClass(dojo.byId("board"), "lights-flicker");
                }, 1000);
            })

            eventHandles.attach(dojo.byId("pageheader_gameview"), "zoomout", "click", function(){
                dojo.removeAttr(dojo.byId("board"), "style");
            })

            this.addTooltip("character-card-deck", _("Remaining character cards"), "");
            this.addTooltip("alibi-card-deck", _("Remaining alibi cards"), "");
        },
        
        /*
            setup:
            
            This method must set up the game user interface according to current game situation specified
            in parameters.
            
            The method is called each time the game interface is displayed to a player, ie:
            _ when the game starts
            _ when a player refreshes the game page (F5)
            
            "gameData" argument contains all datas retrieved by your "getAllDatas" PHP method.
        */
        
        setup: function( gameData )
        {
            // Setting up player panels
            for(var playerId in gameData.players )
            {
                var player = gameData.players[playerId];

                this.createPlayerInfoBox(player, gameData.isMrJack, gameData.mrJackCharacterId);
            }

            this.gameBoard.setTokens(JSON.parse(gameData.tokens));

            var activePoliceCordons = Object.values(JSON.parse(gameData.policeCordons)).filter(function(policeCordon){
                return policeCordon.isActive;
            })
            this.gameBoard.setPoliceCordons(activePoliceCordons);

            for (var playerId in gameData.privateAlibiCards) {
                if (gameData.privateAlibiCards.hasOwnProperty(playerId)) {
                    var alibiCardHand = gameData.privateAlibiCards[playerId];

                    if (+playerId === this.player_id){
                        this.playerInfoBoxes[playerId].addAlibiCards(Object.values(alibiCardHand));
                    } else {
                        this.playerInfoBoxes[playerId].addAlibiCardBacks(alibiCardHand);
                    }
                }
            }
            
            this.gameBoard.setCardDisplay(Object.values(gameData.characterCardsDisplay));
            this.gameBoard.setRemainingAlibiCardsNumber(gameData.remainingAlibiCardsNumber);
            this.gameBoard.setRemainingCharacterCardsNumber(gameData.remainingCharacterCardsNumber)
            this.gameBoard.setWitnessVisibility(gameData.mrJackWasVisible);
            this.gameBoard.setRound(gameData.roundNumber);
            this.gameBoard.setLights(gameData.lights);
            
            // Setup game notifications to handle (see "setupNotifications" method below)
            this.setupNotifications();
        },
       

        ///////////////////////////////////////////////////
        //// Game & client states
        
        // onEnteringState: this method is called each time we are entering into a new game state.
        //                  You can use this method to perform some user interface changes at this moment.
        //
        onEnteringState: function( stateName, args )
        {
            console.log( 'Entering state: '+stateName );

            switch( stateName )
            {
                case "pickCharacterCard":
                    this.gameBoard.deselectAllPawns();
                    this.gameBoard.deselectAllCards();
                    this.gameBoard.deactivateAbilityBoxInteractions();
                    if (this.isCurrentPlayerActive()){
                        this.gameBoard.activateCardDisplay(this.onCharacterCardSelected.bind(this));
                        this.gameBoard.activateFields(Object.values(args.args.fields), this.onSelectCharacterByField.bind(this), "available");
                    }
                    break;
                case "moveCharacterWithAbility":
                case "moveCharacter":
                    if (this.isCurrentPlayerActive()){
                        var possibleFields = args.args._private.possibleMoves.fields;
                        this.gameBoard.activateMoves(Object.values(possibleFields), this.onMoveCharacter.bind(this))

                        var possibleCaptures = args.args._private.possibleMoves.captures;
                        this.gameBoard.activateCaptures(Object.values(possibleCaptures), this.onCharacterCapture.bind(this));

                        var possibleExits = args.args._private.possibleMoves.exits;
                        this.gameBoard.activateEscapes(Object.values(possibleExits), this.onCharacterExit.bind(this));                        

                        if (args.args.canCancel){
                            this.gameBoard.activateAbilityBoxInteractions(
                                this.onCancelCharacter.bind(this),
                                stateName == "moveCharacterWithAbility" ? this.onDecideAbility.bind(this) : null
                            );

                            this.gameBoard.activateNonActivatedFields(this.onCancelCharacter.bind(this), "cancel");
                        }
                    }
                    break;
                case "selectSourceCordon":
                    if (this.isCurrentPlayerActive()){
                        activeCordons = Object.values(args.args.cordons).filter(function(cordon){
                            return cordon.isActive;
                        });

                        this.gameBoard.activatePoliceCordons(activeCordons, this.onSelectSourceCordon.bind(this));                        
                    }
                    break;
                case "selectCordonDestination":
                    if (this.isCurrentPlayerActive()){
                        inactiveCordons = Object.values(args.args.cordons).filter(function(cordon){
                            return !cordon.isActive;
                        });
                        
                        this.gameBoard.activatePoliceCordons(inactiveCordons, this.onSelectCordonDestination.bind(this));  
                    }
                    break;
                case "selectSourceManhole":
                    if (this.isCurrentPlayerActive()){
                        this.gameBoard.activateFields(Object.values(args.args.manholes), this.onSelectSourceManhole.bind(this), "select manhole");
                    }
                    break;
                case "selectManholeDestination":
                    if (this.isCurrentPlayerActive()){
                        this.gameBoard.activateFields(Object.values(args.args.fields), this.onSelectManholeDestination.bind(this), "select manhole");
                    }
                    break;
                case "selectSourceGaslight":
                    if (this.isCurrentPlayerActive()){
                        this.gameBoard.activateFields(Object.values(args.args.gaslights), this.onSelectSourceGaslight.bind(this), "select");
                    }
                    break;
                case "selectGaslightDestination":
                    if (this.isCurrentPlayerActive()){
                        this.gameBoard.activateFields(Object.values(args.args.fields), this.onSelectGaslightDestination.bind(this), "select");
                    }
                    break;
                case "switchPlace":
                    if (this.isCurrentPlayerActive()){
                        this.gameBoard.activateFields(Object.values(JSON.parse(args.args.characters)), this.onSwitchPlace.bind(this), "available");
                    }
                    break;
                
                case "pickCharacterToMoveCloser":
                    if (this.isCurrentPlayerActive()){
                        this.gameBoard.activateFields(Object.values(JSON.parse(args.args.characters)), this.onPickCharacterToMoveCloser.bind(this), "available");
                    }
                    break;
                
                case "moveCloser":
                    if (this.isCurrentPlayerActive()){
                        this.gameBoard.activateMoves(Object.values(JSON.parse(args.args.distances)), this.onMoveCloser.bind(this));

                        this.gameBoard.activateNonActivatedFields(this.onCancelCharacter.bind(this), "cancel");
                    }
                    break;
                
                case "rotateWatson":
                    if (this.isCurrentPlayerActive()){
                        Object.values(JSON.parse(args.args.fields)).forEach(function(field){
                            this.gameBoard.activateField(field.field, this.onRotateWatson.bind(this), "rotate rotate-" + field.direction);       
                        }.bind(this));
                    }
                    break;
            }
        },

        // onLeavingState: this method is called each time we are leaving a game state.
        //                 You can use this method to perform some user interface changes at this moment.
        //
        onLeavingState: function( stateName )
        {
            console.log( 'Leaving state: '+stateName );
            
            switch( stateName )
            {
                case "pickCharacterCard":
                    this.gameBoard.deactivateCardDisplay();
                    this.gameBoard.deactivateAllFields();
                    break;
                case "moveCharacter":
                case "moveCharacterWithAbility":
                    this.gameBoard.deactivateAbilityBoxInteractions();
                case "selectSourceGaslight":
                case "selectSourceCordon":
                case "selectCordonDestination":
                case "selectSourceManhole":
                case "selectManholeDestination":
                case "selectSourceGaslight":
                case "selectGaslightDestination":
                case "switchPlace":
                case "pickCharacterToMoveCloser":
                case "moveCloser":
                case "rotateWatson":
                    this.gameBoard.deactivateAllFields();
                    this.gameBoard.deactivateAllPoliceCordons();
                    break;
            }               
        }, 

        // onUpdateActionButtons: in this method you can manage "action buttons" that are displayed in the
        //                        action status bar (ie: the HTML links in the status bar).
        //        
        onUpdateActionButtons: function( stateName, args )
        {
            console.log( 'onUpdateActionButtons: '+stateName );
                      
            if( this.isCurrentPlayerActive() )
            {            
                switch( stateName )
                {
                    case "moveCharacter":
                    case "moveCharacterWithAbility":
                        stateName == "moveCharacterWithAbility" && this.addActionButton( 'decideAbility', _('Use ability'), 'onDecideAbility' );
                        args.canCancel && this.addActionButton( 'cancelCharacter', _('Cancel'), 'onCancelCharacter' );
                        break;                    
                    case "selectSourceCordon":
                    case "selectSourceManhole":
                    case "selectSourceGaslight":
                    case "switchPlace":
                    case "pickCharacterToMoveCloser":
                        args.canCancel && this.addActionButton( 'cancelAbility', _('Cancel ability'), 'onCancelAbility' );
                        break;
                    case "selectCordonDestination":
                    case "selectManholeDestination":
                    case "selectGaslightDestination":
                        this.addActionButton( 'cancelSelection', _('Cancel selection'), 'onCancelSelection' );
                }
            }
        },    
        
        /* @Override */
        format_string_recursive : function(log, args) {
            try {
                if (log && args && !args.processed) {
                    args.processed = true;
                    
                    for (arg in args){
                        if (args.hasOwnProperty(arg)){
                            if (typeof args[arg] == 'string') {
                                [
                                    "Miss Stealthy",
                                    "Sherlock Holmes",
                                    "Inspector Lestrade",
                                    "Jeremy Bert",
                                    "John H. Watson",
                                    "John Smith",
                                    "Sergeant Goodley",
                                    "Sir William Gull"
                                ].forEach(function(characterName){
                                    if (characterName.indexOf(characterName) > -1){
                                        switch (characterName) {
                                            case "Miss Stealthy":
                                                substitute = '<span style="color: #00aa00;">' + characterName + '</span>';
                                                break;
                                            case "Sherlock Holmes":
                                                substitute = '<span style="color: #ff0000;">' + characterName + '</span>';
                                                break;
                                            case "Inspector Lestrade":
                                                substitute = '<span style="color: #0067f4;">' + characterName + '</span>';
                                                break;
                                            case "Jeremy Bert":
                                                substitute = '<span style="color: #fea100;">' + characterName + '</span>';
                                                break;
                                            case "John H. Watson":
                                                substitute = '<span style="color: #682e00;">' + characterName + '</span>';
                                                break;
                                            case "John Smith":
                                                substitute = '<span style="color: #CCCC00;">' + characterName + '</span>';
                                                break;
                                            case "Sergeant Goodley":
                                                substitute = '<span style="color: #7f7f7f;">' + characterName + '</span>';
                                                break;
                                            case "Sir William Gull":
                                                substitute = '<span style="color: #9c00ff;">' + characterName + '</span>';
                                                break;
                                        
                                            default:
                                                substitute = characterName;
                                                break;
                                        }

                                        args[arg] = args[arg].replace(characterName, substitute);
                                    }
                                })
                            }
                        }
                    }

                }
            } catch (e) {
                console.error(log,args,"Exception thrown", e.stack);
            }
            return this.inherited(arguments);
        },

        ///////////////////////////////////////////////////
        //// Utility methods
        
        /*
        
            Here, you can defines some utility methods that you can use everywhere in your javascript
            script.
        
        */
       createCardStock : function(elementId, imageOffset, imageBackPosition, overlap, onItemCreate){
            imageOffset == undefined && (imageOffset = 0);
            overlap == undefined && (overlap = false);
            var stock = new ebg.stock();
            stock.create(this, $(elementId), 53, 81);
            stock.image_items_per_row = 4;

            ["il", "ms", "jb", "jhw", "swg", "sg", "js", "sh", "back"].forEach(function(id, index){
                stock.addItemType(id, index, g_gamethemeurl + 'img/alibi_cards.jpg', index + imageOffset);
            });
            stock.addItemType("back", imageBackPosition, g_gamethemeurl + 'img/alibi_cards.jpg', imageBackPosition);
            
            stock.setSelectionMode(0);

            if (overlap){
                stock.setOverlap(30, 0);
            }

            stock.onItemCreate = onItemCreate;

            return stock;
        },
        createPlayerInfoBox: function(player, isMrJack, mrJackCharacterId){
            var playerInfoBox = PlayerInfoBox(player, this);
            
            this.playerInfoBoxes[player.id] = playerInfoBox;
            if (this.player_id == player.id ? isMrJack : !isMrJack){
                playerInfoBox.setPlayerRole("Mr.Jack", mrJackCharacterId);
            } else {
                playerInfoBox.setPlayerRole("Detective");                    
            }
        },
        createDialog: function(message){
            this.dialog = new ebg.popindialog();
            this.dialog.create('winConditionDialog');
            
            var html = this.format_block("jstpl_endDialogContent", { message: message });

            this.dialog.setContent( html );
            this.dialog.show();
            this.dialog.hideCloseIcon();

            eventHandles.attachQuery(".end-game-dialog .confirm-button", "confirmDialog", "onclick", function(){
                this.dialog.destroy();
            }.bind(this));
        },
        gameEndDialog: function(message){
            this.createDialog(message);
        },
        addCharacterTooltip: function(nodeId, characterId, characterStatus){
            this.addTooltipHtml(nodeId, this.getCharacterAbilityMarkup(characterId, characterStatus));
        },
        addCharacterTooltipWithInnocenceStatus: function(nodeId, characterId, isInnocent){
            this.addCharacterTooltip(nodeId, characterId, isInnocent ? _("Couldn't be Mr. Jack") : _("Could be Mr. Jack"))
        },
        getCharacterAbilityMarkup: function(characterId, characterStatus){
            var meta = Object.assign({}, this.getCharacterMeta(characterId));
            meta.characterStatus = "";

            if (characterStatus !== undefined){
                meta.characterStatus =  characterStatus;
            }

            return this.format_block('jstpl_character_ability', meta);
        },
        getCharacterMeta: function(characterId){
            var meta = {
                ms: {
                    name: _("Miss Stealthy"),
                    ability: _("During her movement, Miss Stealthy can cross any hex (building, gaslight, garden) but she must stop her movement on a street hex."),
                    id: "ms",
                    minMovement: 1,
                    maxMovement: 4,
                    specialAbilityHeader: _("Move through blocked spaces"),
                    isMandatory: _("optional")
                },
                jhw: {
                    name: _("John H. Watson"),
                    ability: _("Watson bears a lantern, pictured on his character token. This lantern illuminates all the characters standing straight ahead of him!! (It is important to note that Watson himself is not illumated by the lantern!!)<br>Any player moving Watson, one way or another, chooses the final facing of the character, and therefore the line illuminated by the lantern."),
                    id: "jhw",
                    minMovement: 1,
                    maxMovement: 3,
                    specialAbilityHeader: _("Use lantern"),
                    isMandatory: _("mandatory")
                },
                sh: {
                    name: _("Sherlock Holmes"),
                    ability: _("AFTER MOVEMENT, Sherlock Holmes secretly draws the first card from the alibi pile and places it face down in front of him."),
                    id: "sh",
                    minMovement: 1,
                    maxMovement: 3,
                    specialAbilityHeader: _("Deal alibi card"),
                    isMandatory: _("mandatory")
                },
                js: {
                    name: _("John Smith"),
                    ability: _("Move one of the lit gaslight tiles onto one of the shut-off gaslight hex. This ability can be used before or after the movement, as the player sees fit."),
                    id: "js",
                    minMovement: 1,
                    maxMovement: 3,
                    specialAbilityHeader: _("Move gaslight"),
                    isMandatory: _("mandatory")
                },
                il: {
                    name: _("Inspector Lestrade"),
                    ability: _("Move one police cordon. This will free one of the exits but block another!! This ability can be used before or after the movement, as the player sees fit."),
                    id: "il",
                    minMovement: 1,
                    maxMovement: 3,
                    specialAbilityHeader: _("Move police cordon"),
                    isMandatory: _("mandatory")
                },
                sg: {
                    name: _("Sergeant Goodley"),
                    ability: _("Sergeant Goodley calls for help with his whistle!! You then get 3 movement points to use as you see fit on one or several characters in order to bring them closer to Sergeant Goodley!! This ability can be used before or after the movement, as the player sees fit."),
                    id: "sg",
                    minMovement: 1,
                    maxMovement: 3,
                    specialAbilityHeader: _("Blow whistle"),
                    isMandatory: _("mandatory")
                },
                swg: {
                    name: _("Sir William Gull"),
                    ability: _("Instead of moving normally William Gull, you can exchange this character’s location with the location of any other character."),
                    id: "swg",
                    minMovement: 1,
                    maxMovement: 3,
                    specialAbilityHeader: _("Change position with another character"),
                    isMandatory: _("optional")
                },
                jb: {
                    name: _("Jeremy Bert"),
                    ability: _("Journalists like to search where it stinks... Jeremy Bert opens a manhole and closes another (move one covered manhole tile on any other open manhole hex.) This ability can be used before or after the movement, as the player sees fit."),
                    id: "jb",
                    minMovement: 1,
                    maxMovement: 3,
                    specialAbilityHeader: _("Move manhole cover"),
                    isMandatory: "mandatory"
                },
            }

            return meta[characterId];
        },
        
        ///////////////////////////////////////////////////
        //// Player's action
        
        /*
        
            Here, you are defining methods to handle player's action (ex: results of mouse click on 
            game objects).
            
            Most of the time, these methods:
            _ check the action is possible at this game state.
            _ make a call to the game server
        
        */

        onCharacterCardSelected: function(e){
            var cardType = dojo.attr(dojo.byId(e.target.id), "cardType");

            if (this.checkAction( "selectCharacterCard" )){
                this.ajaxcall( '/mrjack/mrjack/selectCharacterCard.html', { 
                    lock: true, 
                    type: cardType, 
                }, this, function(){});
            }
            
        },

        onSelectCharacterByField: function(e) {
            var x = +dojo.attr(e.currentTarget, 'data-x'),
                y = +dojo.attr(e.currentTarget, 'data-y');

            if (this.checkAction( "selectCharacterCard" )){
                this.ajaxcall( '/mrjack/mrjack/selectCharacterByField.html', { 
                    lock: true, 
                    x: x,
                    y: y 
                }, this, function(){});
            }
        },

        onMoveCharacter: function(e){
            var x = +dojo.attr(e.currentTarget, 'data-x'),
                y = +dojo.attr(e.currentTarget, 'data-y');

            if (this.checkAction( "moveCharacter" )){
                this.ajaxcall( '/mrjack/mrjack/moveCharacter.html', { 
                    lock: true, 
                    x: x,
                    y: y 
                }, this, function(){});
            }
        },

        onCharacterCapture: function(e){
            var x = +dojo.attr(e.currentTarget, 'data-x'),
                y = +dojo.attr(e.currentTarget, 'data-y');

            this.confirmationDialog(
                _('Are you sure to want to attempt a capture?'),
                function() {
                    eventHandles.detachGroup("moveCharacter")
                    if (this.checkAction( "characterCapture" )){
                        this.ajaxcall( '/mrjack/mrjack/characterCapture.html', { 
                            lock: true, 
                            x: x,
                            y: y 
                        }, this, function(){});
                    }
                }.bind(this) ); 
        },

        onCharacterExit: function(e){
            var x = +dojo.attr(e.currentTarget, 'data-x'),
                y = +dojo.attr(e.currentTarget, 'data-y');

            if (this.checkAction( "characterExit" )){
                this.ajaxcall( '/mrjack/mrjack/characterExit.html', { 
                    lock: true, 
                    x: x,
                    y: y 
                }, this, function(){});
            }
        },

        onDecideAbility: function() {
            if (this.checkAction( "decideAbility" )){
                this.ajaxcall( '/mrjack/mrjack/decideAbility.html', { 
                    lock: true
                }, this, function(){});
            }
        },
        
        onCancelAbility: function() {
            if (this.checkAction( "cancelAbility" )){
                this.ajaxcall( '/mrjack/mrjack/cancelAbility.html', { 
                    lock: true
                }, this, function(){});
            }
        },
        
        onCancelCharacter: function(){
            if (this.checkAction( "cancelCharacter" )){
                this.ajaxcall( '/mrjack/mrjack/cancelCharacter.html', { 
                    lock: true
                }, this, function(){});
            }
        },
        
        onCancelSelection: function(){
            if (this.checkAction( "cancelSelection" )){
                this.ajaxcall( '/mrjack/mrjack/cancelSelection.html', { 
                    lock: true
                }, this, function(){});
            }
        },

        onSelectSourceCordon: function(e) {
            var position = dojo.attr(e.currentTarget, 'data-position');

            if (this.checkAction( "selectSourceCordon" )){
                this.ajaxcall( '/mrjack/mrjack/selectSourceCordon.html', { 
                    lock: true, 
                    position: position
                }, this, function(){});
            }
        },
        onSelectCordonDestination: function(e) {
            var position = dojo.attr(e.currentTarget, 'data-position');

            if (this.checkAction( "selectCordonDestination" )){
                this.ajaxcall( '/mrjack/mrjack/selectCordonDestination.html', { 
                    lock: true, 
                    position: position
                }, this, function(){});
            }
        },
        onSelectSourceManhole: function(e){
            var x = +dojo.attr(e.currentTarget, 'data-x'),
                y = +dojo.attr(e.currentTarget, 'data-y');

            if (this.checkAction( "selectSourceManhole" )){
                this.ajaxcall( '/mrjack/mrjack/selectSourceManhole.html', { 
                    lock: true, 
                    x: x,
                    y: y 
                }, this, function(){});
            }
        },
        onSelectManholeDestination: function(e){
            var x = +dojo.attr(e.currentTarget, 'data-x'),
                y = +dojo.attr(e.currentTarget, 'data-y');

            if (this.checkAction( "selectManholeDestination" )){
                this.ajaxcall( '/mrjack/mrjack/selectManholeDestination.html', { 
                    lock: true, 
                    x: x,
                    y: y 
                }, this, function(){});
            }
        },
        onSelectSourceGaslight: function(e){
            var x = +dojo.attr(e.currentTarget, 'data-x'),
                y = +dojo.attr(e.currentTarget, 'data-y');

            if (this.checkAction( "selectSourceGaslight" )){
                this.ajaxcall( '/mrjack/mrjack/selectSourceGaslight.html', { 
                    lock: true, 
                    x: x,
                    y: y 
                }, this, function(){});
            }
        },
        onSelectGaslightDestination: function(e){
            var x = +dojo.attr(e.currentTarget, 'data-x'),
                y = +dojo.attr(e.currentTarget, 'data-y');

            if (this.checkAction( "selectGaslightDestination" )){
                this.ajaxcall( '/mrjack/mrjack/selectGaslightDestination.html', { 
                    lock: true, 
                    x: x,
                    y: y 
                }, this, function(){});
            }
        },
        onSwitchPlace: function(e){
            var x = +dojo.attr(e.currentTarget, 'data-x'),
                y = +dojo.attr(e.currentTarget, 'data-y');

            if (this.checkAction( "switchPlace" )){
                this.ajaxcall( '/mrjack/mrjack/switchPlace.html', { 
                    lock: true, 
                    x: x,
                    y: y 
                }, this, function(){});
            }
        },
        onPickCharacterToMoveCloser: function(e){
            var x = +dojo.attr(e.currentTarget, 'data-x'),
                y = +dojo.attr(e.currentTarget, 'data-y');

            if (this.checkAction( "pickCharacterToMoveCloser" )){
                this.ajaxcall( '/mrjack/mrjack/pickCharacterToMoveCloser.html', { 
                    lock: true, 
                    x: x,
                    y: y 
                }, this, function(){});
            }
        },
        onMoveCloser: function(e){
            var x = +dojo.attr(e.currentTarget, 'data-x'),
                y = +dojo.attr(e.currentTarget, 'data-y');

            if (this.checkAction( "moveCloser" )){
                this.ajaxcall( '/mrjack/mrjack/moveCloser.html', { 
                    lock: true, 
                    x: x,
                    y: y 
                }, this, function(){});
            }
        },
        onRotateWatson: function(e){
            var x = +dojo.attr(e.currentTarget, 'data-x'),
                y = +dojo.attr(e.currentTarget, 'data-y');

            if (this.checkAction( "rotateWatson" )){
                this.ajaxcall( '/mrjack/mrjack/rotateWatson.html', { 
                    lock: true, 
                    x: x,
                    y: y 
                }, this, function(){});
            }
        },
        
        ///////////////////////////////////////////////////
        //// Reaction to cometD notifications

        /*
            setupNotifications:
            
            In this method, you associate each of your game notifications with your local method to handle it.
            
            Note: game notification names correspond to "notifyAllPlayers" and "notifyPlayer" calls in
                  your mrjack.game.php file.
        
        */
        setupNotifications: function()
        {
            dojo.subscribe( 'newRound', this, "notif_newRound" );
            dojo.subscribe( 'characterCardSelected', this, "notif_characterCardSelected" );
            dojo.subscribe( 'cardDiscarded', this, "notif_cardDiscarded" );
            dojo.subscribe( 'tokenMoved', this, "notif_tokenMoved" );
            this.notifqueue.setSynchronous( 'tokenMoved', 2500 );
            dojo.subscribe( 'mrJackVisibility', this, "notif_mrJackVisibility" );
            this.notifqueue.setSynchronous( 'mrJackVisibility', 3000 );
            dojo.subscribe( 'updateCharacterInnocence', this, "notif_updateCharacterInnocence" );
            this.notifqueue.setSynchronous( 'updateCharacterInnocence', 1500 );
            dojo.subscribe( 'gaslightTurnOff', this, "notif_gaslightTurnOff" );
            dojo.subscribe( 'setScore', this, "notif_setScore" );
            dojo.subscribe( 'gameEnd', this, "notif_gameEnd" );
            this.notifqueue.setSynchronous( 'gameEnd', 2500 );
            dojo.subscribe( 'gameEndDialog', this, "notif_gameEndDialog" );
            dojo.subscribe( 'cordonMoved', this, "notif_cordonMoved" );
            this.notifqueue.setSynchronous( 'cordonMoved', 1000 );
            dojo.subscribe( 'dealAlibiCard', this, "notif_dealAlibiCard" );
            dojo.subscribe( 'dealAlibiCardPrivate', this, "notif_dealAlibiCardPrivate" );
            dojo.subscribe( 'switchTokens', this, "notif_switchTokens" );
            this.notifqueue.setSynchronous( 'switchTokens', 1500 );
            dojo.subscribe( 'rotateWatson', this, "notif_rotateWatson" );
        },  
        
        notif_newRound: function(notif){
            console.log("notif_newRound");
            this.gameBoard.setRound(notif.args.round);
            this.gameBoard.setRemainingCharacterCardsNumber(notif.args.remainingCharacterCardsNumber);
            
            this.gameBoard.setCardDisplay(Object.values(notif.args.characterCards));

        },

        notif_characterCardSelected: function(notif){
            console.log("notif_characterCardSelected");
            this.gameBoard.selectCharacter(notif.args.cardType);
        },
        notif_cardDiscarded: function(notif){
            console.log("notif_cardDiscarded");
            this.gameBoard.useCardInDisplay(notif.args.type);
            this.gameBoard.deselectAllCards();
        },
        
        notif_tokenMoved: function(notif){
            console.log("notif_tokenMoved");
            this.gameBoard.moveToken(notif.args.type, notif.args.id, notif.args.target, notif.args.lights);            
        },
        
        notif_switchTokens: function(notif){
            console.log("notif_switchTokens");
            var first = notif.args.first,
                second = notif.args.second;
            this.gameBoard.switchTokens(first, second);
            !!notif.args.lights && this.gameBoard.setLights(notif.args.lights);
        },

        notif_mrJackVisibility: function(notif){
            console.log("notif_mrJackVisibility");
            this.gameBoard.setWitnessVisibility(notif.args.isMrJackVisible, true);
        },

        notif_updateCharacterInnocence: function(notif){
            console.log("notif_updateCharacterInnocence");
            this.gameBoard.setTokens(JSON.parse(notif.args.tokens), true);
        },

        notif_gaslightTurnOff: function(notif){
            console.log("notif_gaslightTurnOff");
            this.gameBoard.removeToken("gaslight", notif.args.id, true);
            !!notif.args.lights && this.gameBoard.setLights(notif.args.lights);
        },

        notif_setScore: function(notif){
            console.log("notif_setScore");
            this.playerInfoBoxes[notif.args.playerId].increaseScore(notif.args.score)
        },
        
        notif_gameEnd: function(notif){
            console.log("notif_gameEnd");
            var x = 7.21 * notif.args.x -42.93,
                y = 10.5 * notif.args.y - 5.25 * notif.args.x -26.75;

            x = Math.sign(x) * Math.min(Math.abs(x), 33.33);
            y = Math.sign(y) * Math.min(Math.abs(y), 33.33);
            
            dojo.byId("responsive-board").style.overflow = "hidden";
            dojo.byId("board").style.transform = "scale(3) translateX("+x+"%) translateY("+y+"%)";
            //this.gameEndDialog(notif.args.message);
        },
        
        notif_gameEndDialog: function(notif){
            console.log("notif_gameEndDialog");
            this.gameEndDialog(notif.args.message);
        },

        notif_cordonMoved: function(notif){
            console.log("notif_cordonMoved");
            this.gameBoard.movePoliceCordon(notif.args.from, notif.args.to, true);
        },
        notif_dealAlibiCard: function(notif){
            console.log("notif_dealAlibiCard");
            if (notif.args.player_id != this.player_id){
                this.playerInfoBoxes[notif.args.player_id].addAlibiCardBack();
            }
            this.gameBoard.setRemainingAlibiCardsNumber(notif.args.newAlibiCardsCount);
        },
        notif_dealAlibiCardPrivate: function(notif){
            console.log("notif_dealAlibiCardPrivate");
            this.playerInfoBoxes[this.player_id].addAlibiCard(notif.args.type);
        },
        notif_rotateWatson: function(notif){
            console.log("notif_rotateWatson");
            
            var watsonLight = notif.args.lights.find(function(light){
                return light.type == "watson";
            });

            !!watsonLight && this.gameBoard.updateWatsonLight(watsonLight);
        },
    });

    var PlayerInfoBox = function(player, game){
        var playerId = player.id;
        var node = $('player_board_' + playerId);
        
        dojo.place( game.format_block('jstpl_player_board', player ), node );
        var playerHand = game.createCardStock("private-cards-" + playerId, 0, 8, false, function(node, type){
            var message = _("This character cannot be Mr. Jack");
            if (type !== "back"){
                message = game.format_string_recursive(_("${character} cannot be Mr. Jack"), {
                    character: game.getCharacterMeta(type).name
                });
            }
            game.addTooltip(node.id, message, "")
        });
        
        return {
            setPlayerRole: function(role, character){
                dojo.byId("player-role-" + playerId).innerHTML = role;
                if (!!character){
                    var mrJackCharacterId = "mrjack-character-" + playerId;
                    dojo.addClass(dojo.byId(mrJackCharacterId), "visible " + character)
                    game.addCharacterTooltip(mrJackCharacterId, character, _("This character is Mr. Jack!"));
                }
            }, 
            addAlibiCard: function(type){
                var sherlockPawn = dojo.query(".pawn.sh")[0];
                playerHand.addToStock(type, sherlockPawn);
            },
            addAlibiCardBack: function(){
                this.addAlibiCard("back");
            },
            addAlibiCards: function(alibiCardHand) {
                alibiCardHand.map(function(card){
                    return card.type;
                }).forEach(this.addAlibiCard.bind(this));
            },
            addAlibiCardBacks: function(alibiCardCount){
                for (var i = 0; i < alibiCardCount; i++) {
                    this.addAlibiCardBack();
                }
            },
            increaseScore: function(value){
                game.scoreCtrl[playerId].incValue(value);
            }
        }
    }
    
    var GameBoard = function(game){
        var getTokenElementId = function(token){
            if (token.type == "pawn"){
                return "pawn-" + token.x + "-" + token.y;
            }
            return "token-" + token.x + "-" + token.y;
        }

        var getTokenElement = function(type, id){
            var baseElement = ".token";
            if (type == "pawn"){
                baseElement = ".pawn";
            }

            if (!isNaN(id)){
                id = type + "-" + id;
            }

            return dojo.query([baseElement, type, id].join("."))[0];
        }

        return {
            activateFieldNode: function(node, callback, additionalClass){
                dojo.addClass(node, "highlight");
                if (additionalClass){
                    dojo.addClass(node, additionalClass);
                }
    
                var interactionNode = dojo.query(".field-interaction", node);
                eventHandles.attachQuery(interactionNode, "activateField", "click", callback);
            },
            activateField: function(field, callback, additionalClass){
                var node = dojo.byId("field-" + field.x + "-" + field.y);
                this.activateFieldNode(node, callback, additionalClass)
            },
            activateFields: function(fields, callback, additionalClass){
                for (let index = 0; index < fields.length; index++) {
                    this.activateField(fields[index], callback, additionalClass);
                }
            },
            activateNonActivatedFields: function(callback, additionalClass){
                dojo.query(".field::not(.highlight)").forEach(function(node){
                    this.activateFieldNode(node, callback, additionalClass);
                }.bind(this));
            },
            deactivateAllFields: function(){
                [
                    "highlight", 
                    "move", 
                    "capture", 
                    "from-n", 
                    "from-s", 
                    "from-nw", 
                    "from-ne", 
                    "from-sw", 
                    "from-se", 
                    "from-manhole",
                    "into-manhole",
                    "select",
                    "available",
                    "rotate",
                    "rotate-s",
                    "rotate-n",
                    "rotate-se",
                    "rotate-sw",
                    "rotate-ne",
                    "rotate-nw",
                    "cancel"
                ].forEach(function(className){
                    dojo.query(".field." + className).removeClass(className);
                })
                eventHandles.detachGroup("activateField");
            },
            activateMoves: function(distances, callback){
                for (let index = 0; index < distances.length; index++) {
                    if (distances[index].outFromManhole){
                        this.activateField(distances[index].field, callback, "move manhole");
                    } else if (distances[index].intoManhole){
                        this.activateField(distances[index].field, callback, "move manhole");
                    } else {
                        this.activateField(distances[index].field, callback, "move from-" + distances[index].from);
                    }
                }
            },
            activateCaptures: function(distances, callback){
                for (let index = 0; index < distances.length; index++) {
                    this.activateField(distances[index].field, callback, "move capture");
                }
            },
            activateEscapes: function(distances, callback){
                for (let index = 0; index < distances.length; index++) {
                    this.activateField(distances[index].field, callback, "move escape");
                }
            },
            animateTokenFlip: function(token){
                var duration = 1500;
                var tokenElement = getTokenElement(token.type, token.id);
                var movingObject = dojo.clone(tokenElement);

                //cannot remove it immediately because onEnterState
                //happens before the animation is over and doesn't
                //change unexisting token
                token.type == "pawn" && dojo.query(".pawn-visual", tokenElement).style("display", "none"); //to hide pawn temporarily
                
                var currentField = tokenElement.closest(".field");
                var obj = dojo.place(movingObject, currentField.id);
                dojo.style(obj, 'position', 'absolute');
                dojo.style(obj, 'left', '0px');
                dojo.style(obj, 'top', '0px');
                dojo.style(obj, 'z-index', '947');
                
                game.placeOnObject(obj, tokenElement.id);

                var computedStyle = domStyle.getComputedStyle(dojo.query(".pawn-visual", obj)[0])
                var originalPawnStyle = {
                    width: computedStyle.width,
                    height: computedStyle.height,
                    "margin-top": computedStyle["margin-top"],
                    "margin-left": computedStyle["margin-left"]
                };

                var animations = [];

                animations.push(baseFx.animateProperty({
                    node:dojo.query(".pawn-visual", obj)[0],
                    properties: {
                        width: 1.3*originalPawnStyle.width.replace("px", ""),
                        height: 1.3*originalPawnStyle.height.replace("px", ""),
                        "margin-top": 1.3*originalPawnStyle["margin-top"].replace("px", ""),
                        "margin-left": 1.3*originalPawnStyle["margin-left"].replace("px", "")
                    },
                    duration: duration/3,
                }))
                
                animations.push(baseFx.animateProperty({
                    node:dojo.query(".pawn-visual", obj)[0],
                    properties: {},
                    onBegin: function(){
                        dojo.query(".pawn-visual", obj).addClass("animate-flip");
                        dojo.addClass(obj, "flipped")
                    },
                    duration: duration/3,
                }))

                animations.push(baseFx.animateProperty({
                    node:dojo.query(".pawn-visual", obj)[0],
                    properties: {
                        width: originalPawnStyle.width.replace("px", ""),
                        height: originalPawnStyle.height.replace("px", ""),
                        "margin-top": originalPawnStyle["margin-top"].replace("px", ""),
                        "margin-left": originalPawnStyle["margin-left"].replace("px", "")
                    },
                    duration: duration/3,
                }))
                        
                var anim = fx.chain(animations);
                dojo.connect(anim, 'onEnd', function () {
                    token.type == "pawn" && dojo.query(".pawn-visual", tokenElement).style("display", "block");
                    dojo.addClass(tokenElement, "flipped");
                    dojo.destroy(obj);
                });

                anim.play();
                return anim;
            },
            setToken: function(token, addFlipAnimation){
                var elementId = getTokenElementId(token);
        
                if (token.type != "pawn"){
                    dojo.addClass(elementId, token.type);
                }
                
                var id = token.id;
                if (!isNaN(id)){
                    id = token.type + "-" + id;
                }
    
                dojo.addClass(elementId, id);
                
                if (token.isSelected){
                    dojo.addClass(elementId, 'selected');
                }
    
                if (token.type == "pawn"){
                    var fieldId = dojo.attr(dojo.query("#"+elementId).closest(".field")[0], "id");      
                    game.addCharacterTooltipWithInnocenceStatus(fieldId, token.id, token.isFlipped);
                }
                
                if (!addFlipAnimation && token.isFlipped){
                    dojo.addClass(elementId, 'flipped');
                }
                if (!!addFlipAnimation && token.isFlipped){
                    if (!dojo.hasClass(elementId, "flipped")){
                        this.animateTokenFlip(token);
                    }
                }
            },
            setTokens: function(tokens, animateTokenFlip){
                tokens.forEach(function(token){
                    this.setToken(token, animateTokenFlip);
                }.bind(this))
            },
            animateTokenRemoval: function(type, id){
                var tokenElement = getTokenElement(type, id);
                dojo.addClass(tokenElement, "removing");
                setTimeout(function(){
                    this.removeTokenElement(tokenElement, type, id);
                    dojo.removeClass(tokenElement, "removing");
                }.bind(this), 3000);
            },
            removeToken: function(type, id, animateRemoval){
                var tokenElement = getTokenElement(type, id);

                !animateRemoval && this.removeTokenElement(tokenElement, type, id);
                
                !!animateRemoval && this.animateTokenRemoval(type, id);
            },
            removeTokenElement: function(tokenElement, type, id){
                var classesToRemove = [
                    "flipped",
                    "selected",
                ];

                if (type != "pawn"){
                    classesToRemove.push(type)
                }
                
                classesToRemove.push(!isNaN(id) ? type + "-" + id : id);
                
                classesToRemove.forEach(function(current){
                    dojo.removeClass(tokenElement, current);
                })

                if (type == "pawn"){
                    var fieldId = dojo.query(tokenElement).closest(".field")[0].id;
                    game.removeTooltip(fieldId);
                }
            },
            animateTokenMove: function (token, parentId, target, onEnd) {
                var tokenElement = getTokenElement(token.type, token.id);
                var currentField = tokenElement.closest(".field");
                var movingObject = dojo.clone(currentField);

                //cannot remove it immediately because onEnterState
                //happens before the animation is over and doesn't
                //change unexisting token
                token.type == "pawn" && dojo.removeClass(tokenElement, token.id); //to hide pawn temporarily
                token.type == "pawn" && dojo.removeClass(tokenElement, "selected"); //to hide pawn temporarily
                token.type != "pawn" && dojo.removeClass(tokenElement, token.type); //to hide token temporarily

                var obj = dojo.place(movingObject, parentId);
                dojo.style(obj, 'position', 'absolute');
                dojo.style(obj, 'left', '0px');
                dojo.style(obj, 'top', '0px');
                dojo.style(obj, 'z-index', '947');
                
                dojo.removeClass(obj, "select");
                dojo.query(".lights", obj).forEach(dojo.destroy);
                dojo.query(".pawn", obj).removeClass("selected");
                token.type == "pawn" && dojo.query(".token", obj).style("display", "none");
                token.type != "pawn" && dojo.query(".pawn", obj).style("display", "none");
                
                game.placeOnObject(obj, currentField.id);

                var steps = [target];
                current = target;
                while (current.fromDistance) {
                    current = current.fromDistance
                    steps.unshift(current)
                }

                var stepDuration = 1000 / steps.length;

                var animations = [];

                var computedStyle = domStyle.getComputedStyle(dojo.query(".pawn-visual", obj)[0])
                var originalPawnStyle = {
                    width: computedStyle.width,
                    height: computedStyle.height,
                    "margin-top": computedStyle["margin-top"],
                    "margin-left": computedStyle["margin-left"]
                };

                var isCapture = false;

                for (let index = 0; index < steps.length; index++) {
                    var step = steps[index];
                    if (step.outFromManhole){
                        var previousFieldId = "field-"+step.fromDistance.field.x+"-"+step.fromDistance.field.y,
                            currentFieldId = "field-"+step.field.x+"-"+step.field.y;

                        var intoEffectElement = dojo.place(dojo.clone(obj), parentId);
                        dojo.destroy(dojo.query(".pawn", intoEffectElement)[0]);
                        game.placeOnObject(intoEffectElement, previousFieldId);

                        var outEffectelement = dojo.place(dojo.clone(intoEffectElement), parentId);
                        game.placeOnObject(outEffectelement, currentFieldId);

                        var intoEffectAnimation = baseFx.fadeOut({
                            node: intoEffectElement,
                            duration: 1000,
                            beforeBegin: function(node){
                                dojo.addClass(node, "highlight move into-manhole");
                            },
                            onEnd: function(node){
                                dojo.destroy(node);
                            }
                        });
    
                        animations.push(baseFx.animateProperty({
                            node:dojo.query(".pawn-visual", obj)[0],
                            properties: {
                                width: 0,
                                height: 0,
                                "margin-top": 0,
                                "margin-left": 0
                            },
                            duration: 500,
                            onEnd: function(){
                                intoEffectAnimation.play();
                            }
                        }))
                                
                        var outEffectAnimation = baseFx.fadeOut({
                            node: outEffectelement,
                            duration: 1000,
                            delay: 500,
                            beforeBegin: function(node){
                                dojo.addClass(node, "highlight move out-of-manhole");
                            },
                            onEnd: function(node){
                                dojo.destroy(node);
                            },
                        });

                        animations.push(baseFx.animateProperty({
                            node: dojo.query(".pawn-visual", obj)[0],
                            properties: {
                                width: +originalPawnStyle.width.replace("px", ""),
                                height: +originalPawnStyle.height.replace("px", ""),
                                "margin-top": +originalPawnStyle["margin-top"].replace("px", ""),
                                "margin-left": +originalPawnStyle["margin-left"].replace("px", "")
                            },
                            duration: 500,
                            onBegin: function(){
                                outEffectAnimation.play();
                            },
                            beforeBegin: function(){
                                game.placeOnObject(obj, currentFieldId);
                            },
                            delay: 500
                        }));
                    } else if (!step.isCapture) {
                        animations.push(game.slideToObject(obj, "field-"+step.field.x+"-"+step.field.y, stepDuration))
                    }

                    if (step.isCapture) {
                        isCapture = true;

                        var offsetX = +originalPawnStyle.width.replace("px", "");
                        var offsetY = +originalPawnStyle.height.replace("px", "");
                        switch (step.from) {
                            case "nw":
                                offsetX *= -0.45;
                                offsetY *= -0.45;
                                break;
                            case "n":
                                offsetX = 0;
                                offsetY *= -0.65;
                                break;
                            case "ne":
                                offsetX *= 0.45;
                                offsetY *= -0.45;
                                break;
                            case "sw":
                                offsetX *= -0.45;
                                offsetY *= 0.45;
                                break;
                            case "s":
                                offsetX = 0;
                                offsetY *= 0.65;
                                break;
                            case "se":
                                offsetX *= 0.45;
                                offsetY *= 0.45;
                                break;
                            default:
                                break;
                        }
                        animations.push(game.slideToObjectPos(obj, "field-"+step.field.x+"-"+step.field.y, offsetX, offsetY, stepDuration))
                    }

                }
                
                var anim = fx.chain(animations);
                dojo.connect(anim, 'onEnd', function () {
                    if (!isCapture){
                        token.type == "pawn" && dojo.addClass(tokenElement, token.id);
                        token.type != "pawn" && dojo.addClass(tokenElement, token.type);
                        dojo.destroy(obj);
                    }

                    if (onEnd){
                        onEnd(isCapture);
                    }
                });

                anim.play();
                return anim;
            },
            moveToken: function(type, id, target, lights){
                this.deactivateAllFields();
                tokenElement = getTokenElement(type, id);
                isSelected = dojo.hasClass(tokenElement, "selected");
                isFlipped = dojo.hasClass(tokenElement, "flipped");
                
                this.animateTokenMove({ type: type, id: id }, "board", target, function(isCapture){
                    if (!isCapture){
                        
                        this.removeTokenElement(tokenElement, type, id);
                        this.setToken({ 
                            type: type, 
                            id: id, 
                            y: target.field.y, 
                            x: target.field.x, 
                            isFlipped: isFlipped, 
                            isSelected: isSelected 
                        });
    
                        lights && this.setLights(lights);
                    }      
                }.bind(this));
            },
            switchTokens: function(first, second){
                this.deactivateAllFields();

                var animationParams = [
                    {
                        source: first,
                        target: second
                    }, 
                    {
                        source: second,
                        target: first
                    } 
                ]

                var animationId = fx.combine(animationParams.map(function(param){
                    var target = {
                        field: param.target
                    }
                    return this.animateTokenMove(param.source, "board", target);
                }.bind(this))).play();

                dojo.connect(animationId, 'onEnd', function(){
                    var f = first, s = second, temp = {};
        
                    temp.x = f.x;
                    temp.y = f.y;
                    
                    f.x = s.x;
                    f.y = s.y;
                    
                    s.x = temp.x;
                    s.y = temp.y;
        
                    //cannot do moveToken as it has race condition on second token moved
                    this.removeToken(f.type, f.id);
                    this.removeToken(s.type, s.id);
                    this.setToken(f);
                    this.setToken(s);            
                }.bind(this));
            },
            selectCharacterPawn: function(id){
                dojo.query(".pawn."+id).addClass("selected");
            },
            deselectAllPawns: function(){
                dojo.query(".pawn.selected").removeClass("selected");
            },    
            setPoliceCordonToPosition: function(position){
                var id = "police-cordon-" + position;
                dojo.addClass(id, 'active');
            },
            setPoliceCordon: function(policeCordon){
                this.setPoliceCordonToPosition(policeCordon.position);
            },
            setPoliceCordons: function(policeCordons){
                policeCordons.forEach(this.setPoliceCordon.bind(this));
            },
            removePoliceCordonFromPosition: function(position){
                var id = "police-cordon-" + position;
                
                dojo.removeClass(id, 'active');
            },
            animatePoliceCordonMove: function(from, to){
                var originId = "police-cordon-" + from,
                    originElement = dojo.byId(originId),
                    destinationId = "police-cordon-" + to,
                    destinationElement = dojo.byId(destinationId);

                // it is not possible to start transition on an element
                // on which property is changed immediately after 
                // its placement in the DOM, so I animate on (and destroy)
                // original element and put clone in its place
                var clone = dojo.clone(originElement);
                dojo.removeClass(clone, "active");
                dojo.place(clone, "board");
                
                dojo.style(originElement, "background-image", "none");
                originElement.id = destinationId;
                originElement.addEventListener("transitionend", function(){
                    dojo.addClass(destinationElement, "active");
                    dojo.destroy(originElement);
                })
            },
            movePoliceCordon: function(from, to, isAnimated){
                if (!!isAnimated) {
                    this.animatePoliceCordonMove(from, to);
                } else {
                    this.setPoliceCordonToPosition(to);
                    this.removePoliceCordonFromPosition(from);            
                }
            },
            activatePoliceCordonOnPosition: function(position, callback){
                var node = dojo.byId("police-cordon-" + position);
                    
                dojo.addClass(node, "highlight");
                eventHandles.attach(node, "activatePoliceCordon", "click", callback);
            },
            activatePoliceCordonsOnPositions: function(positions, callback){
                for (let index = 0; index < positions.length; index++) {
                    this.activatePoliceCordonOnPosition(positions[index], callback);
                }
            },
            activatePoliceCordons: function(policeCordons, callback){
                for (let index = 0; index < policeCordons.length; index++) {
                    this.activatePoliceCordonOnPosition(policeCordons[index].position, callback);
                }
            },
            deactivateAllPoliceCordons: function(){
                dojo.query(".police-cordon.highlight").removeClass('highlight');
                eventHandles.detachGroup("activatePoliceCordon");
            },
            setCardDisplay: function(cards){
                cards.forEach(function(card, i){
                    cardElementId = "character-card-" + i;

                    //cleanup
                    dojo.removeClass(cardElementId, "used");
                    dojo.removeClass(cardElementId, "selected");
                    var previousCardType = dojo.attr(cardElementId, "cardType");
                    dojo.removeClass(cardElementId, previousCardType);
                    game.removeTooltip(cardElementId);

                    //setup
                    dojo.addClass(cardElementId, card.type);
                    dojo.attr(cardElementId, "cardType", card.type);
                    
                    if (card.location_arg == 1){
                        this.selectCharacter(card.type);
                    }
                    if (card.location_arg == 2){
                        this.useCardInDisplay(card.type);
                    }
                    
                    game.addCharacterTooltipWithInnocenceStatus(cardElementId, card.type, card.isInnocent)
                }.bind(this))
            },
            selectCardIndisplay: function(cardType){
                dojo.query(".character-card." + cardType).addClass("selected");
                dojo.addClass(dojo.byId("card-display"), "selected"); 
                
                var abilityMarkup = game.getCharacterAbilityMarkup(cardType);
                dojo.byId("character-ability").innerHTML = abilityMarkup;
            },
            deselectAllCards: function(){
                dojo.query(".character-card.selected").removeClass("selected");
                dojo.removeClass(dojo.byId("card-display"), "selected");                
            },
            useCardInDisplay: function(cardType){
                dojo.query(".character-card." + cardType).addClass("used");
            },
            activateCardDisplay: function(eventHandler){
                dojo.addClass(dojo.byId("card-display"), "active");
                eventHandles.attachQuery(".character-card::not(.used)", "activateCardDisplay", 'onclick', eventHandler);
            },
            deactivateCardDisplay: function(){
                dojo.removeClass(dojo.byId("card-display"), "active");
                eventHandles.detachGroup("activateCardDisplay");
            },
            activateAbilityBoxInteractions: function(cancelHandler, abilityUseHandler){
                dojo.query("#character-ability .character-ability-interactions").addClass("active");

                eventHandles.detachGroup("activateAbilityBoxInteractions");
                eventHandles.attachQuery("#character-ability .character-ability-interactions .character-ability-cancel-button", "activateAbilityBoxInteractions", "click", cancelHandler)
                
                if (abilityUseHandler){
                    eventHandles.attachQuery("#character-ability .character-ability-interactions .character-ability-use-button", "activateAbilityBoxInteractions", "click", abilityUseHandler)
                    dojo.query("#character-ability .character-ability-interactions .character-ability-use-button").addClass("active");
                } else {
                    dojo.query("#character-ability .character-ability-interactions .character-ability-use-button").removeClass("active");
                }
            },
            deactivateAbilityBoxInteractions: function(){
                dojo.query("#character-ability .character-ability-interactions").removeClass("active");
                eventHandles.detachGroup("activateAbilityBoxInteractions");
            },
            selectCharacter: function(cardType) {
                this.selectCardIndisplay(cardType);
                this.selectCharacterPawn(cardType);
            },
            setRemainingAlibiCardsNumber: function(number){
                dojo.byId("alibi-card-number").innerHTML = number;
            },
            setRemainingCharacterCardsNumber: function(number){
                dojo.byId("character-card-number").innerHTML = number;            
            },
            animateWitnessVisibility: function(isVisible){
                var duration = 3000
                var witnessCardElement = dojo.byId("witness-card");
                var movingObject = dojo.clone(witnessCardElement);

                //cannot remove it immediately because onEnterState
                //happens before the animation is over and doesn't
                //change unexisting token
                
                var parentElement = witnessCardElement.closest(".witness-card-wrapper");
                var obj = dojo.place(movingObject, parentElement);
                dojo.style(obj, 'position', 'absolute');
                dojo.style(obj, 'left', '0px');
                dojo.style(obj, 'top', '0px');
                dojo.style(obj, 'z-index', '947');
                
                obj.removeAttribute("id");
                
                game.placeOnObject(obj, witnessCardElement.id);
                dojo.style("witness-card", "visibility", "hidden"); //to hide pawn temporarily

                var animations = [];

                var slideAnimation = game.slideToObject(obj, "board", duration/3, 0);
                dojo.connect(slideAnimation, 'onBegin', function (node) {
                    dojo.addClass(obj, "enlarge");   
                });
                animations.push(slideAnimation);

                animations.push(baseFx.animateProperty({
                    node: obj,
                    properties: {},
                    duration: duration*3/6,
                    onBegin: function(){
                        isVisible && dojo.hasClass(obj, "invisible") && dojo.removeClass(obj, "invisible");
                        !isVisible && !dojo.hasClass(obj, "innvisible") && dojo.addClass(obj, "invisible");
                        isVisible && dojo.removeClass(dojo.byId("witness-card"), "invisible");
                        !isVisible && dojo.addClass(dojo.byId("witness-card"), "invisible");
                    }
                }));

                var slideBackAnimation = game.slideToObject(obj, "witness-card", duration/6, 0);
                dojo.connect(slideBackAnimation, 'onBegin', function (node) {
                    dojo.addClass(obj, "fast-reduce");
                    dojo.removeClass(obj, "enlarge");
                });
                animations.push(slideBackAnimation);

                var anim = fx.chain(animations);
                dojo.connect(anim, 'onEnd', function () {
                    dojo.style("witness-card", "visibility", "visible");
                    dojo.destroy(obj);
                });

                anim.play();
                return anim;
            },
            setWitnessVisibility: function(isVisible, animateVisibility){
                if (!animateVisibility){
                    isVisible && dojo.removeClass(dojo.byId("witness-card"), "invisible");
                    !isVisible && dojo.addClass(dojo.byId("witness-card"), "invisible");
                }

                game.removeTooltip("witness-card");
                game.addTooltip("witness-card", isVisible ? _("Mr. Jack was visible and can not escape this round") : _("Mr. Jack was invisible and can escape this round"), "")

                !!animateVisibility && this.animateWitnessVisibility(isVisible);
            },
            setRoundTooltip: function(round){
                game.removeTooltip("round-number");
    
                var endRoundSteps = 
                    round != 8 ? 
                        [_("Call for witness (witness card changes and innocent character flips)")] 
                        : [ _("Game ends with Mr. Jack victory")];

                round <= 4 && endRoundSteps.push(_("Gaslight ${round} turns off").replace("${round}", round));

                var detectiveLabel = _("Detective");
                var mrJackLabel = _("Mr. Jack");

                var turnOrder = round % 2 == 1 ? 
                    [ detectiveLabel, mrJackLabel, mrJackLabel, detectiveLabel] :
                    [ mrJackLabel, detectiveLabel, detectiveLabel, mrJackLabel];

                var html = game.format_block('jstpl_roundTooltip', {
                    round: round,
                    turnOrder: turnOrder.join(" &#x21e8; "),
                    roundEnd: "<ul><li>"+ endRoundSteps.join("</li><li>") + "</li></ul>"
                });
                game.addTooltipHtml(dojo.byId("round-number"), html);

                //dojo.place(html, dojo.byId("play-area"))
            },
            setRound: function(round){
                dojo.addClass(dojo.byId("round-number"), "round-" + round);
                this.setRoundTooltip(round)
            },
            setLight: function(light){
                var lightsElement = dojo.byId("lights-" + light.field.x + "-" + light.field.y);
                var html = game.format_block('jstpl_light', light);

                var element = dojo.place(html, lightsElement);
                
                if (light.rotation){
                    dojo.addClass(element, "to-" + light.rotation);
                }
                if (light.length != undefined){
                    dojo.addClass(element, "length-" + light.length);
                }
            },
            clearAllLights: function(){
                dojo.query(".lights").forEach(dojo.destroy);
            },
            setLights: function(lights){
                this.clearAllLights();
                lights.forEach(this.setLight);
            },
            updateWatsonLight: function(light){
                var query = dojo.query(".lights-watson");
                query.removeAttr("class");
                query.addClass("lights lights-watson");

                if (light.rotation){
                    query.addClass("to-" + light.rotation);
                }
                if (light.length != undefined){
                    query.addClass("length-" + light.length);
                }
            }
        }
    }
   
    var eventHandles = {
        handles: {},
        attach: function(node, group, event, handle) {
            if (!this.handles[group]) {
                this.handles[group] = [];
            }

            this.handles[group].push(dojo.connect(node, event, handle));
        },
        attachQuery: function(query, group, event, handle) {
            dojo.query(query).forEach(
                function(node) {
                this.attach(node, group, event, handle);
                }.bind(this)
            );
        },
        detachAll: function(){
            for (var group in this.handles) {
                if (this.handles.hasOwnProperty(group)) {
                    this.detachGroup(group);                    
                }
            }
        },
        detachGroup: function(group) {
            if (!this.handles[group]) {
                return;
            }

            this.handles[group].forEach(function(handle) {
                handle.remove();
            });
            this.handles[group] = [];
        }
    }

    return gamegui;
});
