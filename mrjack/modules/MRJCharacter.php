<?php

abstract class MRJCharacter implements \JsonSerializable
{
    const ID_SHERLOCK_HOLMES = "sh";
    const ID_JOHN_H_WATSON = "jhw";
    const ID_JOHN_SMITH = "js";
    const ID_INSPECTOR_LESTRADE = "il";
    const ID_MISS_STEALTHY = "ms";
    const ID_SERGEANT_GOODLY = "sg";
    const ID_SIR_WILLIAM_GULL = "swg";
    const ID_JEREMY_BERT = "jb";

    public function postMoveAction(MrJack $game){
        $nextTransition = $this->getAbilityTransition();
        if ($this->hasBeforeOrAfterAbility()){
            if ($game->checkIfAbilityDone() || $this->abilityReplacesMove()){
                $nextTransition = "endTurn";
            }
        }
        $game->gamestate->nextState($nextTransition);
    }

    public function postSelectAction(MrJack $game){
        $nextTransition = "moveCharacter";
        if ($this->hasBeforeOrAfterAbility()){
            $nextTransition = "moveCharacterWithAbility";
        }

        $game->gamestate->nextState($nextTransition);
    }
    
    abstract public function getId() : string;
    abstract protected function hasBeforeOrAfterAbility() : bool;
    abstract public function getAbilityTransition();
    abstract public static function getName();

    protected function abilityReplacesMove() : bool
    {
        return false;
    }

    public function getCanMoveThroughObstacles() : bool
    {
        return false;
    }

    public function getMovementPoints(){
        return 3;
    }

    public static function getCharacter(string $characterId) : ?MRJCharacter
    {
        switch ($characterId) {
            case self::ID_SHERLOCK_HOLMES:
                return new MRJSherlockHolmes();
            case self::ID_JOHN_H_WATSON:
                return new MRJJohnHWatson();
            case self::ID_JOHN_SMITH:
                return new MRJJohnSmith();
            case self::ID_INSPECTOR_LESTRADE:
                return new MRJInspectorLestrade();
            case self::ID_MISS_STEALTHY:
                return new MRJMissStealthy();
            case self::ID_SERGEANT_GOODLY:
                return new MRJSergeantGoodley();
            case self::ID_SIR_WILLIAM_GULL:
                return new MRJSirWilliamGull();
            case self::ID_JEREMY_BERT:
                return new MRJJeremyBert();
            default:
                return null;
        }
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}