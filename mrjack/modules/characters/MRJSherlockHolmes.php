<?php

class MRJSherlockHolmes extends MRJCharacter{
    public function getId() : string { return MRJCharacter::ID_SHERLOCK_HOLMES; } 

    public function getabilityTransition() : string
    {
        return "dealAlibiCard";
    }

    protected function hasBeforeOrAfterAbility() : bool
    {
        return false;
    }

    public static function getName() {
        return "Sherlock Holmes";
    }
}