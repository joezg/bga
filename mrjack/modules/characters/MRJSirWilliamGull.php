<?php

class MRJSirWilliamGull extends MRJCharacter{
    public function getId() : string { return MRJCharacter::ID_SIR_WILLIAM_GULL; } 
    
    protected function hasBeforeOrAfterAbility() : bool
    {
        return true;
    }

    public function getabilityTransition() : string
    {
        return "switchPlace";
    }

    public function abilityReplacesMove() : bool {
        return true;
    }

    public static function getName() {
        return "Sir William Gull";
    }
}