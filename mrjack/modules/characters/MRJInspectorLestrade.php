<?php

class MRJInspectorLestrade extends MRJCharacter{
    public function getId() : string { return MRJCharacter::ID_INSPECTOR_LESTRADE; }

    protected function hasBeforeOrAfterAbility() : bool
    {
        return true;
    }

    public function getabilityTransition() : string
    {
        return "moveCordon";
    }

    public static function getName() {
        return "Inspector Lestrade";
    }
}