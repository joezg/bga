<?php

class MRJJeremyBert extends MRJCharacter{
    public function getId() : string { return MRJCharacter::ID_JEREMY_BERT; } 
    
    protected function hasBeforeOrAfterAbility() : bool
    {
        return true;
    }

    public function getabilityTransition() : string
    {
        return "moveManhole";
    }
    public static function getName() {
        return "Jeremy Bert";
    }
}