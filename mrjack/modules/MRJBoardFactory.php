<?php

class MRJBoardFactory
{
    public static function getCards() : array{
        return [
            [
                "type" => MRJCharacter::ID_INSPECTOR_LESTRADE,
                "type_arg" => null,
                "nbr" => 1
            ],
            [
                "type" => MRJCharacter::ID_JEREMY_BERT,
                "type_arg" => null,
                "nbr" => 1
            ],
            [
                "type" => MRJCharacter::ID_JOHN_H_WATSON,
                "type_arg" => null,
                "nbr" => 1
            ],
            [
                "type" => MRJCharacter::ID_JOHN_SMITH,
                "type_arg" => null,
                "nbr" => 1
            ],
            [
                "type" => MRJCharacter::ID_MISS_STEALTHY,
                "type_arg" => null,
                "nbr" => 1
            ],
            [
                "type" => MRJCharacter::ID_SERGEANT_GOODLY,
                "type_arg" => null,
                "nbr" => 1
            ],
            [
                "type" => MRJCharacter::ID_SHERLOCK_HOLMES,
                "type_arg" => null,
                "nbr" => 1
            ],
            [
                "type" => MRJCharacter::ID_SIR_WILLIAM_GULL,
                "type_arg" => null,
                "nbr" => 1
            ],
        ];
    }

    public static function getInitialTokenPositions() : array
    {
        return [
            [
                "x" => 2,
                "y" => 1,
                "id" => 4,
                "type" => MRJToken::TYPE_GASLIGHT,
                "is_flipped" => 0,
                "is_being_moved" => 0,
                "rotation" => null
            ],
            [
                "x" => 1,
                "y" => 5,
                "id" => 2,
                "type" => MRJToken::TYPE_GASLIGHT,
                "is_flipped" => 0,
                "is_being_moved" => 0,
                "rotation" => null
            ],
            [
                "x" => 5,
                "y" => 6,
                "id" => 5,
                "type" => MRJToken::TYPE_GASLIGHT,
                "is_flipped" => 0,
                "is_being_moved" => 0,
                "rotation" => null
            ],
            [
                "x" => 7,
                "y" => 5,
                "id" => 6,
                "type" => MRJToken::TYPE_GASLIGHT,
                "is_flipped" => 0,
                "is_being_moved" => 0,
                "rotation" => null
            ],
            [
                "x" => 10,
                "y" => 10,
                "id" => 3,
                "type" => MRJToken::TYPE_GASLIGHT,
                "is_flipped" => 0,
                "is_being_moved" => 0,
                "rotation" => null
            ],
            [
                "x" => 11,
                "y" => 6,
                "id" => 1,
                "type" => MRJToken::TYPE_GASLIGHT,
                "is_flipped" => 0,
                "is_being_moved" => 0,
                "rotation" => null
            ],
            [
                "x" => 0,
                "y" => 3,
                "is_flipped" => 0,
                "is_being_moved" => 0,
                "type" => MRJToken::TYPE_PAWN,
                "id" => MRJCharacter::ID_SERGEANT_GOODLY,
                "rotation" => null
            ],
            [
                "x" => 4,
                "y" => 5,
                "is_flipped" => 0,
                "is_being_moved" => 0,
                "type" => MRJToken::TYPE_PAWN,
                "id" => MRJCharacter::ID_JEREMY_BERT,
                "rotation" => null
            ],
            [
                "x" => 4,
                "y" => 1,
                "is_flipped" => 0,
                "is_being_moved" => 0,
                "type" => MRJToken::TYPE_PAWN,
                "id" => MRJCharacter::ID_JOHN_H_WATSON,
                "rotation" => MRJToken::ROTATION_NE
            ],
            [
                "x" => 6,
                "y" => 4,
                "is_flipped" => 0,
                "is_being_moved" => 0,
                "type" => MRJToken::TYPE_PAWN,
                "id" => MRJCharacter::ID_SHERLOCK_HOLMES,
                "rotation" => null
            ],
            [
                "x" => 6,
                "y" => 7,
                "is_flipped" => 0,
                "is_being_moved" => 0,
                "type" => MRJToken::TYPE_PAWN,
                "id" => MRJCharacter::ID_JOHN_SMITH,
                "rotation" => null
            ],
            [
                "x" => 8,
                "y" => 6,
                "is_flipped" => 0,
                "is_being_moved" => 0,
                "type" => MRJToken::TYPE_PAWN,
                "id" => MRJCharacter::ID_INSPECTOR_LESTRADE,
                "rotation" => null
            ],
            [
                "x" => 8,
                "y" => 10,
                "is_flipped" => 0,
                "is_being_moved" => 0,
                "type" => MRJToken::TYPE_PAWN,
                "id" => MRJCharacter::ID_SIR_WILLIAM_GULL,
                "rotation" => null
            ],
            [
                "x" => 12,
                "y" => 8,
                "is_flipped" => 0,
                "is_being_moved" => 0,
                "type" => MRJToken::TYPE_PAWN,
                "id" => MRJCharacter::ID_MISS_STEALTHY,
                "rotation" => null
            ],
            [
                "x" => 1,
                "y" => 6,
                "is_flipped" => 0,
                "is_being_moved" => 0,
                "type" => MRJToken::TYPE_MANHOLE,
                "id" => 1,
                "rotation" => null
            ],
            [
                "x" => 11,
                "y" => 5,
                "is_flipped" => 0,
                "is_being_moved" => 0,
                "type" => MRJToken::TYPE_MANHOLE,
                "id" => 2,
                "rotation" => null
            ],

        ];
    }

    public static function getInitialPoliceCordons() : array
    {
        return [
            MRJBoard::EXIT_ID_SW => [
                "is_active" => 1
            ],
            MRJBoard::EXIT_ID_NW => [
                "is_active" => 0
            ],
            MRJBoard::EXIT_ID_SE => [
                "is_active" => 0
            ],
            MRJBoard::EXIT_ID_NE => [
                "is_active" => 1
            ],
        ];
    }

    public static function getBoard() : MRJBoard
    {
        $board = new MRJBoard();

        $field = new MRJField(0, -1);
        $field->setIsBlocked(true);
        $field->setIsExit(true);
        $field->setExitTo(MRJBoard::EXIT_ID_SE);
        $board->addField($field);

        $field = new MRJField(1, -1);
        $field->setIsBlocked(true);
        $board->addField($field);

        $field = new MRJField(-1, 0);
        $field->setIsBlocked(true);
        $board->addField($field);

        $field = new MRJField(0, 0);
        $field->setIsBlocked(true);
        $board->addField($field);

        $field = new MRJField(1, 0);
        $board->addField($field);

        $field = new MRJField(2, 0);
        $field->setIsBlocked(true);
        $board->addField($field);

        $field = new MRJField(3, 0);
        $field->setIsBlocked(true);
        $board->addField($field);

        $field = new MRJField(4, 0);
        $field->setIsBlocked(true);
        $board->addField($field);
        
        $field = new MRJField(5, 0);
        $field->setIsBlocked(true);
        $board->addField($field);

        $field = new MRJField(-1, 1);
        $field->setIsBlocked(true);
        $board->addField($field);

        $field = new MRJField(0, 1);
        $board->addField($field);

        $field = new MRJField(1, 1);
        $field->setIsManhole(true);
        $board->addField($field);

        $field = new MRJField(2, 1);
        $field->setIsGaslight(true);
        $board->addField($field);

        $field = new MRJField(3, 1);
        $field->setIsBlocked(true);
        $board->addField($field);

        $field = new MRJField(4, 1);
        $board->addField($field);

        $field = new MRJField(5, 1);
        $field->setIsManhole(true);
        $board->addField($field);

        $field = new MRJField(6, 1);
        $field->setIsBlocked(true);
        $board->addField($field);

        $field = new MRJField(-1, 2);
        $field->setIsBlocked(true);
        $board->addField($field);

        $field = new MRJField(0, 2);
        $board->addField($field);

        $field = new MRJField(1, 2);
        $field->setIsBlocked(true);
        $board->addField($field);

        $field = new MRJField(2, 2);
        $board->addField($field);

        $field = new MRJField(3, 2);
        $board->addField($field);

        $field = new MRJField(4, 2);
        $board->addField($field);

        $field = new MRJField(5, 2);
        $field->setIsGaslight(true);
        $board->addField($field);

        $field = new MRJField(6, 2);
        $board->addField($field);

        $field = new MRJField(7, 2);
        $field->setIsBlocked(true);
        $board->addField($field);

        $field = new MRJField(-1, 3);
        $field->setIsBlocked(true);
        $board->addField($field);

        $field = new MRJField(0, 3);
        $board->addField($field);

        $field = new MRJField(1, 3);
        $field->setIsBlocked(true);
        $board->addField($field);

        $field = new MRJField(2, 3);
        $field->setIsBlocked(true);
        $board->addField($field);

        $field = new MRJField(3, 3);
        $field->setIsBlocked(true);
        $board->addField($field);

        $field = new MRJField(4, 3);
        $board->addField($field);

        $field = new MRJField(5, 3);
        $field->setIsBlocked(true);
        $board->addField($field);

        $field = new MRJField(6, 3);
        $board->addField($field);

        $field = new MRJField(7, 3);
        $board->addField($field);

        $field = new MRJField(8, 3);
        $field->setIsBlocked(true);
        $board->addField($field);
        
        $field = new MRJField(-1, 4);
        $field->setIsBlocked(true);
        $board->addField($field);

        $field = new MRJField(0, 4);
        $board->addField($field);
        
        $field = new MRJField(1, 4);
        $board->addField($field);
        
        $field = new MRJField(2, 4);
        $field->setIsManhole(true);
        $board->addField($field);
        
        $field = new MRJField(3, 4);
        $board->addField($field);
        
        $field = new MRJField(4, 4);
        $board->addField($field);
        
        $field = new MRJField(5, 4);
        $board->addField($field);
        
        $field = new MRJField(6, 4);
        $board->addField($field);

        $field = new MRJField(7, 4);
        $field->setIsBlocked(true);
        $board->addField($field);

        $field = new MRJField(8, 4);
        $board->addField($field);

        $field = new MRJField(9, 4);
        $field->setIsBlocked(true);
        $board->addField($field);

        $field = new MRJField(10, 4);
        $field->setIsBlocked(true);
        $board->addField($field);

        $field = new MRJField(11, 4);
        $field->setIsBlocked(true);
        $field->setIsExit(true);
        $field->setExitTo(MRJBoard::EXIT_ID_SW);
        $board->addField($field);
        
        $field = new MRJField(-1, 5);
        $field->setIsBlocked(true);
        $board->addField($field);

        $field = new MRJField(0, 5);
        $board->addField($field);
        
        $field = new MRJField(1, 5);
        $field->setIsGaslight(true);
        $board->addField($field);
        
        $field = new MRJField(2, 5);
        $board->addField($field);

        $field = new MRJField(3, 5);
        $field->setIsBlocked(true);
        $board->addField($field);

        $field = new MRJField(4, 5);
        $board->addField($field);

        $field = new MRJField(5, 5);
        $field->setIsBlocked(true);
        $board->addField($field);
        
        $field = new MRJField(6, 5);
        $field->setIsManhole(true);
        $board->addField($field);
        
        $field = new MRJField(7, 5);
        $field->setIsGaslight(true);
        $board->addField($field);
        
        $field = new MRJField(8, 5);
        $board->addField($field);
        
        $field = new MRJField(9, 5);
        $board->addField($field);
        
        $field = new MRJField(10, 5);
        $board->addField($field);
        
        $field = new MRJField(11, 5);
        $field->setIsManhole(true);
        $board->addField($field);

        $field = new MRJField(12, 5);
        $field->setIsBlocked(true);
        $board->addField($field);

        $field = new MRJField(0, 6);
        $field->setIsBlocked(true);
        $board->addField($field);
        
        $field = new MRJField(1, 6);
        $field->setIsManhole(true);
        $board->addField($field);
        
        $field = new MRJField(2, 6);
        $board->addField($field);
        
        $field = new MRJField(3, 6);
        $board->addField($field);
        
        $field = new MRJField(4, 6);
        $board->addField($field);
        
        $field = new MRJField(5, 6);
        $field->setIsGaslight(true);
        $board->addField($field);
        
        $field = new MRJField(6, 6);
        $board->addField($field);

        $field = new MRJField(7, 6);
        $field->setIsBlocked(true);
        $board->addField($field);
        
        $field = new MRJField(8, 6);
        $board->addField($field);

        $field = new MRJField(9, 6);
        $field->setIsBlocked(true);
        $board->addField($field);
        
        $field = new MRJField(10, 6);
        $board->addField($field);
        
        $field = new MRJField(11, 6);
        $field->setIsGaslight(true);
        $board->addField($field);
        
        $field = new MRJField(12, 6);
        $board->addField($field);

        $field = new MRJField(13, 6);
        $field->setIsBlocked(true);
        $board->addField($field);

        $field = new MRJField(1, 7);
        $field->setIsBlocked(true);
        $field->setIsExit(true);
        $field->setExitTo(MRJBoard::EXIT_ID_NE);
        $board->addField($field);
        
        $field = new MRJField(2, 7);
        $field->setIsBlocked(true);
        $board->addField($field);
        
        $field = new MRJField(3, 7);
        $field->setIsBlocked(true);
        $board->addField($field);
        
        $field = new MRJField(4, 7);
        $board->addField($field);

        $field = new MRJField(5, 7);
        $field->setIsBlocked(true);
        $board->addField($field);
        
        $field = new MRJField(6, 7);
        $board->addField($field);
        
        $field = new MRJField(7, 7);
        $board->addField($field);
        
        $field = new MRJField(8, 7);
        $board->addField($field);
        
        $field = new MRJField(9, 7);
        $board->addField($field);
        
        $field = new MRJField(10, 7);
        $field->setIsManhole(true);
        $board->addField($field);
        
        $field = new MRJField(11, 7);
        $board->addField($field);
        
        $field = new MRJField(12, 7);
        $board->addField($field);

        $field = new MRJField(13, 7);
        $field->setIsBlocked(true);
        $board->addField($field);
        
        $field = new MRJField(4, 8);
        $field->setIsBlocked(true);
        $board->addField($field);
        
        $field = new MRJField(5, 8);
        $board->addField($field);
        
        $field = new MRJField(6, 8);
        $board->addField($field);

        $field = new MRJField(7, 8);
        $field->setIsBlocked(true);
        $board->addField($field);
        
        $field = new MRJField(8, 8);
        $board->addField($field);

        $field = new MRJField(9, 8);
        $field->setIsBlocked(true);
        $board->addField($field);

        $field = new MRJField(10, 8);
        $field->setIsBlocked(true);
        $board->addField($field);

        $field = new MRJField(11, 8);
        $field->setIsBlocked(true);
        $board->addField($field);
        
        $field = new MRJField(12, 8);
        $board->addField($field);

        $field = new MRJField(13, 8);
        $field->setIsBlocked(true);
        $board->addField($field);
        
        $field = new MRJField(5, 9);
        $field->setIsBlocked(true);
        $board->addField($field);

        $field = new MRJField(6, 9);
        $board->addField($field);
        
        $field = new MRJField(7, 9);
        $field->setIsGaslight(true);
        $board->addField($field);
        
        $field = new MRJField(8, 9);
        $board->addField($field);
        
        $field = new MRJField(9, 9);
        $board->addField($field);
        
        $field = new MRJField(10, 9);
        $board->addField($field);
        
        $field = new MRJField(11, 9);
        $field->setIsBlocked(true);
        $board->addField($field);
        
        $field = new MRJField(12, 9);
        $board->addField($field);

        $field = new MRJField(13, 9);
        $field->setIsBlocked(true);
        $board->addField($field);

        $field = new MRJField(6, 10);
        $field->setIsBlocked(true);
        $board->addField($field);
        
        $field = new MRJField(7, 10);
        $field->setIsManhole(true);
        $board->addField($field);
        
        $field = new MRJField(8, 10);
        $board->addField($field);

        $field = new MRJField(9, 10);
        $field->setIsBlocked(true);
        $board->addField($field);
        
        $field = new MRJField(10, 10);
        $field->setIsGaslight(true);
        $board->addField($field);
        
        $field = new MRJField(11, 10);
        $field->setIsManhole(true);
        $board->addField($field);
        
        $field = new MRJField(12, 10);
        $board->addField($field);

        $field = new MRJField(13, 10);
        $field->setIsBlocked(true);
        $board->addField($field);

        $field = new MRJField(7, 11);
        $field->setIsBlocked(true);
        $board->addField($field);
        
        $field = new MRJField(8, 11);
        $field->setIsBlocked(true);
        $board->addField($field);
        
        $field = new MRJField(9, 11);
        $field->setIsBlocked(true);
        $board->addField($field);
        
        $field = new MRJField(10, 11);
        $field->setIsBlocked(true);
        $board->addField($field);

        $field = new MRJField(11, 11);
        $board->addField($field);

        $field = new MRJField(12, 11);
        $field->setIsBlocked(true);
        $board->addField($field);
        
        $field = new MRJField(13, 11);
        $field->setIsBlocked(true);
        $board->addField($field);
        
        $field = new MRJField(11, 12);
        $field->setIsBlocked(true);
        $board->addField($field);

        $field = new MRJField(12, 12);
        $field->setIsBlocked(true);
        $field->setIsExit(true);
        $field->setExitTo(MRJBoard::EXIT_ID_NW);
        $board->addField($field);

        return $board;
    }
}