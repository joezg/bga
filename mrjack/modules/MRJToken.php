<?php

class MRJToken implements \JsonSerializable
{
    const TYPE_PAWN = "pawn";
    const TYPE_GASLIGHT = "gaslight";
    const TYPE_MANHOLE = "manhole";
    const ROTATION_N = "n";
    const ROTATION_S = "s";
    const ROTATION_NE = "ne";
    const ROTATION_NW = "nw";
    const ROTATION_SE = "se";
    const ROTATION_SW = "sw";

    private $type;
    private $id;
    private $x;
    private $y;
    private $isFlipped;
    private $isBeingMoved;
    private $rotation;

    public function __construct(array $raw) {
        $this->type = $raw["type"];
        $this->id = $raw["id"];
        $this->x = $raw["x"];
        $this->y = $raw["y"];
        $this->isFlipped = $raw["is_flipped"] === "1";
        $this->isBeingMoved = $raw["is_being_moved"] === "1";
        $this->rotation = $raw["rotation"];
    }
    
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }

    /**
     * Get the value of isFlipped
     */ 
    public function getIsFlipped()
    {
        return $this->isFlipped;
    }

    /**
     * Set the value of isFlipped
     *
     * @return  self
     */ 
    public function setIsFlipped($isFlipped)
    {
        $this->isFlipped = $isFlipped;

        return $this;
    }

    /**
     * Get the value of type
     */ 
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set the value of type
     *
     * @return  self
     */ 
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get the value of id
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */ 
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of x
     */ 
    public function getX()
    {
        return $this->x;
    }

    /**
     * Set the value of x
     *
     * @return  self
     */ 
    public function setX($x)
    {
        $this->x = $x;

        return $this;
    }

    /**
     * Get the value of y
     */ 
    public function getY()
    {
        return $this->y;
    }

    /**
     * Set the value of y
     *
     * @return  self
     */ 
    public function setY($y)
    {
        $this->y = $y;

        return $this;
    }

    public function getZ(){
        return -1 * ($this->x + $this->y);
    }

    public function getCoordinatesKey(){
        return $this->x.$this->y;
    }

    /**
     * Get the value of isBeingMoved
     */ 
    public function getIsBeingMoved()
    {
        return $this->isBeingMoved;
    }

    /**
     * Set the value of isBeingMoved
     *
     * @return  self
     */ 
    public function setIsBeingMoved($isBeingMoved)
    {
        $this->isBeingMoved = $isBeingMoved;

        return $this;
    }

    /**
     * Get the value of rotation
     */ 
    public function getRotation()
    {
        return $this->rotation;
    }

    /**
     * Set the value of rotation
     *
     * @return  self
     */ 
    public function setRotation($rotation)
    {
        $this->rotation = $rotation;

        return $this;
    }
}