const int = "int";
const string = "string";
const bool = "bool";

const stateDefinition = {
    board: {
        fields: [{
            x: int,
            y: int,
            isActive: bool,
            isCapturePossible: bool
        }],
        pawns: [{
            x: int,
            y: int,
            id: string,
            isFlipped: bool,
            rotation: string
        }],
        tokens: [{
            x: int,
            y: int,
            type: string,
            id: string
        }],
        cordons: [{
            position: string,
            isActive: bool
        }]
    },
    commonArea: {
        cardDisplay: [{
            id: int,
            type: string,
            isSelected: bool,
            isActive: bool
        }],
        remainingAlibiCards: int,
        remainingCharacterCards: int,
        wasMrJackVisible: bool,
        roundNumber: int 
    },
    playerInformationArea: [{
        playerId: int,
        playerRole: string,
        playerCharacter: string,
        privateAlibiCards: [{
            id: int,
            type: string
        }]
    }]
}