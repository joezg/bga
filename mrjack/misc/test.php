<?php 

include "/home/jurica/projects/bga/mrjack/modules/MRJBoard.php";
include "/home/jurica/projects/bga/mrjack/modules/MRJBoardFactory.php";
include "/home/jurica/projects/bga/mrjack/modules/MRJCharacter.php";
include "/home/jurica/projects/bga/mrjack/modules/MRJField.php";
include "/home/jurica/projects/bga/mrjack/modules/MRJToken.php";
include "/home/jurica/projects/bga/mrjack/modules/MRJPoliceCordon.php";
include "/home/jurica/projects/bga/mrjack/modules/characters/MRJSherlockHolmes.php";
include "/home/jurica/projects/bga/mrjack/modules/characters/MRJJohnHWatson.php";
include "/home/jurica/projects/bga/mrjack/modules/characters/MRJJohnSmith.php";
include "/home/jurica/projects/bga/mrjack/modules/characters/MRJInspectorLestrade.php";
include "/home/jurica/projects/bga/mrjack/modules/characters/MRJMissStealthy.php";
include "/home/jurica/projects/bga/mrjack/modules/characters/MRJSergeantGoodley.php";
include "/home/jurica/projects/bga/mrjack/modules/characters/MRJSirWilliamGull.php";
include "/home/jurica/projects/bga/mrjack/modules/characters/MRJJeremyBert.php";

$board = MRJBoardFactory::getBoard();
$tokens = array_map(function($raw){
    return new MRJToken($raw);
}, MRJBoardFactory::getInitialTokenPositions());

$board->addTokens($tokens);

$source = new MRJField(0, 3);

var_dump($board->getCharacterMoves(MRJCharacter::getCharacter("swg"), true, true));